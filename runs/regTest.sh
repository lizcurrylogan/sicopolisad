#!/bin/bash

#export SUFFIX1=$1
#export SUFFIX2=$2

#echo $SUFFUX1 $SUFFIX2

declare -a arr=("" "_CALCMOD1" "_ENHMOD5" "_CALCMOD2" "_PDDMOD2")

for i in "${arr[@]}"
do 
   echo "This is the $i-th run"

   ./singleRunSico.sh -fm REG_TEST$i
 
   cd ../sico_out

   diff -q REF_OUTPUT/ref_output$i/ref_output$i'.core' REG_TEST$i/REG_TEST$i'.core'
   diff -q REF_OUTPUT/ref_output$i/ref_output$i'.log'  REG_TEST$i/REG_TEST$i'.log'
   diff -q REF_OUTPUT/ref_output$i/ref_output$i'.ser'  REG_TEST$i/REG_TEST$i'.ser'

   cd ../runs
done
