#!/bin/bash
LANG=C

################################################################################

function error()
{
   echo -e "\n******************************************************************************">&2
   echo -e   "** ERROR:                                                                   **">&2
   echo -e   "$1" | awk '{printf("** %-73s**\n",$0)}'>&2
   echo -e "******************************************************************************\n">&2
   exit 1
}

################################################################################

function info()
{
   echo -e "$1" >&2
}

################################################################################

function usage()
{
   echo -e "\n  Usage: `basename $0` [options...]\n"\
   "     [-i <dir>] => individual input directory, default is sico_in\n"\
   "     [-d <dir>] => individual output directory, default is sico_out/<run_name>\n"\
   "     [-f] => force overwriting the output directory\n"\
   "     [-n] => skip make clean\n"\
   "     [-z] => manual configuration by file sico_configs.sh\n"\
   "             (otherwise handled by GNU Autotools)\n"\

      if [ "$1" ]; then error "$1"; fi
}

################################################################################

function check_args()
{
   while getopts d:fhi:nz? OPT ; do
     case $OPT in
       d) local MULTI_OUTDIR_ARG=$OPTARG ;;
       f) local FORCE="TRUE";;
       i) local INDIR=$OPTARG;;
       n) local SKIP_MAKECLEAN="TRUE";;
       z) local MANUAL_CONFIGURATION=="TRUE";;
     h|?) usage; exit 1;;
     esac            
   done

   MULTI_OPTIONS_1=' '
   MULTI_OPTIONS_2=' '

   if [ ! "$MULTI_OUTDIR_ARG" ]; then
      MULTI_OUTDIR=${PWD}"/../sico_out"
   else
      lastch=`echo $MULTI_OUTDIR_ARG | sed -e 's/\(^.*\)\(.$\)/\2/'`
      if [ ${lastch} == "/" ]; then
         MULTI_OUTDIR_ARG=`echo $MULTI_OUTDIR_ARG  | sed '$s/.$//'`
      fi
      MULTI_OUTDIR=${MULTI_OUTDIR_ARG}
      MULTI_OPTIONS_1="$MULTI_OPTIONS_1 -d $MULTI_OUTDIR"
      MULTI_OPTIONS_2="$MULTI_OPTIONS_2 -d $MULTI_OUTDIR"
   fi

   if [ "$FORCE" ]; then
      MULTI_OPTIONS_1="$MULTI_OPTIONS_1 -f"
   fi

   if [ "$INDIR" ]; then
      MULTI_OPTIONS_1="$MULTI_OPTIONS_1 -i $INDIR"
   fi

   if [ "$SKIP_MAKECLEAN" ]; then
      MULTI_OPTIONS_1="$MULTI_OPTIONS_1 -n"
      MULTI_OPTIONS_2="$MULTI_OPTIONS_2 -n"
   fi

   if [ "$MANUAL_CONFIGURATION" ]; then
      MULTI_OPTIONS_1="$MULTI_OPTIONS_1 -z"
      MULTI_OPTIONS_2="$MULTI_OPTIONS_2 -z"
   fi

   info "Options for  sico.sh:$MULTI_OPTIONS_1"
   info "Options for tools.sh:$MULTI_OPTIONS_2"
}

################################################################################

function run()
{
   (./sico.sh ${MULTI_OPTIONS_1} -m v32_emtp2sge25_expA) \
              >out_multi_101.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_grl20_ss25ka) \
              >out_multi_102.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_ant40_ss25ka) \
              >out_multi_103.dat 2>&1

   #--------

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_grl20_paleo01_init) \
              >out_multi_104.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_grl20_paleo01 \
              -a ${MULTI_OUTDIR}/v32_grl20_paleo01_init) \
              >out_multi_105.dat 2>&1

   #--------

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_grl20_wre1000 \
              -a ${MULTI_OUTDIR}/v32_grl20_paleo01) \
              >out_multi_106.dat 2>&1

   cd $PWD/../tools ; echo 0003 | \
   (./tools.sh -p resolution_doubler ${MULTI_OPTIONS_2} -m v32_grl20_paleo01) \
               >$OLDPWD/out_multi_107.dat 2>&1 ; cd $OLDPWD

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_grl10_wre1000 \
              -a ${MULTI_OUTDIR}/v32_grl20_paleo01) \
              >out_multi_108.dat 2>&1

   #--------

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_nhem80_nt012_new) \
              >out_multi_111.dat 2>&1

   #--------

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_grl20_sr_paleo44_init100a) \
              >out_multi_112.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_grl20_sr_paleo44_fixtopo1 \
              -a ${MULTI_OUTDIR}/v32_grl20_sr_paleo44_init100a) \
              >out_multi_113.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_grl20_sr_paleo44_fixtopo2 \
              -a ${MULTI_OUTDIR}/v32_grl20_sr_paleo44_fixtopo1) \
              >out_multi_114.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_grl20_sr_paleo44_100a \
              -a ${MULTI_OUTDIR}/v32_grl20_sr_paleo44_fixtopo2) \
              >out_multi_115.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_grl20_sr_future44_ctl \
              -a ${MULTI_OUTDIR}/v32_grl20_sr_paleo44_100a) \
              >out_multi_116.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_grl20_sr_future44_c2 \
              -a ${MULTI_OUTDIR}/v32_grl20_sr_paleo44_100a) \
              >out_multi_117.dat 2>&1

   #--------

   OMP_NUM_THREADS=2; export OMP_NUM_THREADS
   #              (number of threads for the SSA solver using OpenMP)

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_ant40_sr_spinup01_init100a) \
              >out_multi_118.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_ant40_sr_spinup01_fixtopo1 \
              -a ${MULTI_OUTDIR}/v32_ant40_sr_spinup01_init100a) \
              >out_multi_119.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_ant40_sr_spinup01_fixtopo2 \
              -a ${MULTI_OUTDIR}/v32_ant40_sr_spinup01_fixtopo1) \
              >out_multi_120.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_ant40_sr_spinup01_fixtopo3 \
              -a ${MULTI_OUTDIR}/v32_ant40_sr_spinup01_fixtopo2) \
              >out_multi_121.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_ant40_sr_spinup01_20a \
              -a ${MULTI_OUTDIR}/v32_ant40_sr_spinup01_fixtopo3) \
              >out_multi_122.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_ant40_sr_future01_ctl \
              -a ${MULTI_OUTDIR}/v32_ant40_sr_spinup01_20a) \
              >out_multi_123.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_ant40_sr_future01_m2 \
              -a ${MULTI_OUTDIR}/v32_ant40_sr_spinup01_20a) \
              >out_multi_124.dat 2>&1
}

################################################################################

check_args $*
run

######################## End of multi_sico_1.sh ################################
