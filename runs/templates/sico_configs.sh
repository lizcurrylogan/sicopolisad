#-------- Compiler --------

if [ "$FC" != "" ] ; then
   echo "Fortran compiler >$FC< found."
else
   echo "No Fortran compiler found (environment variable FC not set)."
   echo "Trying gfortran..."
   FC=gfortran
fi

#-------- Flags --------

NETCDF_FLAG='yes'
#   'yes' -> linking of SICOPOLIS with NetCDF library
#   'no'  -> linking of SICOPOLIS without NetCDF library

LIS_FLAG='no'
#   'yes' -> linking of SICOPOLIS with Lis library
#            (requires also OPENMP_FLAG='yes')
#   'no'  -> linking of SICOPOLIS without Lis library

OPENMP_FLAG='no'
#   'yes' -> OpenMP will be used
#            (so far only required if LIS_FLAG='yes')
#   'no'  -> OpenMP will not be used

LARGE_DATA_FLAG='no'
#   'yes' -> no memory restriction on data (more than 2GB possible);
#            required for very-high-resolution simulations
#   'no'  -> memory restriction on data (max. 2GB possible);
#            OK for medium- and low-resolution simulations

#-------- NetCDF settings --------

if [ ${NETCDF_FLAG} = 'yes' ] ; then

   NETCDFHOME=/opt/netcdf

   if [ -f $NETCDFHOME/bin/nf-config ] ; then
      # Automatic settings with nf-config
      if [ $($NETCDFHOME/bin/nf-config --has-f90) == 'yes' ] ; then
         NETCDF_FLAGS="$($NETCDFHOME/bin/nf-config --cflags)"
         NETCDF_LIBS="$($NETCDFHOME/bin/nf-config --flibs)"
      else
         echo "NetCDF error: compiled without f90 interface."
         exit 1
      fi
   elif [ -f $NETCDFHOME/bin/nc-config ] ; then
      # Automatic settings with nc-config
      if [ $($NETCDFHOME/bin/nc-config --has-f90) == 'yes' ] ; then
         NETCDF_FLAGS="$($NETCDFHOME/bin/nc-config --cflags)"
         NETCDF_LIBS="$($NETCDFHOME/bin/nc-config --flibs)"
      else
         echo "NetCDF error: compiled without f90 interface."
         exit 1
      fi
   else
      # Manual settings
      NETCDFINCLUDE=${NETCDFHOME}'/include'
      NETCDFLIB=${NETCDFHOME}'/lib'
   fi

fi

#-------- Lis settings --------

if [ ${LIS_FLAG} = 'yes' ] ; then
   LISHOME=/opt/lis
   LISINCLUDE=${LISHOME}'/include'
   LISLIB=${LISHOME}'/lib'
fi

#-------- Compiler flags --------

if [ ${FC} = ifort ] ; then

   case $PROGNAME in
        "sicopolis")
           FCFLAGS='-xHOST -O3 -no-prec-div'
           # This is '-fast' without '-static' and '-ipo'
           ;;
        "sicograph")
           FCFLAGS='-O2 -Vaxlib'
           ;;
        *)
           FCFLAGS='-O2'
           ;;
   esac            

   if [ ${LARGE_DATA_FLAG} = 'yes' ] ; then
      FCFLAGS=${FCFLAGS}' -mcmodel=medium -shared-intel'
      # No memory restriction on data (more than 2GB possible)
   fi

   if [ ${OPENMP_FLAG} = 'yes' ] ; then
      case $PROGNAME in
           "sicopolis")
              FCFLAGS=${FCFLAGS}' -openmp'
              ;;
           *) ;;
      esac            
   fi

elif [ ${FC} = gfortran ] ; then

   case $PROGNAME in
        "sicopolis")
           FCFLAGS='-O3'
           ;;
        "sicograph")
           FCFLAGS='-O2'
           ;;
        *)
           FCFLAGS='-O2'
           ;;
   esac            

   if [ ${LARGE_DATA_FLAG} = 'yes' ] ; then
      FCFLAGS=${FCFLAGS}' -mcmodel=medium'
      # No memory restriction on data (more than 2GB possible)
   fi

   if [ ${OPENMP_FLAG} = 'yes' ] ; then
      case $PROGNAME in
           "sicopolis")
              FCFLAGS=${FCFLAGS}' -fopenmp'
              ;;
           *) ;;
      esac            
   fi

else
   echo "Unknown compiler flags for >$FC<, must exit."
   echo "Add flags to >sico_configs.sh< and try again."
   exit 1
fi

if [ ${LIS_FLAG} = 'yes' ] ; then
   case $PROGNAME in
        "sicopolis")
           FCFLAGS=${FCFLAGS}' -I'${LISINCLUDE}' -L'${LISLIB}' -llis'
           ;;
        *) ;;
   esac            
fi

if [ ${NETCDF_FLAG} = 'yes' ] ; then
   if [ -f $NETCDFHOME/bin/nf-config ] ; then
      # Automatic settings with nf-config
      FCFLAGS=${FCFLAGS}' '${NETCDF_FLAGS}' '${NETCDF_LIBS}
   elif [ -f $NETCDFHOME/bin/nc-config ] ; then
      # Automatic settings with nc-config
      FCFLAGS=${FCFLAGS}' '${NETCDF_FLAGS}' '${NETCDF_LIBS}
   else
      # Manual settings
      FCFLAGS=${FCFLAGS}' -I'${NETCDFINCLUDE}' -L'${NETCDFLIB}' -lnetcdf'
   fi
fi

echo "Flags: $FCFLAGS"
