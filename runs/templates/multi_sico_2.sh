#!/bin/bash
LANG=C

################################################################################

function error()
{
   echo -e "\n******************************************************************************">&2
   echo -e   "** ERROR:                                                                   **">&2
   echo -e   "$1" | awk '{printf("** %-73s**\n",$0)}'>&2
   echo -e "******************************************************************************\n">&2
   exit 1
}

################################################################################

function info()
{
   echo -e "$1" >&2
}

################################################################################

function usage()
{
   echo -e "\n  Usage: `basename $0` [options...]\n"\
   "     [-i <dir>] => individual input directory, default is sico_in\n"\
   "     [-d <dir>] => individual output directory, default is sico_out/<run_name>\n"\
   "     [-f] => force overwriting the output directory\n"\
   "     [-n] => skip make clean\n"\
   "     [-z] => manual configuration by file sico_configs.sh\n"\
   "             (otherwise handled by GNU Autotools)\n"\

      if [ "$1" ]; then error "$1"; fi
}

################################################################################

function check_args()
{
   while getopts d:fhi:nz? OPT ; do
     case $OPT in
       d) local MULTI_OUTDIR_ARG=$OPTARG ;;
       f) local FORCE="TRUE";;
       i) local INDIR=$OPTARG;;
       n) local SKIP_MAKECLEAN="TRUE";;
       z) local MANUAL_CONFIGURATION=="TRUE";;
     h|?) usage; exit 1;;
     esac            
   done

   MULTI_OPTIONS_1=' '
   MULTI_OPTIONS_2=' '

   if [ ! "$MULTI_OUTDIR_ARG" ]; then
      MULTI_OUTDIR=${PWD}"/../sico_out"
   else
      lastch=`echo $MULTI_OUTDIR_ARG | sed -e 's/\(^.*\)\(.$\)/\2/'`
      if [ ${lastch} == "/" ]; then
         MULTI_OUTDIR_ARG=`echo $MULTI_OUTDIR_ARG  | sed '$s/.$//'`
      fi
      MULTI_OUTDIR=${MULTI_OUTDIR_ARG}
      MULTI_OPTIONS_1="$MULTI_OPTIONS_1 -d $MULTI_OUTDIR"
      MULTI_OPTIONS_2="$MULTI_OPTIONS_2 -d $MULTI_OUTDIR"
   fi

   if [ "$FORCE" ]; then
      MULTI_OPTIONS_1="$MULTI_OPTIONS_1 -f"
   fi

   if [ "$INDIR" ]; then
      MULTI_OPTIONS_1="$MULTI_OPTIONS_1 -i $INDIR"
   fi

   if [ "$SKIP_MAKECLEAN" ]; then
      MULTI_OPTIONS_1="$MULTI_OPTIONS_1 -n"
      MULTI_OPTIONS_2="$MULTI_OPTIONS_2 -n"
   fi

   if [ "$MANUAL_CONFIGURATION" ]; then
      MULTI_OPTIONS_1="$MULTI_OPTIONS_1 -z"
      MULTI_OPTIONS_2="$MULTI_OPTIONS_2 -z"
   fi

   info "Options for  sico.sh:$MULTI_OPTIONS_1"
   info "Options for tools.sh:$MULTI_OPTIONS_2"
}

################################################################################

function run()
{
   (./sico.sh ${MULTI_OPTIONS_1} -m v32_asf2_steady) \
              >out_multi_201.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_asf2_surge) \
              >out_multi_202.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_scand_test) \
              >out_multi_203.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_tibet_test) \
              >out_multi_204.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_nmars_test) \
              >out_multi_205.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_smars_test) \
              >out_multi_206.dat 2>&1

   (./sico.sh ${MULTI_OPTIONS_1} -m v32_heino50_st) \
              >out_multi_207.dat 2>&1
}

################################################################################

check_args $*
run

######################## End of multi_sico_2.sh ################################
