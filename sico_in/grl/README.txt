Data files grl_b2_xx_w(o)em_xxx.dat (present-day ice surface,
ice base, lithosphere surface, mask) based on

   Bamber, J. L. and 10 others. 2013.
   A new bed elevation dataset for Greenland.
   The Cryosphere 7 (2), 499-510, doi: 10.5194/tc-7-499-2013.

Files ..._wem_... include Ellesmere Island as ice-free land,
while files ..._woem_... are without Ellesmere Island
(changed to ocean).

Data files grl_cc_xx_prec.dat (present-day precipitation) based on

   Calanca, P., H. Gilgen, S. Ekholm and A. Ohmura. 2000.
   Gridded temperature and accumulation distributions for
   Greenland for use in cryospheric models.
   Ann. Glaciol., 31, 118-120.

   Jaeger, L.. 1976. Monatskarten des Niederschlags fuer die ganze Erde.
   Berichte des Deutschen Wetterdienstes, No. 139, Offenbach, Germany.

Data files grl_uk_xx_xxx_anom.dat (LGM temperature and precipitation
anomalies) based on

   Hewitt, C. D. and J. F. B. Mitchell. 1997.
   Radiative forcing and response of a GCM to ice age boundary
   conditions: cloud feedback and climate sensitivity.
   Climate Dyn., 13 (11), 821-834.

Data files grl_pg_modx_xx_qgeo.dat (geothermal heat flux) based on

   Greve, R. 2005.
   Relation of measured basal temperatures and the spatial
   distribution of the geothermal heat flux for the Greenland
   ice sheet.
   Ann. Glaciol. 42, 424-432, doi: 10.3189/172756405781812510.

Data files grl_pu_xx_qgeo.dat (geothermal heat flux) based on

   Michael Purucker (personal communication 2012),

   Fox Maule, C., M. E. Purucker, N. Olsen and K. Mosegaard. 2005.
   Heat flux in Antarctica revealed from satellite magnetic data.
   Science 309 (5733), 464-467, doi: 10.1126/science.1106888.

All data files grl_sr_dev1.2_xxx.dat have been produced from the
SeaRISE data set Greenland_5km_dev1.2.nc (retrieved on 2010-04-19 from
http://websrv.cs.umt.edu/isis/index.php/Present_Day_Greenland).

For references see Present_Day_Greenland_2011-03-11.pdf.

All data files grl_sr_JHKP_xxx.dat have been produced from the
SeaRISE data set Greenland_5km_JHKP.nc (Herzfeld and 5 others,
Ann. Glaciol. 53 (60), 281-293, 2012, doi: 10.3189/2012AoG60A061).

The climate forcing files
grl_sr_climate_forcing_anom_2004_2098_v4_20.nc,
grl_sr_climate_forcing_anom_2004_2098_v4_10.nc and
grl_sr_climate_forcing_anom_2004_2098_v4_05.nc have been produced from
the SeaRISE file GL_climate_forcing_2004_2098_v4.nc (retrieved on
2010-06-08 from
http://websrv.cs.umt.edu/isis/index.php/Future_Climate_Data).

For references see Future_Climate_Data_2011-06-29.pdf.
