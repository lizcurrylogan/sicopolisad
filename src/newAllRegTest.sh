#!/bin/bash
# This script takes variables to re-build and compile the plain forward version of of sicoAD
# versus the original forward version of sicoAD

################################################################################

function page_break()
{
echo "-------------------------------------------------------------------------"
echo "-------------------------------------------------------------------------"
echo "                              "$1" DONE"   
echo "-------------------------------------------------------------------------"
echo "-------------------------------------------------------------------------"
}

################################################################################

function compare_outputs()
{

if [ $1 == "COSTS" ]; then
  echo "Checking cost comparison betwee ORIG and GRDCHK:"
  FILE_ORIG="../sico_out/"$2"_ORIG/"$2"_ORIG_COST.dat"
  FILE_GRDCHK="../sico_out/"$2"_GRDCHK/CostVals_"$2"_GRDCHK.dat"
  DIFF=$(diff <(head -n 1 $FILE_ORIG) <(head -n 1 $FILE_GRDCHK))

  if [ "$DIFF" == "" ]; then
    echo "GOOD NEWS! costs are same for "$2" ORIG and GRDCHK"
  else
    echo "Sorry Liz, unfortunately, costs are different for "$2", ORIG and GRDCHK:"
    echo "$DIFF"
    echo "exiting now." 
    exit 1
  fi

elif [ $1 == "GRADIENTS" ]; then
  echo "gradients"
fi

}

################################################################################

# These are the header files for our regression tests:
# --- This first one is simply 100 years longer in simulation time than the second one:
#declare -a HEADER_FILE=("ref_output_100y" "CALCMOD1_100y" "CALCMOD2_100y" "ENHMOD5_100y" "PDDMOD2_100y" )
declare -a HEADER_FILE=("ref_output" "CALCMOD1" "CALCMOD2" "ENHMOD5" "PDDMOD2" )

# These are the arguments given in _OAD mode (plain, tape, or adjoint):
declare -a MODES=("-p" "-t" "-a")

# TESTS: _ORIG, _GRDCHK, _OAD

for test in "${HEADER_FILE[@]}"
do

# ------------------------ _ORIG
   HEADER=$test"_ORIG" 
   cd ../runs
   ./singleRunSico.sh -fm $HEADER
   page_break $HEADER

# ------------------------ _GRDCHK
   HEADER=$test"_GRDCHK" 
   ./singleRunSico.sh -fm $HEADER
   mv ../src/GradientVals_$HEADER.dat ../sico_out/$HEADER
   mv ../src/CostVals_$HEADER.dat ../sico_out/$HEADER
   compare_outputs "COSTS" $test
   page_break $HEADER

# ------------------------ _OAD
#   HEADER=$test"_OAD" 

#   cd ../src

   # compiling OAD version:
#   make -f MakefileOpenAD clean
#   make -f MakefileOpenAD HEADER=$HEADER
#   make -f MakefileOpenAD driver

   # loop over _OAD MODES: plain (-p), tape (-t), adjoint (-a)
#   for mode in "${MODES[@]}"
#   do

      # remove the directory if it already exists (will cause sico problems):
#      if [ -d ../sico_out/$HEADER ]; then
#         rm -r ../sico_out/$HEADER
#         echo "output dir "$HEADER" existed and was removed"
#      fi

#      OUT_FNAME="out_"$HEADER$mode".dat"

      # this is the execution:
#      ./driver $mode > $OUT_FNAME
   
#      page_break $HEADER$mode

#   done
done
