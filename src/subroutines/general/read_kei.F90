!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  Subroutine :  r e a d _ k e i
!
!> @file
!!
!! Reading of the tabulated kei function.
!!
!! @section Copyright
!!
!! Copyright 2009-2016 Ralf Greve
!!
!! @section License
!!
!! This file is part of SICOPOLIS.
!!
!! SICOPOLIS is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! SICOPOLIS is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with SICOPOLIS.  If not, see <http://www.gnu.org/licenses/>.
!<
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!-------------------------------------------------------------------------------
!> Reading of the tabulated kei function.
!<------------------------------------------------------------------------------
subroutine read_kei()

use sico_types
use sico_variables

implicit none

integer(i4b) :: n, n_data_kei_max
integer(i4b) :: ios
real(dp)     :: r_val, d_dummy
character    :: ch_dummy

n_data_kei_max = 10000

kei = 0.0_dp

!-------- Reading of tabulated values --------

open(unit=11, iostat=ios, &
     file=INPATH//'/general/kei.dat', &
     status='old')
#ifndef ALLOW_OPENAD
if (ios /= 0) stop ' read_kei: Error when opening the kei file!'
#else
if (ios /= 0) write(*,*) ' read_kei: Error when opening the kei file!'
#endif
read(unit=11, fmt='(a)') ch_dummy
read(unit=11, fmt='(15x,f5.0)') kei_r_max
read(unit=11, fmt='(15x,f5.0)') kei_r_incr
read(unit=11, fmt='(a)') ch_dummy

n_data_kei = nint(kei_r_max/kei_r_incr)

if (n_data_kei > n_data_kei_max) stop ' read_kei: Array kei too small!'

read(unit=11, fmt='(a)') ch_dummy
read(unit=11, fmt='(a)') ch_dummy
read(unit=11, fmt='(a)') ch_dummy

do n=-n_data_kei, n_data_kei
   read(unit=11, fmt=*) d_dummy, kei(n)
end do

close(unit=11, status='keep')

end subroutine read_kei
!
