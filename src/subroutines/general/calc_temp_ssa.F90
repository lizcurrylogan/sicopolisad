!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  Subroutine :  c a l c _ t e m p _ s s a
!
!> @file
!!
!! Computation of temperature and age for ice shelves (floating ice).
!!
!! @section Copyright
!!
!! Copyright 2009-2016 Ralf Greve, Tatsuru Sato
!!
!! @section License
!!
!! This file is part of SICOPOLIS.
!!
!! SICOPOLIS is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! SICOPOLIS is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with SICOPOLIS.  If not, see <http://www.gnu.org/licenses/>.
!<
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!-------------------------------------------------------------------------------
!> Computation of temperature and age for ice shelves (floating ice).
!<------------------------------------------------------------------------------
subroutine calc_temp_ssa(at1, at2_1, at2_2, at3_1, at3_2, &
   at4_1, at4_2, at5, at6, at7, atr1, alb1, &
   ai1, ai2, &
   dtime_temp, dtt_2dxi, dtt_2deta, i, j)

use sico_types
use sico_variables
use ice_material_quantities, only : kappa_val, c_val
#ifndef ALLOW_OPENAD
use sico_sle_solvers, only : tri_sle
#else
use sico_sle_solvers
#endif 

implicit none

integer(i4b), intent(inout) :: i, j
real(dp), intent(in) :: at1(0:KCMAX), at2_1(0:KCMAX), at2_2(0:KCMAX), &
                        at3_1(0:KCMAX), at3_2(0:KCMAX), at4_1(0:KCMAX), &
                        at4_2(0:KCMAX), at5(0:KCMAX), at6(0:KCMAX), at7, &
                        ai1(0:KCMAX), ai2(0:KCMAX), &
                        atr1, alb1
real(dp), intent(in) :: dtime_temp, dtt_2dxi, dtt_2deta

integer(i4b) :: kc, kt, kr
real(dp) :: ct1(0:KCMAX), ct2(0:KCMAX), ct3(0:KCMAX), ct4(0:KCMAX), &
            ct5(0:KCMAX), ct6(0:KCMAX), ct7(0:KCMAX), ctr1, clb1
real(dp) :: ct1_sg(0:KCMAX), ct2_sg(0:KCMAX), ct3_sg(0:KCMAX), &
            ct4_sg(0:KCMAX), adv_vert_sg(0:KCMAX), abs_adv_vert_sg(0:KCMAX)
real(dp) :: ci1(0:KCMAX), ci2(0:KCMAX)
real(dp) :: temp_c_help(0:KCMAX)
real(dp) :: vx_c_help, vy_c_help
real(dp) :: adv_vert_help
real(dp) :: dtt_dxi, dtt_deta
real(dp) :: lgs_a0(0:KCMAX+KTMAX+KRMAX+IMAX+JMAX), &
            lgs_a1(0:KCMAX+KTMAX+KRMAX+IMAX+JMAX), &
            lgs_a2(0:KCMAX+KTMAX+KRMAX+IMAX+JMAX), &
            lgs_x(0:KCMAX+KTMAX+KRMAX+IMAX+JMAX), &
            lgs_b(0:KCMAX+KTMAX+KRMAX+IMAX+JMAX)
real(dp), parameter :: zero=0.0_dp

!-------- Check for boundary points --------

if ((i == 0).or.(i == IMAX).or.(j == 0).or.(j == JMAX)) &
   stop ' calc_temp_ssa: Boundary points not allowed.'

!-------- Abbreviations --------

ctr1 = atr1
clb1 = alb1*q_geo(j,i)

#if ADV_VERT==1

do kc=1, KCMAX-1
   ct1(kc) = at1(kc)/H_c(j,i)*0.5_dp*(vz_c(kc,j,i)+vz_c(kc-1,j,i))
end do

kc=0
ct1_sg(kc) = 0.5_dp*(at1(kc)+at1(kc+1))/H_c(j,i)*vz_c(kc,j,i)
             ! only needed for kc=0 ...
kc=KCMAX-1
ct1_sg(kc) = 0.5_dp*(at1(kc)+at1(kc+1))/H_c(j,i)*vz_c(kc,j,i)
             ! ... and kc=KCMAX-1

#elif ( ADV_VERT==2 || ADV_VERT==3 )

do kc=0, KCMAX-1
   ct1_sg(kc) = 0.5_dp*(at1(kc)+at1(kc+1))/H_c(j,i)*vz_c(kc,j,i)
end do

#endif

do kc=0, KCMAX

   ct2(kc) = ( at2_1(kc)*dzm_dtau(j,i) &
           +at2_2(kc)*dH_c_dtau(j,i) )/H_c(j,i)
   ct3(kc) = ( at3_1(kc)*dzm_dxi_g(j,i) &
           +at3_2(kc)*dH_c_dxi_g(j,i) )/H_c(j,i) &
          *0.5_dp*(vx_c(kc,j,i)+vx_c(kc,j,i-1)) *insq_g11_g(j,i)
   ct4(kc) = ( at4_1(kc)*dzm_deta_g(j,i) &
            +at4_2(kc)*dH_c_deta_g(j,i) )/H_c(j,i) &
          *0.5_dp*(vy_c(kc,j,i)+vy_c(kc,j-1,i)) *insq_g22_g(j,i)
   ct5(kc) = at5(kc) &
             /c_val(temp_c(kc,j,i)) &
             /H_c(j,i)
   ct7(kc) = 2.0_dp*at7 &
             /c_val(temp_c(kc,j,i)) &
             *viscosity(de_ssa(j,i), &
                        temp_c(kc,j,i), temp_c_m(kc,j,i), 0.0_dp, &
                        enh_c(kc,j,i), 0_i2b) &
             *de_ssa(j,i)**2
   ci1(kc) = ai1(kc)/H_c(j,i)

end do

#if ADV_VERT==1

kc=0
ct2_sg(kc) = 0.5_dp*(ct2(kc)+ct2(kc+1))
ct3_sg(kc) = 0.5_dp*(ct3(kc)+ct3(kc+1))
ct4_sg(kc) = 0.5_dp*(ct4(kc)+ct4(kc+1))
adv_vert_sg(kc) = ct1_sg(kc)-ct2_sg(kc)-ct3_sg(kc)-ct4_sg(kc)
abs_adv_vert_sg(kc) = abs(adv_vert_sg(kc))   ! only needed for kc=0 ...
kc=KCMAX-1
ct2_sg(kc) = 0.5_dp*(ct2(kc)+ct2(kc+1))
ct3_sg(kc) = 0.5_dp*(ct3(kc)+ct3(kc+1))
ct4_sg(kc) = 0.5_dp*(ct4(kc)+ct4(kc+1))
adv_vert_sg(kc) = ct1_sg(kc)-ct2_sg(kc)-ct3_sg(kc)-ct4_sg(kc)
abs_adv_vert_sg(kc) = abs(adv_vert_sg(kc))   ! ... and kc=KCMAX-1

#elif ( ADV_VERT==2 || ADV_VERT==3 )

do kc=0, KCMAX-1
   ct2_sg(kc) = 0.5_dp*(ct2(kc)+ct2(kc+1))
   ct3_sg(kc) = 0.5_dp*(ct3(kc)+ct3(kc+1))
   ct4_sg(kc) = 0.5_dp*(ct4(kc)+ct4(kc+1))
   adv_vert_sg(kc) = ct1_sg(kc)-ct2_sg(kc)-ct3_sg(kc)-ct4_sg(kc)
   abs_adv_vert_sg(kc) = abs(adv_vert_sg(kc))
end do

#endif

do kc=0, KCMAX-1
   temp_c_help(kc) = 0.5_dp*(temp_c(kc,j,i)+temp_c(kc+1,j,i))
   ct6(kc) = at6(kc) &
    *kappa_val(temp_c_help(kc)) &
    /H_c(j,i)
   ci2(kc) = ai2(kc)/H_c(j,i)
end do

#if (ADV_HOR==3)
dtt_dxi  = 2.0_dp*dtt_2dxi
dtt_deta = 2.0_dp*dtt_2deta
#endif

!-------- Set up the equations for the bedrock temperature --------

kr=0
lgs_a1(kr) = 1.0_dp
lgs_a2(kr) = -1.0_dp
lgs_b(kr)    = clb1

#if Q_LITHO==1
!   (coupled heat-conducting bedrock)

do kr=1, KRMAX-1
   lgs_a0(kr) = - ctr1
   lgs_a1(kr) = 1.0_dp + 2.0_dp*ctr1
   lgs_a2(kr) = - ctr1
   lgs_b(kr)    = temp_r(kr,j,i)
end do

#elif Q_LITHO==0
!   (no coupled heat-conducting bedrock)

do kr=1, KRMAX-1
   lgs_a0(kr) = 1.0_dp
   lgs_a1(kr) = 0.0_dp
   lgs_a2(kr) = -1.0_dp
   lgs_b(kr)  = 2.0_dp*clb1
end do

#endif

kr=KRMAX
lgs_a0(kr) = 0.0_dp
lgs_a1(kr) = 1.0_dp
lgs_b(kr)  = temp_c_m(0,j,i)-DELTA_TM_SW

!-------- Solve system of linear equations --------

call tri_sle(lgs_a0, lgs_a1, lgs_a2, lgs_x, lgs_b, KRMAX)

!-------- Assign the result --------

do kr=0, KRMAX
   temp_r_neu(kr,j,i) = lgs_x(kr)
end do

!-------- Set up the equations for the ice temperature --------

kc=0
lgs_a1(kc) = 1.0_dp
lgs_a2(kc) = 0.0_dp
lgs_b(kc)  = temp_c_m(0,j,i)-DELTA_TM_SW

do kc=1, KCMAX-1

#if ADV_VERT==1

   lgs_a0(kc) = -0.5_dp*(ct1(kc)-ct2(kc)-ct3(kc)-ct4(kc)) &
                      -ct5(kc)*ct6(kc-1)
   lgs_a1(kc) = 1.0_dp+ct5(kc)*(ct6(kc)+ct6(kc-1))
   lgs_a2(kc) = 0.5_dp*(ct1(kc)-ct2(kc)-ct3(kc)-ct4(kc)) &
                      -ct5(kc)*ct6(kc)

#elif ADV_VERT==2

   lgs_a0(kc) &
         = -0.5_dp*(adv_vert_sg(kc-1)+abs_adv_vert_sg(kc-1)) &
           -ct5(kc)*ct6(kc-1)
   lgs_a1(kc) &
         = 1.0_dp &
           +0.5_dp*(adv_vert_sg(kc-1)+abs_adv_vert_sg(kc-1)) &
           -0.5_dp*(adv_vert_sg(kc)  -abs_adv_vert_sg(kc)  ) &
           +ct5(kc)*(ct6(kc)+ct6(kc-1))
   lgs_a2(kc) &
         =  0.5_dp*(adv_vert_sg(kc)  -abs_adv_vert_sg(kc)  ) &
           -ct5(kc)*ct6(kc)

#elif ADV_VERT==3

   adv_vert_help = 0.5_dp*(adv_vert_sg(kc)+adv_vert_sg(kc-1))

   lgs_a0(kc) &
         = -max(adv_vert_help, 0.0_dp) &
           -ct5(kc)*ct6(kc-1)
   lgs_a1(kc) &
         = 1.0_dp &
           +max(adv_vert_help, 0.0_dp)-min(adv_vert_help, 0.0_dp) &
           +ct5(kc)*(ct6(kc)+ct6(kc-1))
   lgs_a2(kc) &
         =  min(adv_vert_help, 0.0_dp) &
           -ct5(kc)*ct6(kc)

#endif

#if ADV_HOR==2

   lgs_b(kc) = temp_c(kc,j,i) + ct7(kc) &
       -dtt_2dxi* &
          ( (vx_c(kc,j,i)-abs(vx_c(kc,j,i))) &
            *(temp_c(kc,j,i+1)-temp_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +(vx_c(kc,j,i-1)+abs(vx_c(kc,j,i-1))) &
            *(temp_c(kc,j,i)-temp_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_2deta* &
          ( (vy_c(kc,j,i)-abs(vy_c(kc,j,i))) &
            *(temp_c(kc,j+1,i)-temp_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +(vy_c(kc,j-1,i)+abs(vy_c(kc,j-1,i))) &
            *(temp_c(kc,j,i)-temp_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#elif ADV_HOR==3

   vx_c_help = 0.5_dp*(vx_c(kc,j,i)+vx_c(kc,j,i-1))
   vy_c_help = 0.5_dp*(vy_c(kc,j,i)+vy_c(kc,j-1,i))

   lgs_b(kc) = temp_c(kc,j,i) + ct7(kc) &
       -dtt_dxi* &
          ( min(vx_c_help, 0.0_dp) &
            *(temp_c(kc,j,i+1)-temp_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +max(vx_c_help, 0.0_dp) &
            *(temp_c(kc,j,i)-temp_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_deta* &
          ( min(vy_c_help, 0.0_dp) &
            *(temp_c(kc,j+1,i)-temp_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +max(vy_c_help, 0.0_dp) &
            *(temp_c(kc,j,i)-temp_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#endif

end do

kc=KCMAX
lgs_a0(kc) = 0.0_dp
lgs_a1(kc) = 1.0_dp
lgs_b(kc)  = temp_s(j,i)

!-------- Solve system of linear equations --------

call tri_sle(lgs_a0, lgs_a1, lgs_a2, lgs_x, lgs_b, KCMAX)

!-------- Assign the result --------

do kc=0, KCMAX
   temp_c_neu(kc,j,i) = lgs_x(kc)
end do

!-------- Set water content in the non-existing temperate layer
!         to zero --------

do kt=0, KTMAX
   omega_t_neu(kt,j,i) = 0.0_dp
end do

!-------- Water drainage from the non-existing temperate layer --------

Q_tld(j,i) = 0.0_dp

!-------- Set up the equations for the age of cold ice --------

kc=0                                                 ! adv_vert_sg(0) <= 0
lgs_a1(kc) = 1.0_dp - min(adv_vert_sg(kc), 0.0_dp)   ! (directed downward)
lgs_a2(kc) = min(adv_vert_sg(kc), 0.0_dp)            ! assumed/enforced

#if ADV_HOR==2

lgs_b(kc) = age_c(kc,j,i) + dtime_temp &
       -dtt_2dxi* &
          ( (vx_c(kc,j,i)-abs(vx_c(kc,j,i))) &
            *(age_c(kc,j,i+1)-age_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +(vx_c(kc,j,i-1)+abs(vx_c(kc,j,i-1))) &
            *(age_c(kc,j,i)-age_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_2deta* &
          ( (vy_c(kc,j,i)-abs(vy_c(kc,j,i))) &
            *(age_c(kc,j+1,i)-age_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +(vy_c(kc,j-1,i)+abs(vy_c(kc,j-1,i))) &
            *(age_c(kc,j,i)-age_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#elif ADV_HOR==3

vx_c_help = 0.5_dp*(vx_c(kc,j,i)+vx_c(kc,j,i-1))
vy_c_help = 0.5_dp*(vy_c(kc,j,i)+vy_c(kc,j-1,i))

lgs_b(kc) = age_c(kc,j,i) + dtime_temp &
       -dtt_dxi* &
          ( min(vx_c_help, 0.0_dp) &
            *(age_c(kc,j,i+1)-age_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +max(vx_c_help, 0.0_dp) &
            *(age_c(kc,j,i)-age_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_deta* &
          ( min(vy_c_help, 0.0_dp) &
            *(age_c(kc,j+1,i)-age_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +max(vy_c_help, 0.0_dp) &
            *(age_c(kc,j,i)-age_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#endif

do kc=1, KCMAX-1

#if ADV_VERT==1

   lgs_a0(kc) = -0.5_dp*(ct1(kc)-ct2(kc)-ct3(kc)-ct4(kc)) &
                      -ci1(kc)*ci2(kc-1)
   lgs_a1(kc) = 1.0_dp+ci1(kc)*(ci2(kc)+ci2(kc-1))
   lgs_a2(kc) = 0.5_dp*(ct1(kc)-ct2(kc)-ct3(kc)-ct4(kc)) &
                      -ci1(kc)*ci2(kc)

#elif ADV_VERT==2

   lgs_a0(kc) = -0.5_dp*(adv_vert_sg(kc-1)+abs_adv_vert_sg(kc-1))
   lgs_a1(kc) =  1.0_dp &
                +0.5_dp*(adv_vert_sg(kc-1)+abs_adv_vert_sg(kc-1)) &
                -0.5_dp*(adv_vert_sg(kc)  -abs_adv_vert_sg(kc)  )
   lgs_a2(kc) =  0.5_dp*(adv_vert_sg(kc)  -abs_adv_vert_sg(kc)  )

#elif ADV_VERT==3

   adv_vert_help = 0.5_dp*(adv_vert_sg(kc)+adv_vert_sg(kc-1))

   lgs_a0(kc) = -max(adv_vert_help, 0.0_dp)
   lgs_a1(kc) =  1.0_dp &
                +max(adv_vert_help, 0.0_dp)-min(adv_vert_help, 0.0_dp)
   lgs_a2(kc) =  min(adv_vert_help, 0.0_dp)

#endif

#if ADV_HOR==2

   lgs_b(kc) = age_c(kc,j,i) + dtime_temp &
       -dtt_2dxi* &
          ( (vx_c(kc,j,i)-abs(vx_c(kc,j,i))) &
            *(age_c(kc,j,i+1)-age_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +(vx_c(kc,j,i-1)+abs(vx_c(kc,j,i-1))) &
            *(age_c(kc,j,i)-age_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_2deta* &
          ( (vy_c(kc,j,i)-abs(vy_c(kc,j,i))) &
            *(age_c(kc,j+1,i)-age_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +(vy_c(kc,j-1,i)+abs(vy_c(kc,j-1,i))) &
            *(age_c(kc,j,i)-age_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#elif ADV_HOR==3

   vx_c_help = 0.5_dp*(vx_c(kc,j,i)+vx_c(kc,j,i-1))
   vy_c_help = 0.5_dp*(vy_c(kc,j,i)+vy_c(kc,j-1,i))

   lgs_b(kc) = age_c(kc,j,i) + dtime_temp &
       -dtt_dxi* &
          ( min(vx_c_help, 0.0_dp) &
            *(age_c(kc,j,i+1)-age_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +max(vx_c_help, 0.0_dp) &
            *(age_c(kc,j,i)-age_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_deta* &
          ( min(vy_c_help, 0.0_dp) &
            *(age_c(kc,j+1,i)-age_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +max(vy_c_help, 0.0_dp) &
            *(age_c(kc,j,i)-age_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#endif

end do

kc=KCMAX
if (as_perp(j,i) >= zero) then
   lgs_a0(kc) = 0.0_dp
   lgs_a1(kc) = 1.0_dp
   lgs_b(kc)  = 0.0_dp
else
   lgs_a0(kc) = -max(adv_vert_sg(kc-1), 0.0_dp)
   lgs_a1(kc) = 1.0_dp + max(adv_vert_sg(kc-1), 0.0_dp)
                       ! adv_vert_sg(KCMAX-1) >= 0 (directed upward)
                       ! assumed/enforced
#if ADV_HOR==2

   lgs_b(kc) = age_c(kc,j,i) + dtime_temp &
       -dtt_2dxi* &
          ( (vx_c(kc,j,i)-abs(vx_c(kc,j,i))) &
            *(age_c(kc,j,i+1)-age_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +(vx_c(kc,j,i-1)+abs(vx_c(kc,j,i-1))) &
            *(age_c(kc,j,i)-age_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_2deta* &
          ( (vy_c(kc,j,i)-abs(vy_c(kc,j,i))) &
            *(age_c(kc,j+1,i)-age_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +(vy_c(kc,j-1,i)+abs(vy_c(kc,j-1,i))) &
            *(age_c(kc,j,i)-age_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#elif ADV_HOR==3

   vx_c_help = 0.5_dp*(vx_c(kc,j,i)+vx_c(kc,j,i-1))
   vy_c_help = 0.5_dp*(vy_c(kc,j,i)+vy_c(kc,j-1,i))

   lgs_b(kc) = age_c(kc,j,i) + dtime_temp &
       -dtt_dxi* &
          ( min(vx_c_help, 0.0_dp) &
            *(age_c(kc,j,i+1)-age_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +max(vx_c_help, 0.0_dp) &
            *(age_c(kc,j,i)-age_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_deta* &
          ( min(vy_c_help, 0.0_dp) &
            *(age_c(kc,j+1,i)-age_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +max(vy_c_help, 0.0_dp) &
            *(age_c(kc,j,i)-age_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#endif

end if

!-------- Solve system of linear equations --------

call tri_sle(lgs_a0, lgs_a1, lgs_a2, lgs_x, lgs_b, KCMAX)

!-------- Assign the result,
!         restriction to interval [0, AGE_MAX yr] --------

do kc=0, KCMAX

   age_c_neu(kc,j,i) = lgs_x(kc)

   if (age_c_neu(kc,j,i) < (AGE_MIN*YEAR_SEC)) &
                           age_c_neu(kc,j,i) = 0.0_dp
   if (age_c_neu(kc,j,i) > (AGE_MAX*YEAR_SEC)) &
                           age_c_neu(kc,j,i) = AGE_MAX*YEAR_SEC

end do

!-------- Age of the ice in the non-existing temperate layer --------

do kt=0, KTMAX
   age_t_neu(kt,j,i) = age_c_neu(0,j,i)
end do

end subroutine calc_temp_ssa
!
