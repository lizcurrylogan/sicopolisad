!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  Subroutine :  c a l c _ t o p _ 2
!
!> @file
!!
!! Computation of the ice topography (including gradients) for
!! hybrid SIA/SStA dynamics of ice sheets without ice shelves.
!!
!! @section Copyright
!!
!! Copyright 2009-2016 Ralf Greve, Tatsuru Sato
!!
!! @section License
!!
!! This file is part of SICOPOLIS.
!!
!! SICOPOLIS is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! SICOPOLIS is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with SICOPOLIS.  If not, see <http://www.gnu.org/licenses/>.
!<
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!-------------------------------------------------------------------------------
!> Computation of the ice topography (including gradients) for
!! hybrid SIA/SStA dynamics of ice sheets without ice shelves.
!<------------------------------------------------------------------------------
subroutine calc_top_2(time, z_sl, z_mar, dtime, dxi, deta, mean_accum, &
                      ii, jj, nn)

use sico_types
use sico_variables
#ifndef ALLOW_OPENAD
use sico_sle_solvers, only : sor_sprs
#else
use sico_sle_solvers
#endif
implicit none

integer(i4b), intent(inout) :: ii((IMAX+1)*(JMAX+1)), jj((IMAX+1)*(JMAX+1))
integer(i4b), intent(in) :: nn(0:JMAX,0:IMAX)
real(dp),     intent(in) :: time
real(dp),     intent(in) :: z_sl, z_mar
real(dp),     intent(in) :: dtime, dxi, deta
real(dp),     intent(in) :: mean_accum

integer(i4b) :: i, j, kc, kt
integer(i4b) :: k, nnz
real(dp) :: eps_sor
real(dp) :: year_sec_inv
real(dp) :: dtime_inv
real(dp) :: dt_dxi, dt_deta
real(dp), dimension(0:JMAX,0:IMAX) :: H, H_neu, H_sea_neu
real(dp) :: rhosw_rho_ratio
real(dp) :: vx_m_help, vy_m_help
real(dp) :: vx_m_1, vx_m_2, vy_m_1, vy_m_2
real(dp) :: upH_x_1, upH_x_2, upH_y_1, upH_y_2
real(dp) :: calv_uw_coeff, r1_calv_uw, r2_calv_uw
real(dp) :: target_topo_time_lag, target_topo_weight_lin
logical  :: flag_calving_event

real(dp), parameter :: H_isol_max = 1.0e+03_dp
                       ! Thickness limit of isolated glaciated points

#if (CALCZS==3)

integer(i4b) :: ierr
integer(i4b) :: iter
integer(i4b) :: nc, nr
integer(i4b), parameter                 :: nmax   =    (IMAX+1)*(JMAX+1)
integer(i4b), parameter                 :: n_sprs = 10*(IMAX+1)*(JMAX+1)
#ifndef ALLOW_OPENAD
integer(i4b), allocatable, dimension(:) :: lgs_a_ptr, lgs_a_index
integer(i4b), allocatable, dimension(:) :: lgs_a_diag_index
integer(i4b), allocatable, dimension(:) :: lgs_a_index_trim
real(dp), allocatable, dimension(:) :: lgs_a_value, lgs_b_value, lgs_x_value
real(dp), allocatable, dimension(:) :: lgs_a_value_trim
#else
real(dp), dimension(n_sprs) :: lgs_a_index
integer(i4b), dimension(nmax+1) :: lgs_a_ptr
integer(i4b), dimension(nmax) :: lgs_a_diag_index
integer(i4b), dimension(n_sprs) :: lgs_a_index_trim
real(dp), dimension(n_sprs) :: lgs_a_value
real(dp), dimension(nmax) :: lgs_b_value
real(dp), dimension(nmax) :: lgs_x_value
real(dp), dimension(n_sprs) :: lgs_a_value_trim
#endif
#elif (CALCZS==4)

LIS_INTEGER :: ierr
LIS_INTEGER :: iter
LIS_INTEGER :: nc, nr
LIS_INTEGER, parameter                 :: nmax   =    (IMAX+1)*(JMAX+1)
LIS_INTEGER, parameter                 :: n_sprs = 10*(IMAX+1)*(JMAX+1)
LIS_INTEGER, allocatable, dimension(:) :: lgs_a_ptr, lgs_a_index
LIS_INTEGER, allocatable, dimension(:) :: lgs_a_diag_index
LIS_MATRIX :: lgs_a
LIS_VECTOR :: lgs_b, lgs_x
LIS_SCALAR, allocatable, dimension(:) :: lgs_a_value, lgs_b_value, lgs_x_value
LIS_SOLVER :: solver
character(len=256) :: ch_solver_set_option

#endif

!-------- Term abbreviations --------

year_sec_inv = 1.0_dp/YEAR_SEC

rhosw_rho_ratio = RHO_SW/RHO

dtime_inv = 1.0_dp/dtime

dt_dxi  = dtime/dxi
dt_deta = dtime/deta

!-------- Computation of zb_neu (updated ice base topography)
!                                     and its time derivative --------

zb_neu   = zl_neu
dzb_dtau = dzl_dtau

!-------- Computation of H_neu (updated ice thickness) --------

H = H_c + H_t

zs_neu = zs   ! initialisation, will be overwritten later
H_neu  = H    ! initialisation, will be overwritten later

!  ------ Explicit scheme

#if (CALCZS==0)

!    ---- Abbreviations

do i=1, IMAX-1
do j=1, JMAX-1

#if (MARGIN==1)

   if (maske(j,i) <= 1_i2b) then   ! grounded ice or ice-free land

#endif

      vx_m_help = 0.5_dp*(vx_m(j,i)+vx_m(j,i-1))
      vy_m_help = 0.5_dp*(vy_m(j,i)+vy_m(j-1,i))

      vx_m_1    = min(vx_m_help, 0.0_dp)
      vx_m_2    = max(vx_m_help, 0.0_dp)
      vy_m_1    = min(vy_m_help, 0.0_dp)
      vy_m_2    = max(vy_m_help, 0.0_dp)

      upH_x_1   = H(j,i+1)-H(j,i  )
      upH_x_2   = H(j,i  )-H(j,i-1)
      upH_y_1   = H(j+1,i)-H(j  ,i)
      upH_y_2   = H(j  ,i)-H(j-1,i)

      H_neu(j,i) = H(j,i) &
                   + dtime*(as_perp(j,i)-Q_b_tot(j,i)) &
                   - dt_dxi &
                     * (   vx_m_1*upH_x_1 + vx_m_2*upH_x_2 &
                         + (vx_m(j,i)-vx_m(j,i-1))*H(j,i) ) &
                   - dt_deta &
                     * (   vy_m_1*upH_y_1 + vy_m_2*upH_y_2 &
                         + (vy_m(j,i)-vy_m(j-1,i))*H(j,i) )

#if (MARGIN==1)

   else   ! maske(j,i) == 2_i2b, sea
      H_neu(j,i) = 0.0_dp
   end if

#endif

end do
end do

do i=0, IMAX
   H_neu(0,i)    = 0.0_dp
   H_neu(JMAX,i) = 0.0_dp
end do

do j=0, JMAX
   H_neu(j,0)    = 0.0_dp
   H_neu(j,IMAX) = 0.0_dp
end do

!  ------ ADI scheme / over-implicit ADI scheme (DELETED)

#elif (CALCZS==1 || CALCZS==2)
#ifndef ALLOW_OPENAD
stop ' calc_top_2: Options CALCZS==1 or 2 not supported by this routine!'
#endif
!  ------ Over-implicit scheme

#elif (CALCZS==3 || CALCZS==4)

!    ---- Assembly of the system of linear equations
!                         (matrix storage: compressed sparse row CSR)

#ifndef ALLOW_OPENAD
allocate(lgs_a_value(n_sprs), lgs_a_index(n_sprs), lgs_a_ptr(nmax+1))
allocate(lgs_a_diag_index(nmax), lgs_b_value(nmax), lgs_x_value(nmax))
#endif

lgs_a_value = 0.0_dp
lgs_a_index = 0.0_dp
lgs_a_ptr   = 0

lgs_b_value = 0.0_dp
lgs_x_value = 0.0_dp

lgs_a_ptr(1) = 1

k = 0

do nr=1, nmax   ! loop over rows

   i = ii(nr)
   j = jj(nr)

   if ( &
        (i /= 0).and.(i /= IMAX).and.(j /= 0).and.(j /= JMAX) &   ! inner point
#if (MARGIN==1)
        .and.(maske(j,i) <= 1_i2b) &   ! grounded ice or ice-free land
#endif
      ) then

      vx_m_help = 0.5_dp*(vx_m(j,i)+vx_m(j,i-1))
      vy_m_help = 0.5_dp*(vy_m(j,i)+vy_m(j-1,i))

      vx_m_1    = min(vx_m_help, 0.0_dp)
      vx_m_2    = max(vx_m_help, 0.0_dp)
      vy_m_1    = min(vy_m_help, 0.0_dp)
      vy_m_2    = max(vy_m_help, 0.0_dp)

      upH_x_1   = H(j,i+1)-H(j,i  )
      upH_x_2   = H(j,i  )-H(j,i-1)
      upH_y_1   = H(j+1,i)-H(j  ,i)
      upH_y_2   = H(j  ,i)-H(j-1,i)

      k=k+1 ; nc=nn(j-1,i) ; lgs_a_index(k)=nc   ! for H(j-1,i)
      lgs_a_value(k) = -vy_m_2*dt_deta*OVI_WEIGHT !&
         ! *insq_g11_g(j,i)*insq_g22_g(j,i)

      k=k+1 ; nc=nn(j,i-1) ; lgs_a_index(k)=nc   ! for H(j,i-1)
      lgs_a_value(k) = -vx_m_2*dt_dxi *OVI_WEIGHT !&
         ! *insq_g11_g(j,i)*insq_g22_g(j,i)

      k=k+1 ; lgs_a_index(k)=nr ; lgs_a_diag_index(nr)=k  ! for H(j,i)
      lgs_a_value(k) = 1.0_dp &                           ! (diagonal element)
                       +OVI_WEIGHT &
                        *( dt_dxi &
                           * ( -vx_m_1+vx_m_2 &
                               +(vx_m(j,i)-vx_m(j,i-1)) ) &
                          +dt_deta &
                           * ( -vy_m_1+vy_m_2 &
                               +(vy_m(j,i)-vy_m(j-1,i)) ) ) !&
                             ! *insq_g11_g(j,i)*insq_g22_g(j,i)

      k=k+1 ; nc=nn(j,i+1) ; lgs_a_index(k)=nc   ! for H(j,i+1)
      lgs_a_value(k) =  vx_m_1*dt_dxi *OVI_WEIGHT !&
         ! *insq_g11_g(j,i)*insq_g22_g(j,i)

      k=k+1 ; nc=nn(j+1,i) ; lgs_a_index(k)=nc   ! for H(j+1,i)
      lgs_a_value(k) =  vy_m_1*dt_deta*OVI_WEIGHT !&
         ! *insq_g11_g(j,i)*insq_g22_g(j,i)

      lgs_b_value(nr) = H(j,i) &
                        +dtime*(as_perp(j,i)-Q_b_tot(j,i)) &
                        -(1.0_dp-OVI_WEIGHT) &
                         *( dt_dxi &
                            * (   vx_m_1*upH_x_1 + vx_m_2*upH_x_2 &
                                + (vx_m(j,i)-vx_m(j,i-1))*H(j,i) ) &
                           +dt_deta &
                            * (   vy_m_1*upH_y_1 + vy_m_2*upH_y_2 &
                                + (vy_m(j,i)-vy_m(j-1,i))*H(j,i) ) )
                                                          ! right-hand side

   else   ! zero-ice-thickness boundary condition

      k = k+1
      lgs_a_value(k)       = 1.0_dp   ! diagonal element only
      lgs_a_diag_index(nr) = k
      lgs_a_index(k)       = nr
      lgs_b_value(nr)      = 0.0_dp

   end if

   lgs_x_value(nr) = H(j,i)   ! old ice thickness,
                              ! initial guess for solution vector

   lgs_a_ptr(nr+1) = k+1   ! row is completed, store index to next row

end do

nnz = k   ! number of non-zero elements of the matrix

#if (CALCZS==3)

!    ---- Solution of the system of linear equations

#ifndef ALLOW_OPENAD
allocate(lgs_a_value_trim(nnz), lgs_a_index_trim(nnz))
#endif

do k=1, nnz   ! relocate matrix to trimmed arrays
   lgs_a_value_trim(k) = lgs_a_value(k)
   lgs_a_index_trim(k) = INT(lgs_a_index(k))
end do

#ifndef ALLOW_OPENAD
deallocate(lgs_a_value, lgs_a_index)
#endif

eps_sor = 1.0e-05_dp*mean_accum*dtime   ! convergence parameter

call sor_sprs(lgs_a_value_trim, &
              lgs_a_index_trim, lgs_a_diag_index, lgs_a_ptr, &
              lgs_b_value, &
              nnz, nmax,&
#ifdef ALLOW_OPENAD
              n_sprs, &
#endif
              OMEGA_SOR, eps_sor, lgs_x_value, ierr)

do nr=1, nmax
   i = ii(nr)
   j = jj(nr)
   H_neu(j,i) = lgs_x_value(nr)
end do

#ifndef ALLOW_OPENAD
deallocate(lgs_a_value_trim, lgs_a_index_trim, lgs_a_ptr)
deallocate(lgs_a_diag_index, lgs_b_value, lgs_x_value)
#endif 

#elif (CALCZS==4)

!    ---- Settings for Lis

call lis_matrix_create(LIS_COMM_WORLD, lgs_a, ierr)
call lis_vector_create(LIS_COMM_WORLD, lgs_b, ierr)
call lis_vector_create(LIS_COMM_WORLD, lgs_x, ierr)

call lis_matrix_set_size(lgs_a, 0, nmax, ierr)
call lis_vector_set_size(lgs_b, 0, nmax, ierr)
call lis_vector_set_size(lgs_x, 0, nmax, ierr)

do nr=1, nmax

   do nc=lgs_a_ptr(nr), lgs_a_ptr(nr+1)-1
      call lis_matrix_set_value(LIS_INS_VALUE, nr, lgs_a_index(nc), &
                                               lgs_a_value(nc), lgs_a, ierr)
   end do

   call lis_vector_set_value(LIS_INS_VALUE, nr, lgs_b_value(nr), lgs_b, ierr)
   call lis_vector_set_value(LIS_INS_VALUE, nr, lgs_x_value(nr), lgs_x, ierr)

end do

call lis_matrix_set_type(lgs_a, LIS_MATRIX_CSR, ierr)
call lis_matrix_assemble(lgs_a, ierr)

!    ---- Solution of the system of linear equations with Lis

call lis_solver_create(solver, ierr)

ch_solver_set_option = '-i bicg -p ilu '// &
                       '-maxiter 1000 -tol 1.0e-12 -initx_zeros false'
call lis_solver_set_option(trim(ch_solver_set_option), solver, ierr)

call lis_solve(lgs_a, lgs_b, lgs_x, solver, ierr)

call lis_solver_get_iters(solver, iter, ierr)
write(6,'(11x,a,i5)') 'calc_top_2: iter = ', iter

lgs_x_value = 0.0_dp
call lis_vector_gather(lgs_x, lgs_x_value, ierr)
call lis_matrix_destroy(lgs_a, ierr)
call lis_vector_destroy(lgs_b, ierr)
call lis_vector_destroy(lgs_x, ierr)
call lis_solver_destroy(solver, ierr)

do nr=1, nmax
   i = ii(nr)
   j = jj(nr)
   H_neu(j,i) = lgs_x_value(nr)
end do

deallocate(lgs_a_value, lgs_a_index, lgs_a_ptr)
deallocate(lgs_a_diag_index, lgs_b_value, lgs_x_value)

#endif

#endif

!  ------ Adjustment due to prescribed target topography

#if (ZS_EVOL==2)

if (time >= time_target_topo_final) then
   H_neu = H_target
else if (time >= time_target_topo_init) then
   H_neu =   ( target_topo_time_lag*H_neu + dtime*H_target ) &
           / ( target_topo_time_lag       + dtime )
end if

#endif

!-------- Update of the mask --------

zs_neu = zb_neu + H_neu  ! ice surface topography

#if ((MARGIN==2) \
      && (MARINE_ICE_FORMATION==2) \
      && (MARINE_ICE_CALVING==9))

calv_uw_coeff = CALV_UW_COEFF * year_sec_inv
r1_calv_uw    = R1_CALV_UW
r2_calv_uw    = R2_CALV_UW

#if (ZS_EVOL==2)
if (time >= time_target_topo_final) then
   calv_uw_coeff = 0.0_dp
                   ! adjustment due to prescribed target topography
else if (time >= time_target_topo_init) then
   target_topo_weight_lin = (time-time_target_topo_init) &
                            /(time_target_topo_final-time_target_topo_init)
   calv_uw_coeff = (1.0_dp-target_topo_weight_lin)*calv_uw_coeff
                   ! adjustment due to prescribed target topography
end if
#endif

#endif

#if (ZS_EVOL>=1)

do i=0, IMAX
do j=0, JMAX

   if (maske(j,i) <= 1_i2b) then   ! grounded ice or ice-free land

      H_sea_neu(j,i) = z_sl-zl_neu(j,i)   ! sea depth (only makes sense for
                                          ! marine ice and "underwater ice",
                                          ! otherwise meaningless)

      if (H_neu(j,i) >= H_MIN) then
         maske(j,i) = 0_i2b   ! grounded ice
#if ((MARGIN==2) \
      && (MARINE_ICE_FORMATION==2) \
      && (MARINE_ICE_CALVING==9))
         if ( H_neu(j,i) < (rhosw_rho_ratio*H_sea_neu(j,i)) ) then
                                                          ! "underwater ice"
            H_neu(j,i) = H_neu(j,i) - ( calv_uw_coeff &
                                        *H_neu(j,i)**r1_calv_uw &
                                        *H_sea_neu(j,i)**r2_calv_uw ) * dtime
                                               ! calving of "underwater ice"
            zs_neu(j,i) = zb_neu(j,i) + H_neu(j,i)
            if (H_neu(j,i) < H_MIN) maske(j,i) = 2_i2b   ! sea
         end if
#endif
      else
         maske(j,i) = 1_i2b   ! ice-free land
      end if

#if (MARGIN==2)

   else   ! (maske(j,i) == 2_i2b); sea

      H_sea_neu(j,i) = z_sl-zl_neu(j,i)   ! sea depth

      if (H_neu(j,i) >= H_MIN) then

         if ( H_neu(j,i) < (rhosw_rho_ratio*H_sea_neu(j,i)) ) then

#if (MARINE_ICE_FORMATION==1)
            maske(j,i) = 2_i2b   ! floating ice cut off -> sea
#elif (MARINE_ICE_FORMATION==2)
            maske(j,i) = 0_i2b   ! "underwater ice"
#if (MARINE_ICE_CALVING==9)
            H_neu(j,i) = H_neu(j,i) - ( calv_uw_coeff &
                                        *H_neu(j,i)**r1_calv_uw &
                                        *H_sea_neu(j,i)**r2_calv_uw ) * dtime
                                               ! calving of "underwater ice"
            zs_neu(j,i) = zb_neu(j,i) + H_neu(j,i)
            if (H_neu(j,i) < H_MIN) maske(j,i) = 2_i2b   ! sea
#endif
#endif
         else
            maske(j,i) = 0_i2b   ! grounded ice
         end if

#if (MARINE_ICE_CALVING==2 \
      || MARINE_ICE_CALVING==4 \
      || MARINE_ICE_CALVING==6)
         if (zl0(j,i) < z_mar) maske(j,i) = 2_i2b   ! sea
#elif (MARINE_ICE_CALVING==3 \
        || MARINE_ICE_CALVING==5 \
        || MARINE_ICE_CALVING==7)
         if (zl_neu(j,i) < z_mar) maske(j,i) = 2_i2b   ! sea
#endif

      else
         maske(j,i) = 2_i2b   ! sea
      end if

#endif

   end if

end do
end do

#endif

!  ------ Adjustment due to prescribed target topography

#if (ZS_EVOL==2)
if (time >= time_target_topo_final) maske = maske_target
#endif

!  ------ Adjustment due to prescribed maximum ice extent

#if (ZS_EVOL==3)

do i=0, IMAX
do j=0, JMAX

   if (maske_maxextent(j,i)==0_i2b) then   ! not allowed to glaciate

      if (maske(j,i)==0_i2b) then
         maske(j,i) = 1_i2b   ! grounded ice -> ice-free land
      else if (maske(j,i)==3_i2b) then
         maske(j,i) = 2_i2b   ! floating ice -> sea
      end if

   end if

end do
end do

#endif

!-------- Detection of the grounding line and the calving front
!                   (not relevant for hybrid SIA/SStA dynamics
!                                 of ice sheets without ice shelves) --------

flag_grounding_line_1 = .false.
flag_grounding_line_2 = .false.

flag_calving_front_1  = .false.
flag_calving_front_2  = .false.

!-------- Correction of zs_neu and zb_neu for ice-free land and sea --------

do i=0, IMAX
do j=0, JMAX

   if (maske(j,i) == 1_i2b) then   ! ice-free land

      zs_neu(j,i) = zb_neu(j,i)   ! this prevents zs_neu(j,i)
      H_neu(j,i)  = 0.0_dp        ! from being below zb_neu(j,i)

   else if (maske(j,i) == 2_i2b) then   ! sea

      zs_neu(j,i) = zb_neu(j,i)   ! this prevents zs_neu(j,i)
      H_neu(j,i)  = 0.0_dp        ! from being below zb_neu(j,i)

   else if (maske(j,i) == 3_i2b) then   ! floating ice

      write(6, fmt='(a)') ' calc_top_2: maske(j,i)==3 not allowed for'
      write(6, fmt='(a)') '             hybrid SIA/SStA dynamics of ice sheets'
      write(6, fmt='(a)') '             without ice shelves!'
#ifndef ALLOW_OPENAD
      stop
#endif
   end if

end do
end do

!-------- Computation of further quantities --------

#if (ZS_EVOL==0)

do i=0, IMAX
do j=0, JMAX

   if (maske(j,i) == 0_i2b) then   ! grounded ice

      as_perp(j,i) = as_perp(j,i) - (zs_neu(j,i)-zs(j,i))*dtime_inv
                         ! correction of the accumulation-ablation function
                         ! such that it is consistent with a steady surface

   else   ! maske(j,i)==1_i2b or 2_i2b, ice-free land or sea

      as_perp(j,i) = 0.0_dp

   end if

   zs_neu(j,i)  = zs(j,i)                   ! newly computed surface topography
   H_neu(j,i)   = zs_neu(j,i)-zb_neu(j,i)   ! is discarded

end do
end do

#endif

do i=0, IMAX
do j=0, JMAX

   if (maske(j,i) == 0_i2b) then   ! grounded ice

!  ------ Limit the thickness of isolated glaciated points to H_isol_max

      if ( ((maske(j,i+1) == 1_i2b).or.(maske(j,i+1) == 2_i2b)) &
           .and. ((maske(j,i-1) == 1_i2b).or.(maske(j,i-1) == 2_i2b)) &
           .and. ((maske(j+1,i) == 1_i2b).or.(maske(j+1,i) == 2_i2b)) &
           .and. ((maske(j-1,i) == 1_i2b).or.(maske(j-1,i) == 2_i2b)) &
         ) then   ! all nearest neighbours ice-free
         H_neu(j,i)   = min(H_neu(j,i), H_isol_max)
         zs_neu(j,i)  = zb_neu(j,i)+H_neu(j,i)
      end if

!  ------ Further stuff

      if (n_cts(j,i) == 1) then
         H_t_neu(j,i) = H_t(j,i) * H_neu(j,i)/H(j,i)
         zm_neu(j,i)  = zb_neu(j,i)+H_t_neu(j,i)
         H_c_neu(j,i) = zs_neu(j,i)-zm_neu(j,i)
      else
         H_t_neu(j,i) = 0.0_dp
         zm_neu(j,i)  = zb_neu(j,i)
         H_c_neu(j,i) = zs_neu(j,i)-zm_neu(j,i)
      end if

   else   ! maske(j,i) == 1_i2b or 2_i2b, ice-free land or sea

      zs_neu(j,i) = zb_neu(j,i)
      zm_neu(j,i) = zb_neu(j,i)
      H_c_neu(j,i) = 0.0_dp
      H_t_neu(j,i) = 0.0_dp
      H_neu(j,i)   = 0.0_dp

   end if

   dzs_dtau(j,i)  = (zs_neu(j,i)-zs(j,i))*dtime_inv
   dzb_dtau(j,i)  = (zb_neu(j,i)-zb(j,i))*dtime_inv
   dzm_dtau(j,i)  = dH_t_dtau(j,i)+dzb_dtau(j,i)
   dH_c_dtau(j,i) = dzs_dtau(j,i)-dzm_dtau(j,i)

#if (ZS_EVOL==2)
   if ( abs((time-time_target_topo_final)*year_sec_inv) < eps ) then
      dzs_dtau       = 0.0_dp
      dzb_dtau       = 0.0_dp
      dzm_dtau       = 0.0_dp
      dH_c_dtau(j,i) = 0.0_dp
      dH_t_dtau(j,i) = 0.0_dp
   end if
#endif

end do
end do

!-------- New gradients --------

#if (TOPOGRAD==0)
call topograd_1(dxi, deta, 2)
#elif (TOPOGRAD==1)
call topograd_2(dxi, deta, 2)
#endif

!-------- Compute the volume flux across the CTS, am_perp --------

#if (CALCMOD==1)

do i=1, IMAX-1
do j=1, JMAX-1

   if ( (maske(j,i) == 0_i2b).and.(n_cts(j,i) == 1_i2b) ) then

      am_perp_st(j,i) = &
                0.5_dp*(vx_c(0,j,i)+vx_c(0,j,i-1))*dzm_dxi_g(j,i) &
              + 0.5_dp*(vy_c(0,j,i)+vy_c(0,j-1,i))*dzm_deta_g(j,i) &
              - 0.5_dp*(vz_c(0,j,i)+vz_t(KTMAX-1,j,i))
      am_perp(j,i) = am_perp_st(j,i) + dzm_dtau(j,i)

   else
      am_perp_st(j,i) = 0.0_dp
      am_perp(j,i)    = 0.0_dp
   end if

end do
end do

#endif

end subroutine calc_top_2
!
