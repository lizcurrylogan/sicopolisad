!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  Subroutine :  o u t p u t 4
!
!> @file
!!
!! Writing of time-series data of the deep ice cores on file in ASCII format.
!!
!! @section Copyright
!!
!! Copyright 2009-2016 Ralf Greve
!!
!! @section License
!!
!! This file is part of SICOPOLIS.
!!
!! SICOPOLIS is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! SICOPOLIS is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with SICOPOLIS.  If not, see <http://www.gnu.org/licenses/>.
!<
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!-------------------------------------------------------------------------------
!> Writing of time-series data of the deep ice cores on file in ASCII format.
!<------------------------------------------------------------------------------
subroutine output4(time, dxi, deta, delta_ts, glac_index, z_sl)

use sico_types
use sico_variables

implicit none
real(dp), intent(in) :: time, dxi, deta, delta_ts, glac_index, z_sl

integer(i4b) :: i, j, n
real(dp) :: time_val
real(dp), dimension(0:JMAX,0:IMAX) :: field
real(dp), dimension(:), allocatable :: H_core, temp_core, &
                                       vx_core, vy_core, v_core, &
                                       Rbx_core, Rby_core, Rb_core

allocate(H_core(n_core), temp_core(n_core), &
         vx_core(n_core), vy_core(n_core), v_core(n_core), &
         Rbx_core(n_core), Rby_core(n_core), Rb_core(n_core))

!-------- Determination of ice-core data --------

do n=1, n_core

!  ------ Ice thickness

   field = H_c + H_t
   call borehole(field, x_core(n), y_core(n), dxi, deta, 'grid', H_core(n))

!  ------ Surface velocity

   do i=0, IMAX
   do j=0, JMAX
      field(j,i) = vx_c(KCMAX,j,i)
   end do
   end do

   call borehole(field, x_core(n), y_core(n), dxi, deta, 'sg_x', vx_core(n))

   do i=0, IMAX
   do j=0, JMAX
      field(j,i) = vy_c(KCMAX,j,i)
   end do
   end do

   call borehole(field, x_core(n), y_core(n), dxi, deta, 'sg_y', vy_core(n))

   v_core(n) = sqrt(vx_core(n)**2+vy_core(n)**2)

!  ------ Basal temperature

   do i=0, IMAX
   do j=0, JMAX
      field(j,i) = temp_r(KRMAX,j,i)
   end do
   end do

   call borehole(field, x_core(n), y_core(n), dxi, deta, 'grid', temp_core(n))

!  ------ Basal frictional heating

   do i=0, IMAX
   do j=0, JMAX
      field(j,i) = vx_t(0,j,i)*txz_t(0,j,i)
   end do
   end do

   call borehole(field, x_core(n), y_core(n), dxi, deta, 'sg_x', Rbx_core(n))

   do i=0, IMAX
   do j=0, JMAX
      field(j,i) = vy_t(0,j,i)*tyz_t(0,j,i)
   end do
   end do

   call borehole(field, x_core(n), y_core(n), dxi, deta, 'sg_y', Rby_core(n))

   Rb_core(n) = Rbx_core(n) + Rby_core(n)

end do

!  ------ Conversion

#if ( !defined(OUT_TIMES) || OUT_TIMES==1 )
time_val = time /YEAR_SEC   ! s -> a
#elif OUT_TIMES == 2
time_val = (time+year_zero) /YEAR_SEC   ! s -> a
#else
stop ' output4: OUT_TIMES must be either 1 or 2!'
#endif

do n=1, n_core
   v_core(n) = v_core(n) *YEAR_SEC   ! m/s -> m/a
end do

!-------- Writing of data on file --------

if (forcing_flag == 1) then
   write(unit=14, fmt='(1pe13.6,2(1pe13.4))') time_val, delta_ts, z_sl
else if (forcing_flag == 2) then
   write(unit=14, fmt='(1pe13.6,2(1pe13.4))') time_val, glac_index, z_sl
else if (forcing_flag == 3) then
   write(unit=14, fmt='(1pe13.6,2(1pe13.4))') time_val, 1.11e+11_dp, z_sl
end if

n=1
write(unit=14, fmt='(13x,1pe13.4)', advance='no') H_core(n)
do n=2, n_core-1
   write(unit=14, fmt='(1pe13.4)', advance='no') H_core(n)
end do
n=n_core
write(unit=14, fmt='(1pe13.4)') H_core(n)

n=1
write(unit=14, fmt='(13x,1pe13.4)', advance='no') v_core(n)
do n=2, n_core-1
   write(unit=14, fmt='(1pe13.4)', advance='no') v_core(n)
end do
n=n_core
write(unit=14, fmt='(1pe13.4)') v_core(n)

n=1
write(unit=14, fmt='(13x,1pe13.4)', advance='no') temp_core(n)
do n=2, n_core-1
   write(unit=14, fmt='(1pe13.4)', advance='no') temp_core(n)
end do
n=n_core
write(unit=14, fmt='(1pe13.4)') temp_core(n)

n=1
write(unit=14, fmt='(13x,1pe13.4)', advance='no') Rb_core(n)
do n=2, n_core-1
   write(unit=14, fmt='(1pe13.4)', advance='no') Rb_core(n)
end do
n=n_core
write(unit=14, fmt='(1pe13.4,/)') Rb_core(n)

deallocate(H_core, vx_core, vy_core, v_core, temp_core, &
           Rbx_core, Rby_core, Rb_core)

end subroutine output4
!
