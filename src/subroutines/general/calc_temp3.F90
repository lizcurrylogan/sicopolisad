!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!  Subroutine :  c a l c _ t e m p 3
!
!> @file
!!
!! Computation of temperature, water content and age for an ice column with a
!! temperate base overlain by a temperate-ice layer.
!!
!! @section Copyright
!!
!! Copyright 2009-2016 Ralf Greve
!!
!! @section License
!!
!! This file is part of SICOPOLIS.
!!
!! SICOPOLIS is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! SICOPOLIS is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with SICOPOLIS.  If not, see <http://www.gnu.org/licenses/>.
!<
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!-------------------------------------------------------------------------------
!> Computation of temperature, water content and age for an ice column with a
!! temperate base overlain by a temperate-ice layer.
!<------------------------------------------------------------------------------
subroutine calc_temp3(at1, at2_1, at2_2, at3_1, at3_2, &
   at4_1, at4_2, at5, at6, at7, atr1, am1, am2, alb1, &
   aw1, aw2, aw3, aw4, aw5, aw7, aw8, aw9, aqtld, &
   ai1, ai2, ai3, dzeta_t, &
   dtime_temp, dtt_2dxi, dtt_2deta, i, j)

use sico_types
use sico_variables
use ice_material_quantities, only : ratefac_c, ratefac_t, kappa_val, c_val
#ifndef ALLOW_OPENAD
use sico_sle_solvers, only : tri_sle
#else
use sico_sle_solvers
#endif

implicit none

integer(i4b), intent(inout) :: i, j
real(dp), intent(in) :: at1(0:KCMAX), at2_1(0:KCMAX), at2_2(0:KCMAX), &
                        at3_1(0:KCMAX), at3_2(0:KCMAX), at4_1(0:KCMAX), &
                        at4_2(0:KCMAX), at5(0:KCMAX), at6(0:KCMAX), at7, &
                        ai1(0:KCMAX), ai2(0:KCMAX), ai3, &
                        atr1, am1, am2, alb1
real(dp), intent(in) :: aw1, aw2, aw3, aw4, aw5, aw7, aw8, aw9, aqtld
real(dp), intent(in) :: dzeta_t, dtime_temp, dtt_2dxi, dtt_2deta

integer(i4b) :: kc, kt, kr
real(dp) :: ct1(0:KCMAX), ct2(0:KCMAX), ct3(0:KCMAX), ct4(0:KCMAX), &
            ct5(0:KCMAX), ct6(0:KCMAX), ct7(0:KCMAX), ctr1, cm1, cm2, &
            clb1
real(dp) :: ct1_sg(0:KCMAX), ct2_sg(0:KCMAX), ct3_sg(0:KCMAX), &
            ct4_sg(0:KCMAX), adv_vert_sg(0:KCMAX), abs_adv_vert_sg(0:KCMAX)
real(dp) :: ci1(0:KCMAX), ci2(0:KCMAX), ci3
real(dp) :: cw1(0:KTMAX), cw2(0:KTMAX), cw3(0:KTMAX), cw4(0:KTMAX), &
            cw5, cw7(0:KTMAX), cw8, cw9(0:KTMAX)
real(dp) :: cw1_sg(0:KTMAX), cw2_sg(0:KTMAX), cw3_sg(0:KTMAX), &
            cw4_sg(0:KTMAX), adv_vert_w_sg(0:KTMAX), abs_adv_vert_w_sg(0:KTMAX)
real(dp) :: sigma_c_help(0:KCMAX), sigma_t_help(0:KTMAX), &
            temp_c_help(0:KCMAX)
real(dp) :: vx_c_help, vy_c_help, vx_t_help, vy_t_help
real(dp) :: adv_vert_help, adv_vert_w_help
real(dp) :: dtt_dxi, dtt_deta
real(dp) :: lgs_a0(0:KCMAX+KTMAX+KRMAX+IMAX+JMAX), &
            lgs_a1(0:KCMAX+KTMAX+KRMAX+IMAX+JMAX), &
            lgs_a2(0:KCMAX+KTMAX+KRMAX+IMAX+JMAX), &
            lgs_x(0:KCMAX+KTMAX+KRMAX+IMAX+JMAX), &
            lgs_b(0:KCMAX+KTMAX+KRMAX+IMAX+JMAX)
real(dp), parameter :: zero=0.0_dp

!-------- Check for boundary points --------
#ifndef ALLOW_OPENAD
if ((i == 0).or.(i == IMAX).or.(j == 0).or.(j == JMAX)) &
   stop ' calc_temp3: Boundary points not allowed.'
#endif
!-------- Abbreviations --------

ctr1 = atr1
cm1  = am1*H_c_neu(j,i)
clb1 = alb1*q_geo(j,i)

#if (ADV_VERT==1)

do kc=1, KCMAX-1
   ct1(kc) = at1(kc)/H_c_neu(j,i)*0.5_dp*(vz_c(kc,j,i)+vz_c(kc-1,j,i))
end do

kc=0
ct1_sg(kc) = 0.5_dp*(at1(kc)+at1(kc+1))/H_c_neu(j,i)*vz_c(kc,j,i)
             ! only needed for kc=0 ...
kc=KCMAX-1
ct1_sg(kc) = 0.5_dp*(at1(kc)+at1(kc+1))/H_c_neu(j,i)*vz_c(kc,j,i)
             ! ... and kc=KCMAX-1

#elif (ADV_VERT==2 || ADV_VERT==3)

do kc=0, KCMAX-1
   ct1_sg(kc) = 0.5_dp*(at1(kc)+at1(kc+1))/H_c_neu(j,i)*vz_c(kc,j,i)
end do

#endif

do kc=0, KCMAX

   ct2(kc) = ( at2_1(kc)*dzm_dtau(j,i) &
           +at2_2(kc)*dH_c_dtau(j,i) )/H_c_neu(j,i)
   ct3(kc) = ( at3_1(kc)*dzm_dxi_g(j,i) &
           +at3_2(kc)*dH_c_dxi_g(j,i) )/H_c_neu(j,i) &
          *0.5_dp*(vx_c(kc,j,i)+vx_c(kc,j,i-1)) *insq_g11_g(j,i)
   ct4(kc) = ( at4_1(kc)*dzm_deta_g(j,i) &
            +at4_2(kc)*dH_c_deta_g(j,i) )/H_c_neu(j,i) &
          *0.5_dp*(vy_c(kc,j,i)+vy_c(kc,j-1,i)) *insq_g22_g(j,i)
   ct5(kc) = at5(kc) &
             /c_val(temp_c(kc,j,i)) &
             /H_c_neu(j,i)

   sigma_c_help(kc) &
           = RHO*G*H_c_neu(j,i)*(1.0_dp-eaz_c_quotient(kc)) &
             *(dzs_dxi_g(j,i)**2+dzs_deta_g(j,i)**2)**0.5_dp

#if (DYNAMICS==2)
   if (.not.flag_shelfy_stream(j,i)) then
#endif
      ct7(kc) = at7 &
                /c_val(temp_c(kc,j,i)) &
                *enh_c(kc,j,i) &
                *ratefac_c(temp_c(kc,j,i), temp_c_m(kc,j,i)) &
                *creep(sigma_c_help(kc)) &
                *sigma_c_help(kc)*sigma_c_help(kc)
#if (DYNAMICS==2)
   else
      ct7(kc) = 2.0_dp*at7 &
                /c_val(temp_c(kc,j,i)) &
                *viscosity(de_c(kc,j,i), &
                           temp_c(kc,j,i), temp_c_m(kc,j,i), 0.0_dp, &
                           enh_c(kc,j,i), 0_i2b) &
                *de_c(kc,j,i)**2
   end if
#endif

   ci1(kc) = ai1(kc)/H_c_neu(j,i)

end do

#if (ADV_VERT==1)

kc=0
ct2_sg(kc) = 0.5_dp*(ct2(kc)+ct2(kc+1))
ct3_sg(kc) = 0.5_dp*(ct3(kc)+ct3(kc+1))
ct4_sg(kc) = 0.5_dp*(ct4(kc)+ct4(kc+1))
adv_vert_sg(kc) = ct1_sg(kc)-ct2_sg(kc)-ct3_sg(kc)-ct4_sg(kc)
abs_adv_vert_sg(kc) = abs(adv_vert_sg(kc))   ! only needed for kc=0 ...
kc=KCMAX-1
ct2_sg(kc) = 0.5_dp*(ct2(kc)+ct2(kc+1))
ct3_sg(kc) = 0.5_dp*(ct3(kc)+ct3(kc+1))
ct4_sg(kc) = 0.5_dp*(ct4(kc)+ct4(kc+1))
adv_vert_sg(kc) = ct1_sg(kc)-ct2_sg(kc)-ct3_sg(kc)-ct4_sg(kc)
abs_adv_vert_sg(kc) = abs(adv_vert_sg(kc))   ! ... and kc=KCMAX-1

#elif (ADV_VERT==2 || ADV_VERT==3)

do kc=0, KCMAX-1
   ct2_sg(kc) = 0.5_dp*(ct2(kc)+ct2(kc+1))
   ct3_sg(kc) = 0.5_dp*(ct3(kc)+ct3(kc+1))
   ct4_sg(kc) = 0.5_dp*(ct4(kc)+ct4(kc+1))
   adv_vert_sg(kc) = ct1_sg(kc)-ct2_sg(kc)-ct3_sg(kc)-ct4_sg(kc)
   abs_adv_vert_sg(kc) = abs(adv_vert_sg(kc))
end do

#endif

do kc=0, KCMAX-1
   temp_c_help(kc) = 0.5_dp*(temp_c(kc,j,i)+temp_c(kc+1,j,i))
   ct6(kc) = at6(kc) &
    *kappa_val(temp_c_help(kc)) &
    /H_c_neu(j,i)
   ci2(kc) = ai2(kc)/H_c_neu(j,i)
end do

cw5 = aw5/(H_t_neu(j,i)**2)
cw8 = aw8
ci3 = ai3/(H_t_neu(j,i)**2)

#if (ADV_VERT==1)

do kt=1, KTMAX-1
   cw1(kt) = aw1/H_t_neu(j,i)*0.5_dp*(vz_t(kt,j,i)+vz_t(kt-1,j,i))
end do

kt=KTMAX
cw1(kt) = aw1/H_t_neu(j,i)*0.5_dp*(vz_t(kt-1,j,i)+vz_c(0,j,i))

kt=0
cw1_sg(kt) = aw1/H_t_neu(j,i)*vz_t(kt,j,i)
             ! only needed for kt=0 ...
kt=KTMAX-1
cw1_sg(kt) = aw1/H_t_neu(j,i)*vz_t(kt,j,i)
             ! ... and kt=KTMAX-1

#elif (ADV_VERT==2 || ADV_VERT==3)

do kt=0, KTMAX-1
   cw1_sg(kt) = aw1/H_t_neu(j,i)*vz_t(kt,j,i)
end do

#endif

do kt=1, KTMAX-1
   cw9(kt) = aw9 &
             *c_val(temp_t_m(kt,j,i)) &
    *( dzs_dtau(j,i) &
      +0.5_dp*(vx_t(kt,j,i)+vx_t(kt,j,i-1))*dzs_dxi_g(j,i) &
      +0.5_dp*(vy_t(kt,j,i)+vy_t(kt,j-1,i))*dzs_deta_g(j,i) &
      -0.5_dp*(vz_t(kt,j,i)+vz_t(kt-1,j,i)) )
end do

do kt=0, KTMAX

   cw2(kt) = aw2*(dzb_dtau(j,i)+zeta_t(kt)*dH_t_dtau(j,i)) &
             /H_t_neu(j,i)
   cw3(kt) = aw3*(dzb_dxi_g(j,i)+zeta_t(kt)*dH_t_dxi_g(j,i)) &
             /H_t_neu(j,i) &
             *0.5_dp*(vx_t(kt,j,i)+vx_t(kt,j,i-1)) *insq_g11_g(j,i)
   cw4(kt) = aw4*(dzb_deta_g(j,i)+zeta_t(kt)*dH_t_deta_g(j,i)) &
             /H_t_neu(j,i) &
             *0.5_dp*(vy_t(kt,j,i)+vy_t(kt,j-1,i)) *insq_g22_g(j,i)
   sigma_t_help(kt) &
           = sigma_c_help(0) &
             + RHO*G*H_t_neu(j,i)*(1.0_dp-zeta_t(kt)) &
               *(dzs_dxi_g(j,i)**2+dzs_deta_g(j,i)**2)**0.5_dp

#if (DYNAMICS==2)
   if (.not.flag_shelfy_stream(j,i)) then
#endif
      cw7(kt) = aw7 &
                *enh_t(kt,j,i) &
                *ratefac_t(omega_t(kt,j,i)) &
                *creep(sigma_t_help(kt)) &
                *sigma_t_help(kt)*sigma_t_help(kt)
#if (DYNAMICS==2)
   else
      cw7(kt) = 2.0_dp*aw7 &
                *viscosity(de_t(kt,j,i), &
                           temp_t_m(kt,j,i), temp_t_m(kt,j,i), &
                           omega_t(kt,j,i), &
                           enh_t(kt,j,i), 1_i2b) &
                *de_t(kt,j,i)**2
   end if
#endif

end do

#if (ADV_VERT==1)

kt=0
cw2_sg(kt) = 0.5_dp*(cw2(kt)+cw2(kt+1))
cw3_sg(kt) = 0.5_dp*(cw3(kt)+cw3(kt+1))
cw4_sg(kt) = 0.5_dp*(cw4(kt)+cw4(kt+1))
adv_vert_w_sg(kt) = cw1_sg(kt)-cw2_sg(kt)-cw3_sg(kt)-cw4_sg(kt)
abs_adv_vert_w_sg(kt) = abs(adv_vert_w_sg(kt))   ! only needed for kt=0 ...
kt=KTMAX-1
cw2_sg(kt) = 0.5_dp*(cw2(kt)+cw2(kt+1))
cw3_sg(kt) = 0.5_dp*(cw3(kt)+cw3(kt+1))
cw4_sg(kt) = 0.5_dp*(cw4(kt)+cw4(kt+1))
adv_vert_w_sg(kt) = cw1_sg(kt)-cw2_sg(kt)-cw3_sg(kt)-cw4_sg(kt)
abs_adv_vert_w_sg(kt) = abs(adv_vert_w_sg(kt))   ! ... and kt=KTMAX-1

#elif (ADV_VERT==2 || ADV_VERT==3)

do kt=0, KTMAX-1
   cw2_sg(kt) = 0.5_dp*(cw2(kt)+cw2(kt+1))
   cw3_sg(kt) = 0.5_dp*(cw3(kt)+cw3(kt+1))
   cw4_sg(kt) = 0.5_dp*(cw4(kt)+cw4(kt+1))
   adv_vert_w_sg(kt) = cw1_sg(kt)-cw2_sg(kt)-cw3_sg(kt)-cw4_sg(kt)
   abs_adv_vert_w_sg(kt) = abs(adv_vert_w_sg(kt))
end do

#endif

#if (ADV_HOR==3)
dtt_dxi  = 2.0_dp*dtt_2dxi
dtt_deta = 2.0_dp*dtt_2deta
#endif

!-------- Set up the equations for the bedrock temperature --------

kr=0
lgs_a1(kr) = 1.0_dp
lgs_a2(kr) = -1.0_dp
lgs_b(kr)    = clb1

#if (Q_LITHO==1)
!   (coupled heat-conducting bedrock)

do kr=1, KRMAX-1
   lgs_a0(kr) = - ctr1
   lgs_a1(kr) = 1.0_dp + 2.0_dp*ctr1
   lgs_a2(kr) = - ctr1
   lgs_b(kr)    = temp_r(kr,j,i)
end do

#elif (Q_LITHO==0)
!   (no coupled heat-conducting bedrock)

do kr=1, KRMAX-1
   lgs_a0(kr) = 1.0_dp
   lgs_a1(kr) = 0.0_dp
   lgs_a2(kr) = -1.0_dp
   lgs_b(kr)  = 2.0_dp*clb1
end do

#endif

kr=KRMAX
lgs_a0(kr) = 0.0_dp
lgs_a1(kr) = 1.0_dp
lgs_b(kr)   = temp_t_m(0,j,i)

!-------- Solve system of linear equations --------

call tri_sle(lgs_a0, lgs_a1, lgs_a2, lgs_x, lgs_b, KRMAX)

!-------- Assign the result --------

do kr=0, KRMAX
   temp_r_neu(kr,j,i) = lgs_x(kr)
end do

!-------- Set up the equations for the water content in
!         temperate ice --------

kt=0
lgs_a1(kt) = 1.0_dp
lgs_a2(kt) = -1.0_dp
lgs_b(kt)  = 0.0_dp

do kt=1, KTMAX-1

#if (ADV_VERT==1)

   lgs_a0(kt) = -0.5_dp*(cw1(kt)-cw2(kt)-cw3(kt)-cw4(kt)) - cw5
   lgs_a1(kt) = 1.0_dp + 2.0_dp*cw5
   lgs_a2(kt) = 0.5_dp*(cw1(kt)-cw2(kt)-cw3(kt)-cw4(kt)) - cw5

#elif (ADV_VERT==2)

   lgs_a0(kt) &
         = -0.5_dp*(adv_vert_w_sg(kt-1)+abs_adv_vert_w_sg(kt-1)) &
           -cw5
   lgs_a1(kt) &
         = 1.0_dp &
           +0.5_dp*(adv_vert_w_sg(kt-1)+abs_adv_vert_w_sg(kt-1)) &
           -0.5_dp*(adv_vert_w_sg(kt)  -abs_adv_vert_w_sg(kt)  ) &
           +2.0_dp*cw5
   lgs_a2(kt) &
         =  0.5_dp*(adv_vert_w_sg(kt)  -abs_adv_vert_w_sg(kt)  ) &
           -cw5

#elif (ADV_VERT==3)

   adv_vert_w_help = 0.5_dp*(adv_vert_w_sg(kt)+adv_vert_w_sg(kt-1))

   lgs_a0(kt) &
         = -max(adv_vert_w_help, 0.0_dp) &
           -cw5
   lgs_a1(kt) &
         = 1.0_dp &
           +max(adv_vert_w_help, 0.0_dp)-min(adv_vert_w_help, 0.0_dp) &
           +2.0_dp*cw5
   lgs_a2(kt) &
         =  min(adv_vert_w_help, 0.0_dp) &
           -cw5

#endif

#if (ADV_HOR==2)

   lgs_b(kt) = omega_t(kt,j,i) + cw7(kt) + cw8 + cw9(kt) &
       -dtt_2dxi* &
          ( (vx_t(kt,j,i)-abs(vx_t(kt,j,i))) &
            *(omega_t(kt,j,i+1)-omega_t(kt,j,i)) &
            *insq_g11_sgx(j,i) &
           +(vx_t(kt,j,i-1)+abs(vx_t(kt,j,i-1))) &
            *(omega_t(kt,j,i)-omega_t(kt,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_2deta* &
          ( (vy_t(kt,j,i)-abs(vy_t(kt,j,i))) &
            *(omega_t(kt,j+1,i)-omega_t(kt,j,i)) &
            *insq_g22_sgy(j,i) &
           +(vy_t(kt,j-1,i)+abs(vy_t(kt,j-1,i))) &
            *(omega_t(kt,j,i)-omega_t(kt,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#elif (ADV_HOR==3)

   vx_t_help = 0.5_dp*(vx_t(kt,j,i)+vx_t(kt,j,i-1))
   vy_t_help = 0.5_dp*(vy_t(kt,j,i)+vy_t(kt,j-1,i))

   lgs_b(kt) = omega_t(kt,j,i) + cw7(kt) + cw8 + cw9(kt) &
       -dtt_dxi* &
          ( min(vx_t_help, 0.0_dp) &
            *(omega_t(kt,j,i+1)-omega_t(kt,j,i)) &
            *insq_g11_sgx(j,i) &
           +max(vx_t_help, 0.0_dp) &
            *(omega_t(kt,j,i)-omega_t(kt,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_deta* &
          ( min(vy_t_help, 0.0_dp) &
            *(omega_t(kt,j+1,i)-omega_t(kt,j,i)) &
            *insq_g22_sgy(j,i) &
           +max(vy_t_help, 0.0_dp) &
            *(omega_t(kt,j,i)-omega_t(kt,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#endif

end do

kt=KTMAX

#if ( !defined(CTS_MELTING_FREEZING) || CTS_MELTING_FREEZING==1 )

if (am_perp(j,i) >= zero) then   ! melting condition
   lgs_a0(kt) = 0.0_dp
   lgs_a1(kt) = 1.0_dp
   lgs_b(kt)  = 0.0_dp
else   ! am_perp(j,i) < 0.0, freezing condition
   lgs_a0(kt) = -1.0_dp
   lgs_a1(kt) = 1.0_dp
   lgs_b(kt)  = 0.0_dp
end if

#elif (CTS_MELTING_FREEZING==2)

lgs_a0(kt) = 0.0_dp
lgs_a1(kt) = 1.0_dp   ! melting condition assumed
lgs_b(kt)  = 0.0_dp

#else
stop ' calc_temp3: CTS_MELTING_FREEZING must be either 1 or 2!'
#endif

!-------- Solve system of linear equations --------

call tri_sle(lgs_a0, lgs_a1, lgs_a2, lgs_x, lgs_b, KTMAX)

!-------- Assign the result, compute the water drainage --------

Q_tld(j,i) = 0.0_dp

do kt=0, KTMAX

   if (lgs_x(kt) < zero) then
      omega_t_neu(kt,j,i) = 0.0_dp   ! (as a precaution)
   else if (lgs_x(kt) < OMEGA_MAX) then
      omega_t_neu(kt,j,i) = lgs_x(kt)
   else
      omega_t_neu(kt,j,i) = OMEGA_MAX
      Q_tld(j,i) = Q_tld(j,i) &
                     +aqtld*H_t_neu(j,i)*(lgs_x(kt)-OMEGA_MAX)
   end if

end do

!-------- Set up the equations for the ice temperature --------

!  ------ Abbreviation for the jump of the temperature gradient with
!         the new omega

#if ( !defined(CTS_MELTING_FREEZING) || CTS_MELTING_FREEZING==1 )

if (am_perp(j,i) >= zero) then   ! melting condition
   cm2 = 0.0_dp
else   ! am_perp(j,i) < 0.0, freezing condition
   cm2  = am2*H_c_neu(j,i)*omega_t_neu(KTMAX,j,i)*am_perp(j,i) &
          /kappa_val(temp_c(0,j,i))
end if

#elif (CTS_MELTING_FREEZING==2)

cm2 = 0.0_dp   ! melting condition assumed

#else
stop ' calc_temp3: CTS_MELTING_FREEZING must be either 1 or 2!'
#endif

kc=0
lgs_a1(kc) = 1.0_dp
lgs_a2(kc) = -1.0_dp
lgs_b(kc)  = -cm1-cm2

do kc=1, KCMAX-1

#if (ADV_VERT==1)

   lgs_a0(kc) = -0.5_dp*(ct1(kc)-ct2(kc)-ct3(kc)-ct4(kc)) &
                      -ct5(kc)*ct6(kc-1)
   lgs_a1(kc) = 1.0_dp+ct5(kc)*(ct6(kc)+ct6(kc-1))
   lgs_a2(kc) = 0.5_dp*(ct1(kc)-ct2(kc)-ct3(kc)-ct4(kc)) &
                      -ct5(kc)*ct6(kc)

#elif (ADV_VERT==2)

   lgs_a0(kc) &
         = -0.5_dp*(adv_vert_sg(kc-1)+abs_adv_vert_sg(kc-1)) &
           -ct5(kc)*ct6(kc-1)
   lgs_a1(kc) &
         = 1.0_dp &
           +0.5_dp*(adv_vert_sg(kc-1)+abs_adv_vert_sg(kc-1)) &
           -0.5_dp*(adv_vert_sg(kc)  -abs_adv_vert_sg(kc)  ) &
           +ct5(kc)*(ct6(kc)+ct6(kc-1))
   lgs_a2(kc) &
         =  0.5_dp*(adv_vert_sg(kc)  -abs_adv_vert_sg(kc)  ) &
           -ct5(kc)*ct6(kc)

#elif (ADV_VERT==3)

   adv_vert_help = 0.5_dp*(adv_vert_sg(kc)+adv_vert_sg(kc-1))

   lgs_a0(kc) &
         = -max(adv_vert_help, 0.0_dp) &
           -ct5(kc)*ct6(kc-1)
   lgs_a1(kc) &
         = 1.0_dp &
           +max(adv_vert_help, 0.0_dp)-min(adv_vert_help, 0.0_dp) &
           +ct5(kc)*(ct6(kc)+ct6(kc-1))
   lgs_a2(kc) &
         =  min(adv_vert_help, 0.0_dp) &
           -ct5(kc)*ct6(kc)

#endif

#if (ADV_HOR==2)

   lgs_b(kc) = temp_c(kc,j,i) + ct7(kc) &
       -dtt_2dxi* &
          ( (vx_c(kc,j,i)-abs(vx_c(kc,j,i))) &
            *(temp_c(kc,j,i+1)-temp_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +(vx_c(kc,j,i-1)+abs(vx_c(kc,j,i-1))) &
            *(temp_c(kc,j,i)-temp_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_2deta* &
          ( (vy_c(kc,j,i)-abs(vy_c(kc,j,i))) &
            *(temp_c(kc,j+1,i)-temp_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +(vy_c(kc,j-1,i)+abs(vy_c(kc,j-1,i))) &
            *(temp_c(kc,j,i)-temp_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#elif (ADV_HOR==3)

   vx_c_help = 0.5_dp*(vx_c(kc,j,i)+vx_c(kc,j,i-1))
   vy_c_help = 0.5_dp*(vy_c(kc,j,i)+vy_c(kc,j-1,i))

   lgs_b(kc) = temp_c(kc,j,i) + ct7(kc) &
       -dtt_dxi* &
          ( min(vx_c_help, 0.0_dp) &
            *(temp_c(kc,j,i+1)-temp_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +max(vx_c_help, 0.0_dp) &
            *(temp_c(kc,j,i)-temp_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_deta* &
          ( min(vy_c_help, 0.0_dp) &
            *(temp_c(kc,j+1,i)-temp_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +max(vy_c_help, 0.0_dp) &
            *(temp_c(kc,j,i)-temp_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#endif

end do

kc=KCMAX
lgs_a0(kc) = 0.0_dp
lgs_a1(kc) = 1.0_dp
lgs_b(kc)  = temp_s(j,i)

!-------- Solve system of linear equations --------

call tri_sle(lgs_a0, lgs_a1, lgs_a2, lgs_x, lgs_b, KCMAX)

!-------- Assign the result --------

do kc=0, KCMAX
   temp_c_neu(kc,j,i) = lgs_x(kc)
end do

!-------- Set up the equations for the age (cold and temperate ice
!         simultaneously) --------

kt=0                                                  ! adv_vert_w_sg(0) <= 0
lgs_a1(kt) = 1.0_dp - min(adv_vert_w_sg(kt), 0.0_dp)  ! (directed downward)
lgs_a2(kt) = min(adv_vert_w_sg(kt), 0.0_dp)           ! assumed/enforced

#if (ADV_HOR==2)

lgs_b(kt) = age_t(kt,j,i) + dtime_temp &
       -dtt_2dxi* &
          ( (vx_t(kt,j,i)-abs(vx_t(kt,j,i))) &
            *(age_t(kt,j,i+1)-age_t(kt,j,i)) &
            *insq_g11_sgx(j,i) &
           +(vx_t(kt,j,i-1)+abs(vx_t(kt,j,i-1))) &
            *(age_t(kt,j,i)-age_t(kt,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_2deta* &
          ( (vy_t(kt,j,i)-abs(vy_t(kt,j,i))) &
            *(age_t(kt,j+1,i)-age_t(kt,j,i)) &
            *insq_g22_sgy(j,i) &
           +(vy_t(kt,j-1,i)+abs(vy_t(kt,j-1,i))) &
            *(age_t(kt,j,i)-age_t(kt,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#elif (ADV_HOR==3)

vx_t_help = 0.5_dp*(vx_t(kt,j,i)+vx_t(kt,j,i-1))
vy_t_help = 0.5_dp*(vy_t(kt,j,i)+vy_t(kt,j-1,i))

lgs_b(kt) = age_t(kt,j,i) + dtime_temp &
       -dtt_dxi* &
          ( min(vx_t_help, 0.0_dp) &
            *(age_t(kt,j,i+1)-age_t(kt,j,i)) &
            *insq_g11_sgx(j,i) &
           +max(vx_t_help, 0.0_dp) &
            *(age_t(kt,j,i)-age_t(kt,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_deta* &
          ( min(vy_t_help, 0.0_dp) &
            *(age_t(kt,j+1,i)-age_t(kt,j,i)) &
            *insq_g22_sgy(j,i) &
           +max(vy_t_help, 0.0_dp) &
            *(age_t(kt,j,i)-age_t(kt,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#endif

do kt=1, KTMAX-1

#if (ADV_VERT==1)

   lgs_a0(kt) = -0.5_dp*(cw1(kt)-cw2(kt)-cw3(kt)-cw4(kt)) - ci3
   lgs_a1(kt) = 1.0_dp + 2.0_dp*ci3
   lgs_a2(kt) = 0.5_dp*(cw1(kt)-cw2(kt)-cw3(kt)-cw4(kt)) - ci3

#elif (ADV_VERT==2)

   lgs_a0(kt) = -0.5_dp*(adv_vert_w_sg(kt-1)+abs_adv_vert_w_sg(kt-1))
   lgs_a1(kt) = 1.0_dp &
               +0.5_dp*(adv_vert_w_sg(kt-1)+abs_adv_vert_w_sg(kt-1)) &
               -0.5_dp*(adv_vert_w_sg(kt)  -abs_adv_vert_w_sg(kt)  )
   lgs_a2(kt) =  0.5_dp*(adv_vert_w_sg(kt)  -abs_adv_vert_w_sg(kt)  )

#elif (ADV_VERT==3)

   adv_vert_w_help = 0.5_dp*(adv_vert_w_sg(kt)+adv_vert_w_sg(kt-1))

   lgs_a0(kt) = -max(adv_vert_w_help, 0.0_dp)
   lgs_a1(kt) = 1.0_dp &
               +max(adv_vert_w_help, 0.0_dp)-min(adv_vert_w_help, 0.0_dp)
   lgs_a2(kt) =  min(adv_vert_w_help, 0.0_dp)

#endif

#if (ADV_HOR==2)

   lgs_b(kt) = age_t(kt,j,i) + dtime_temp &
       -dtt_2dxi* &
          ( (vx_t(kt,j,i)-abs(vx_t(kt,j,i))) &
            *(age_t(kt,j,i+1)-age_t(kt,j,i)) &
            *insq_g11_sgx(j,i) &
           +(vx_t(kt,j,i-1)+abs(vx_t(kt,j,i-1))) &
            *(age_t(kt,j,i)-age_t(kt,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_2deta* &
          ( (vy_t(kt,j,i)-abs(vy_t(kt,j,i))) &
            *(age_t(kt,j+1,i)-age_t(kt,j,i)) &
            *insq_g22_sgy(j,i) &
           +(vy_t(kt,j-1,i)+abs(vy_t(kt,j-1,i))) &
            *(age_t(kt,j,i)-age_t(kt,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#elif (ADV_HOR==3)

   vx_t_help = 0.5_dp*(vx_t(kt,j,i)+vx_t(kt,j,i-1))
   vy_t_help = 0.5_dp*(vy_t(kt,j,i)+vy_t(kt,j-1,i))

   lgs_b(kt) = age_t(kt,j,i) + dtime_temp &
       -dtt_dxi* &
          ( min(vx_t_help, 0.0_dp) &
            *(age_t(kt,j,i+1)-age_t(kt,j,i)) &
            *insq_g11_sgx(j,i) &
           +max(vx_t_help, 0.0_dp) &
            *(age_t(kt,j,i)-age_t(kt,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_deta* &
          ( min(vy_t_help, 0.0_dp) &
            *(age_t(kt,j+1,i)-age_t(kt,j,i)) &
            *insq_g22_sgy(j,i) &
           +max(vy_t_help, 0.0_dp) &
            *(age_t(kt,j,i)-age_t(kt,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#endif

end do

#if (ADV_VERT==1)

kt=KTMAX
kc=0

lgs_a0(kt) = -0.5_dp*(cw1(kt)-cw2(kt)-cw3(kt)-cw4(kt)) - ci3
lgs_a1(kt) = 1.0_dp + 2.0_dp*ci3
lgs_a2(kt) = 0.5_dp*(cw1(kt)-cw2(kt)-cw3(kt)-cw4(kt)) - ci3

#if (ADV_HOR==2)

lgs_b(kt) = age_t(kt,j,i) + dtime_temp &
       -dtt_2dxi* &
          ( (vx_t(kt,j,i)-abs(vx_t(kt,j,i))) &
            *(age_t(kt,j,i+1)-age_t(kt,j,i)) &
            *insq_g11_sgx(j,i) &
           +(vx_t(kt,j,i-1)+abs(vx_t(kt,j,i-1))) &
            *(age_t(kt,j,i)-age_t(kt,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_2deta* &
          ( (vy_t(kt,j,i)-abs(vy_t(kt,j,i))) &
            *(age_t(kt,j+1,i)-age_t(kt,j,i)) &
            *insq_g22_sgy(j,i) &
           +(vy_t(kt,j-1,i)+abs(vy_t(kt,j-1,i))) &
            *(age_t(kt,j,i)-age_t(kt,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#elif (ADV_HOR==3)

vx_t_help = 0.5_dp*(vx_t(kt,j,i)+vx_t(kt,j,i-1))
vy_t_help = 0.5_dp*(vy_t(kt,j,i)+vy_t(kt,j-1,i))

lgs_b(kt) = age_t(kt,j,i) + dtime_temp &
       -dtt_dxi* &
          ( min(vx_t_help, 0.0_dp) &
            *(age_t(kt,j,i+1)-age_t(kt,j,i)) &
            *insq_g11_sgx(j,i) &
           +max(vx_t_help, 0.0_dp) &
            *(age_t(kt,j,i)-age_t(kt,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_deta* &
          ( min(vy_t_help, 0.0_dp) &
            *(age_t(kt,j+1,i)-age_t(kt,j,i)) &
            *insq_g22_sgy(j,i) &
           +max(vy_t_help, 0.0_dp) &
            *(age_t(kt,j,i)-age_t(kt,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#endif

#elif (ADV_VERT==2 || ADV_VERT==3)

kt=KTMAX
kc=0

if (adv_vert_sg(kc) <= zero) then

   lgs_a0(KTMAX+kc) = 0.0_dp
   lgs_a1(KTMAX+kc) = 1.0_dp - adv_vert_sg(kc)
   lgs_a2(KTMAX+kc) = adv_vert_sg(kc)

#if (ADV_HOR==2)

   lgs_b(KTMAX+kc) = age_c(kc,j,i) + dtime_temp &
       -dtt_2dxi* &
          ( (vx_c(kc,j,i)-abs(vx_c(kc,j,i))) &
            *(age_c(kc,j,i+1)-age_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +(vx_c(kc,j,i-1)+abs(vx_c(kc,j,i-1))) &
            *(age_c(kc,j,i)-age_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_2deta* &
          ( (vy_c(kc,j,i)-abs(vy_c(kc,j,i))) &
            *(age_c(kc,j+1,i)-age_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +(vy_c(kc,j-1,i)+abs(vy_c(kc,j-1,i))) &
            *(age_c(kc,j,i)-age_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#elif (ADV_HOR==3)

   vx_c_help = 0.5_dp*(vx_c(kc,j,i)+vx_c(kc,j,i-1))
   vy_c_help = 0.5_dp*(vy_c(kc,j,i)+vy_c(kc,j-1,i))

   lgs_b(KTMAX+kc) = age_c(kc,j,i) + dtime_temp &
       -dtt_dxi* &
          ( min(vx_c_help, 0.0_dp) &
            *(age_c(kc,j,i+1)-age_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +max(vx_c_help, 0.0_dp) &
            *(age_c(kc,j,i)-age_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_deta* &
          ( min(vy_c_help, 0.0_dp) &
            *(age_c(kc,j+1,i)-age_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +max(vy_c_help, 0.0_dp) &
            *(age_c(kc,j,i)-age_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#endif

else if (adv_vert_w_sg(kt-1) >= zero) then

   lgs_a0(kt) = -adv_vert_w_sg(kt-1)
   lgs_a1(kt) = 1.0_dp + adv_vert_w_sg(kt-1)
   lgs_a2(kt) = 0.0_dp

#if (ADV_HOR==2)

   lgs_b(kt) = age_t(kt,j,i) + dtime_temp &
       -dtt_2dxi* &
          ( (vx_t(kt,j,i)-abs(vx_t(kt,j,i))) &
            *(age_t(kt,j,i+1)-age_t(kt,j,i)) &
            *insq_g11_sgx(j,i) &
           +(vx_t(kt,j,i-1)+abs(vx_t(kt,j,i-1))) &
            *(age_t(kt,j,i)-age_t(kt,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_2deta* &
          ( (vy_t(kt,j,i)-abs(vy_t(kt,j,i))) &
            *(age_t(kt,j+1,i)-age_t(kt,j,i)) &
            *insq_g22_sgy(j,i) &
           +(vy_t(kt,j-1,i)+abs(vy_t(kt,j-1,i))) &
            *(age_t(kt,j,i)-age_t(kt,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#elif (ADV_HOR==3)

   vx_t_help = 0.5_dp*(vx_t(kt,j,i)+vx_t(kt,j,i-1))
   vy_t_help = 0.5_dp*(vy_t(kt,j,i)+vy_t(kt,j-1,i))

   lgs_b(kt) = age_t(kt,j,i) + dtime_temp &
       -dtt_dxi* &
          ( min(vx_t_help, 0.0_dp) &
            *(age_t(kt,j,i+1)-age_t(kt,j,i)) &
            *insq_g11_sgx(j,i) &
           +max(vx_t_help, 0.0_dp) &
            *(age_t(kt,j,i)-age_t(kt,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_deta* &
          ( min(vy_t_help, 0.0_dp) &
            *(age_t(kt,j+1,i)-age_t(kt,j,i)) &
            *insq_g22_sgy(j,i) &
           +max(vy_t_help, 0.0_dp) &
            *(age_t(kt,j,i)-age_t(kt,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#endif

else

   lgs_a0(kt) = -0.5_dp
   lgs_a1(kt) = 1.0_dp
   lgs_a2(kt) = -0.5_dp
   lgs_b(kt)  = 0.0_dp
   ! Makeshift: Average of age_c(kc=1) and age_t(kt=KTMAX-1)

end if

#endif

do kc=1, KCMAX-1

#if (ADV_VERT==1)

   lgs_a0(KTMAX+kc) = -0.5_dp*(ct1(kc)-ct2(kc)-ct3(kc)-ct4(kc)) &
                      -ci1(kc)*ci2(kc-1)
   lgs_a1(KTMAX+kc) = 1.0_dp+ci1(kc)*(ci2(kc)+ci2(kc-1))
   lgs_a2(KTMAX+kc) = 0.5_dp*(ct1(kc)-ct2(kc)-ct3(kc)-ct4(kc)) &
                      -ci1(kc)*ci2(kc)

#elif (ADV_VERT==2)

   lgs_a0(KTMAX+kc) = -0.5_dp*(adv_vert_sg(kc-1)+abs_adv_vert_sg(kc-1))
   lgs_a1(KTMAX+kc) =  1.0_dp &
                      +0.5_dp*(adv_vert_sg(kc-1)+abs_adv_vert_sg(kc-1)) &
                      -0.5_dp*(adv_vert_sg(kc)  -abs_adv_vert_sg(kc)  )
   lgs_a2(KTMAX+kc) =  0.5_dp*(adv_vert_sg(kc)  -abs_adv_vert_sg(kc)  )

#elif (ADV_VERT==3)

   adv_vert_help = 0.5_dp*(adv_vert_sg(kc)+adv_vert_sg(kc-1))

   lgs_a0(KTMAX+kc) = -max(adv_vert_help, 0.0_dp)
   lgs_a1(KTMAX+kc) =  1.0_dp &
                      +max(adv_vert_help, 0.0_dp)-min(adv_vert_help, 0.0_dp)
   lgs_a2(KTMAX+kc) =  min(adv_vert_help, 0.0_dp)

#endif

#if (ADV_HOR==2)

   lgs_b(KTMAX+kc) = age_c(kc,j,i) + dtime_temp &
       -dtt_2dxi* &
          ( (vx_c(kc,j,i)-abs(vx_c(kc,j,i))) &
            *(age_c(kc,j,i+1)-age_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +(vx_c(kc,j,i-1)+abs(vx_c(kc,j,i-1))) &
            *(age_c(kc,j,i)-age_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_2deta* &
          ( (vy_c(kc,j,i)-abs(vy_c(kc,j,i))) &
            *(age_c(kc,j+1,i)-age_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +(vy_c(kc,j-1,i)+abs(vy_c(kc,j-1,i))) &
            *(age_c(kc,j,i)-age_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#elif (ADV_HOR==3)

   vx_c_help = 0.5_dp*(vx_c(kc,j,i)+vx_c(kc,j,i-1))
   vy_c_help = 0.5_dp*(vy_c(kc,j,i)+vy_c(kc,j-1,i))

   lgs_b(KTMAX+kc) = age_c(kc,j,i) + dtime_temp &
       -dtt_dxi* &
          ( min(vx_c_help, 0.0_dp) &
            *(age_c(kc,j,i+1)-age_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +max(vx_c_help, 0.0_dp) &
            *(age_c(kc,j,i)-age_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_deta* &
          ( min(vy_c_help, 0.0_dp) &
            *(age_c(kc,j+1,i)-age_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +max(vy_c_help, 0.0_dp) &
            *(age_c(kc,j,i)-age_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#endif

end do

kc=KCMAX
if (as_perp(j,i) >= zero) then
   lgs_a0(KTMAX+kc) = 0.0_dp
   lgs_a1(KTMAX+kc) = 1.0_dp
   lgs_b(KTMAX+kc)  = 0.0_dp
else
   lgs_a0(KTMAX+kc) = -max(adv_vert_sg(kc-1), 0.0_dp)
   lgs_a1(KTMAX+kc) = 1.0_dp + max(adv_vert_sg(kc-1), 0.0_dp)
                             ! adv_vert_sg(KCMAX-1) >= 0 (directed upward)
                             ! assumed/enforced
#if (ADV_HOR==2)

   lgs_b(KTMAX+kc) = age_c(kc,j,i) + dtime_temp &
       -dtt_2dxi* &
          ( (vx_c(kc,j,i)-abs(vx_c(kc,j,i))) &
            *(age_c(kc,j,i+1)-age_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +(vx_c(kc,j,i-1)+abs(vx_c(kc,j,i-1))) &
            *(age_c(kc,j,i)-age_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_2deta* &
          ( (vy_c(kc,j,i)-abs(vy_c(kc,j,i))) &
            *(age_c(kc,j+1,i)-age_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +(vy_c(kc,j-1,i)+abs(vy_c(kc,j-1,i))) &
            *(age_c(kc,j,i)-age_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#elif (ADV_HOR==3)

   vx_c_help = 0.5_dp*(vx_c(kc,j,i)+vx_c(kc,j,i-1))
   vy_c_help = 0.5_dp*(vy_c(kc,j,i)+vy_c(kc,j-1,i))

   lgs_b(KTMAX+kc) = age_c(kc,j,i) + dtime_temp &
       -dtt_dxi* &
          ( min(vx_c_help, 0.0_dp) &
            *(age_c(kc,j,i+1)-age_c(kc,j,i)) &
            *insq_g11_sgx(j,i) &
           +max(vx_c_help, 0.0_dp) &
            *(age_c(kc,j,i)-age_c(kc,j,i-1)) &
            *insq_g11_sgx(j,i-1) ) &
       -dtt_deta* &
          ( min(vy_c_help, 0.0_dp) &
            *(age_c(kc,j+1,i)-age_c(kc,j,i)) &
            *insq_g22_sgy(j,i) &
           +max(vy_c_help, 0.0_dp) &
            *(age_c(kc,j,i)-age_c(kc,j-1,i)) &
            *insq_g22_sgy(j-1,i) )

#endif

end if

!-------- Solve system of linear equations --------

call tri_sle(lgs_a0, lgs_a1, lgs_a2, lgs_x, lgs_b, KCMAX+KTMAX)

!-------- Assign the result,
!         restriction to interval [0, AGE_MAX yr] --------

do kt=0, KTMAX

   age_t_neu(kt,j,i) = lgs_x(kt)

   if (age_t_neu(kt,j,i) < (AGE_MIN*YEAR_SEC)) &
                           age_t_neu(kt,j,i) = 0.0_dp
   if (age_t_neu(kt,j,i) > (AGE_MAX*YEAR_SEC)) &
                           age_t_neu(kt,j,i) = AGE_MAX*YEAR_SEC

end do

do kc=0, KCMAX

   age_c_neu(kc,j,i) = lgs_x(KTMAX+kc)

   if (age_c_neu(kc,j,i) < (AGE_MIN*YEAR_SEC)) &
                           age_c_neu(kc,j,i) = 0.0_dp
   if (age_c_neu(kc,j,i) > (AGE_MAX*YEAR_SEC)) &
                           age_c_neu(kc,j,i) = AGE_MAX*YEAR_SEC

end do

end subroutine calc_temp3
!
