#include "sico_specs.h"

subroutine var_transfer&         
     (a_maske,a_maske_help,a_maske_neu,a_n_cts,a_n_cts_neu,a_kc_cts,a_kc_cts_neu,&
     a_flag_grounding_line_1,a_flag_grounding_line_2,a_flag_calving_front_1,&
     a_flag_calving_front_2,a_flag_shelfy_stream_x,a_flag_shelfy_stream_y,&
     a_flag_shelfy_stream,a_xi,a_eta,a_zeta_c,a_zeta_t,a_zeta_r,a_aa,a_flag_aa_nonzero,&
     a_ea,a_eaz_c,a_eaz_c_quotient,a_lambda,a_phi,a_area,a_sq_g11_g,a_sq_g22_g,&
     a_insq_g11_g,a_insq_g22_g,a_sq_g11_sgx,a_sq_g11_sgy,a_sq_g22_sgx,a_sq_g22_sgy,&
     a_insq_g11_sgx,a_insq_g22_sgy,a_zs,a_zm,a_zb,a_zl,a_zl0,a_wss,a_flex_rig_lith,&
     a_time_lag_asth,a_H_c,a_H_t,a_dzs_dxi,a_dzm_dxi,a_dzb_dxi,a_dH_c_dxi,a_dH_t_dxi,&
     a_dzs_deta,a_dzm_deta,a_dzb_deta,a_dH_c_deta,a_dH_t_deta,a_dzs_dxi_g,&
     a_dzm_dxi_g,a_dzb_dxi_g,a_dH_c_dxi_g,a_dH_t_dxi_g,a_dzs_deta_g,a_dzm_deta_g,&
     a_dzb_deta_g,a_dH_c_deta_g,a_dH_t_deta_g,a_dzs_dtau,a_dzm_dtau,a_dzb_dtau,&
     a_dzl_dtau,a_dH_c_dtau,a_dH_t_dtau,a_p_weert,a_q_weert,a_p_weert_inv,&
     a_c_slide,a_d_help_b,a_c_drag,a_p_b_w,a_vx_b,a_vy_b,a_vx_m,a_vy_m,a_ratio_sl_x,&
     a_ratio_sl_y,a_vx_b_g,a_vy_b_g,a_vz_b,a_vz_m,a_vx_s_g,a_vy_s_g,a_vz_s,&
     a_flui_ave_sia,a_h_diff,a_qx,a_qy,a_q_gl_g,a_q_geo,a_temp_b,a_temph_b,a_Q_bm,&
     a_Q_tld,a_Q_b_tot,a_H_w,a_accum,a_evap,a_runoff,a_as_perp,a_temp_s,a_am_perp,&
     a_am_perp_st,a_zs_neu,a_zm_neu,a_zb_neu,a_zl_neu,a_H_c_neu,a_H_t_neu,a_zs_ref,&
     a_accum_present,a_precip_ma_present,a_precip_ma_lgm_anom,&
     a_temp_ma_present,a_temp_mj_present,a_temp_ma_lgm_anom,a_temp_mj_lgm_anom,&
     a_dist_dxdy,a_precip_present,a_precip_lgm_anom,a_gamma_precip_lgm_anom,&
     a_temp_mm_present,a_temp_mm_lgm_anom,a_d_help_c,a_vx_c,a_vy_c,a_vz_c,a_temp_c,&
     a_temp_c_neu,a_temp_c_m,a_age_c,a_age_c_neu,a_txz_c,a_tyz_c,a_sigma_c,a_enh_c,&
     a_de_ssa,a_vis_int_g,a_vx_g,a_vy_g,a_d_help_t,a_vx_t,a_vy_t,a_vz_t,a_omega_t,&
     a_omega_t_neu,a_temp_t_m,a_age_t,a_age_t_neu,a_txz_t,a_tyz_t,a_sigma_t,a_enh_t,&
     a_temp_r,a_temp_r_neu,a_enth_c,a_enth_c_neu,a_omega_c,a_omega_c_neu,a_enth_t,&
     a_enth_t_neu,a_dxx_c,a_dyy_c,a_dxy_c,a_dxz_c,a_dyz_c,a_de_c,a_lambda_shear_c,&
     a_dxx_t,a_dyy_t,a_dxy_t,a_dxz_t,a_dyz_t,a_de_t,a_lambda_shear_t,a_RHO,a_RHO_W,&
     a_RHO_SW,a_L,a_G,a_NUE,a_BETA,a_DELTA_TM_SW,a_OMEGA_MAX,a_H_R,a_RHO_C_R,a_KAPPA_R,&
     a_RHO_A,a_R_T_IMQ,a_R,a_A,a_B,a_LAMBDA0,a_PHI0,a_S_STAT_0,a_BETA1_0,a_BETA1_LT_0,&
     a_BETA1_HT_0,a_BETA2_0,a_BETA2_LT_0,a_BETA2_HT_0,a_PHI_SEP_0,a_PMAX_0,a_MU_0,&
     a_RF_IMQ,a_KAPPA_IMQ,a_C_IMQ,a_year_zero,&
     a_forcing_flag,a_n_core,a_lambda_core,a_phi_core,a_x_core,&
     a_y_core,a_grip_time_min,a_grip_time_stp,a_grip_time_max,a_ndata_grip,&
     a_griptemp,a_gi_time_min,a_gi_time_stp,a_gi_time_max,a_ndata_gi,&
     a_glacial_index,a_specmap_time_min,a_specmap_time_stp,a_specmap_time_max,&
     a_ndata_specmap,a_specmap_zsl,a_time_target_topo_init,&
     a_time_target_topo_final,a_maske_target,a_zs_target,a_zb_target,&
     a_zl_target,a_H_target,a_maske_maxextent,a_ncid_temp_precip,&
     a_ndata_temp_precip,a_temp_precip_time_min,a_temp_precip_time_stp,&
     a_temp_precip_time_max,a_temp_precip_time,a_kei,a_kei_r_max,&
     a_kei_r_incr,a_fc,a_objf_test,&
     a_mult_test,&
     a_c_int_table,a_c_int_inv_table,a_n_temp_min,a_n_temp_max,a_n_enth_min,&
     a_n_enth_max,a_L_inv,a_L_eto,&
     a_RHO_I_IMQ,a_RHO_C_IMQ,a_KAPPA_C_IMQ,a_C_C_IMQ)

use OAD_active
use oad_sico_variables
use oad_enth_temp_omega
use oad_ice_material_quantities 

implicit none

integer(i2b), dimension(0:JMAX,0:IMAX) :: a_maske
integer(i2b), dimension(0:JMAX,0:IMAX) :: a_maske_help
integer(i2b), dimension(0:JMAX,0:IMAX) :: a_maske_neu
integer(i2b), dimension(0:JMAX,0:IMAX) :: a_n_cts
integer(i2b), dimension(0:JMAX,0:IMAX) :: a_n_cts_neu
integer(i2b), dimension(0:JMAX,0:IMAX) :: a_kc_cts
integer(i2b), dimension(0:JMAX,0:IMAX) :: a_kc_cts_neu
logical, dimension(0:JMAX,0:IMAX) :: a_flag_grounding_line_1
logical, dimension(0:JMAX,0:IMAX) :: a_flag_grounding_line_2
logical, dimension(0:JMAX,0:IMAX) :: a_flag_calving_front_1
logical, dimension(0:JMAX,0:IMAX) :: a_flag_calving_front_2
logical, dimension(0:JMAX,0:IMAX) :: a_flag_shelfy_stream_x
logical, dimension(0:JMAX,0:IMAX) :: a_flag_shelfy_stream_y
logical, dimension(0:JMAX,0:IMAX) :: a_flag_shelfy_stream
real(dp), dimension(0:IMAX) :: a_xi
real(dp), dimension(0:JMAX) :: a_eta
real(dp), dimension(0:KCMAX) :: a_zeta_c
real(dp), dimension(0:KTMAX) :: a_zeta_t
real(dp), dimension(0:KRMAX) :: a_zeta_r
real(dp) :: a_aa
logical :: a_flag_aa_nonzero
real(dp) :: a_ea
real(dp), dimension(0:KCMAX) :: a_eaz_c
real(dp), dimension(0:KCMAX) :: a_eaz_c_quotient
real(dp), dimension(0:JMAX,0:IMAX) :: a_lambda
real(dp), dimension(0:JMAX,0:IMAX) :: a_phi
real(dp), dimension(0:JMAX,0:IMAX) :: a_area
real(dp), dimension(0:JMAX,0:IMAX) :: a_sq_g11_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_sq_g22_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_insq_g11_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_insq_g22_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_sq_g11_sgx
real(dp), dimension(0:JMAX,0:IMAX) :: a_sq_g11_sgy
real(dp), dimension(0:JMAX,0:IMAX) :: a_sq_g22_sgx
real(dp), dimension(0:JMAX,0:IMAX) :: a_sq_g22_sgy
real(dp), dimension(0:JMAX,0:IMAX) :: a_insq_g11_sgx
real(dp), dimension(0:JMAX,0:IMAX) :: a_insq_g22_sgy
real(dp), dimension(0:JMAX,0:IMAX) :: a_zs
real(dp), dimension(0:JMAX,0:IMAX) :: a_zm
real(dp), dimension(0:JMAX,0:IMAX) :: a_zb
real(dp), dimension(0:JMAX,0:IMAX) :: a_zl
real(dp), dimension(0:JMAX,0:IMAX) :: a_zl0
real(dp), dimension(0:JMAX,0:IMAX) :: a_wss
real(dp), dimension(0:JMAX,0:IMAX) :: a_flex_rig_lith
real(dp), dimension(0:JMAX,0:IMAX) :: a_time_lag_asth
real(dp), dimension(0:JMAX,0:IMAX) :: a_H_c
real(dp), dimension(0:JMAX,0:IMAX) :: a_H_t
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzs_dxi
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzm_dxi
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzb_dxi
real(dp), dimension(0:JMAX,0:IMAX) :: a_dH_c_dxi
real(dp), dimension(0:JMAX,0:IMAX) :: a_dH_t_dxi
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzs_deta
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzm_deta
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzb_deta
real(dp), dimension(0:JMAX,0:IMAX) :: a_dH_c_deta
real(dp), dimension(0:JMAX,0:IMAX) :: a_dH_t_deta
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzs_dxi_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzm_dxi_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzb_dxi_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_dH_c_dxi_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_dH_t_dxi_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzs_deta_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzm_deta_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzb_deta_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_dH_c_deta_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_dH_t_deta_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzs_dtau
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzm_dtau
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzb_dtau
real(dp), dimension(0:JMAX,0:IMAX) :: a_dzl_dtau
real(dp), dimension(0:JMAX,0:IMAX) :: a_dH_c_dtau
real(dp), dimension(0:JMAX,0:IMAX) :: a_dH_t_dtau
integer(i4b), dimension(0:JMAX,0:IMAX) :: a_p_weert
integer(i4b), dimension(0:JMAX,0:IMAX) :: a_q_weert
real(dp), dimension(0:JMAX,0:IMAX) :: a_p_weert_inv
real(dp), dimension(0:JMAX,0:IMAX) :: a_c_slide
real(dp), dimension(0:JMAX,0:IMAX) :: a_d_help_b
real(dp), dimension(0:JMAX,0:IMAX) :: a_c_drag
real(dp), dimension(0:JMAX,0:IMAX) :: a_p_b_w
real(dp), dimension(0:JMAX,0:IMAX) :: a_vx_b
real(dp), dimension(0:JMAX,0:IMAX) :: a_vy_b
real(dp), dimension(0:JMAX,0:IMAX) :: a_vx_m
real(dp), dimension(0:JMAX,0:IMAX) :: a_vy_m
real(dp), dimension(0:JMAX,0:IMAX) :: a_ratio_sl_x
real(dp), dimension(0:JMAX,0:IMAX) :: a_ratio_sl_y
real(dp), dimension(0:JMAX,0:IMAX) :: a_vx_b_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_vy_b_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_vz_b
real(dp), dimension(0:JMAX,0:IMAX) :: a_vz_m
real(dp), dimension(0:JMAX,0:IMAX) :: a_vx_s_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_vy_s_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_vz_s
real(dp), dimension(0:JMAX,0:IMAX) :: a_flui_ave_sia
real(dp), dimension(0:JMAX,0:IMAX) :: a_h_diff
real(dp), dimension(0:JMAX,0:IMAX) :: a_qx
real(dp), dimension(0:JMAX,0:IMAX) :: a_qy
real(dp), dimension(0:JMAX,0:IMAX) :: a_q_gl_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_q_geo
real(dp), dimension(0:JMAX,0:IMAX) :: a_temp_b
real(dp), dimension(0:JMAX,0:IMAX) :: a_temph_b
real(dp), dimension(0:JMAX,0:IMAX) :: a_Q_bm
real(dp), dimension(0:JMAX,0:IMAX) :: a_Q_tld
real(dp), dimension(0:JMAX,0:IMAX) :: a_Q_b_tot
real(dp), dimension(0:JMAX,0:IMAX) :: a_H_w
real(dp), dimension(0:JMAX,0:IMAX) :: a_accum
real(dp), dimension(0:JMAX,0:IMAX) :: a_evap
real(dp), dimension(0:JMAX,0:IMAX) :: a_runoff
real(dp), dimension(0:JMAX,0:IMAX) :: a_as_perp
real(dp), dimension(0:JMAX,0:IMAX) :: a_temp_s
real(dp), dimension(0:JMAX,0:IMAX) :: a_am_perp
real(dp), dimension(0:JMAX,0:IMAX) :: a_am_perp_st
real(dp), dimension(0:JMAX,0:IMAX) :: a_zs_neu
real(dp), dimension(0:JMAX,0:IMAX) :: a_zm_neu
real(dp), dimension(0:JMAX,0:IMAX) :: a_zb_neu
real(dp), dimension(0:JMAX,0:IMAX) :: a_zl_neu
real(dp), dimension(0:JMAX,0:IMAX) :: a_H_c_neu
real(dp), dimension(0:JMAX,0:IMAX) :: a_H_t_neu
real(dp), dimension(0:JMAX,0:IMAX) :: a_zs_ref
real(dp), dimension(0:JMAX,0:IMAX) :: a_accum_present
real(dp), dimension(0:JMAX,0:IMAX) :: a_precip_ma_present
real(dp), dimension(0:JMAX,0:IMAX) :: a_precip_ma_lgm_anom
real(dp), dimension(0:JMAX,0:IMAX) :: a_temp_ma_present
real(dp), dimension(0:JMAX,0:IMAX) :: a_temp_mj_present
real(dp), dimension(0:JMAX,0:IMAX) :: a_temp_ma_lgm_anom
real(dp), dimension(0:JMAX,0:IMAX) :: a_temp_mj_lgm_anom
real(dp), dimension(-JMAX:JMAX,-IMAX:IMAX) :: a_dist_dxdy
real(dp), dimension(0:JMAX,0:IMAX,12) :: a_precip_present
real(dp), dimension(0:JMAX,0:IMAX,12) :: a_precip_lgm_anom
real(dp), dimension(0:JMAX,0:IMAX,12) :: a_gamma_precip_lgm_anom
real(dp), dimension(0:JMAX,0:IMAX,12) :: a_temp_mm_present
real(dp), dimension(0:JMAX,0:IMAX,12) :: a_temp_mm_lgm_anom
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_d_help_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_vx_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_vy_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_vz_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_temp_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_temp_c_neu
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_temp_c_m
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_age_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_age_c_neu
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_txz_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_tyz_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_sigma_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_enh_c
real(dp), dimension(0:JMAX,0:IMAX) :: a_de_ssa
real(dp), dimension(0:JMAX,0:IMAX) :: a_vis_int_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_vx_g
real(dp), dimension(0:JMAX,0:IMAX) :: a_vy_g
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_d_help_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_vx_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_vy_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_vz_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_omega_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_omega_t_neu
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_temp_t_m
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_age_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_age_t_neu
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_txz_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_tyz_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_sigma_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_enh_t
real(dp), dimension(0:KRMAX,0:JMAX,0:IMAX) :: a_temp_r
real(dp), dimension(0:KRMAX,0:JMAX,0:IMAX) :: a_temp_r_neu
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_enth_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_enth_c_neu
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_omega_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_omega_c_neu
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_enth_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_enth_t_neu
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_dxx_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_dyy_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_dxy_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_dxz_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_dyz_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_de_c
real(dp), dimension(0:KCMAX,0:JMAX,0:IMAX) :: a_lambda_shear_c
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_dxx_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_dyy_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_dxy_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_dxz_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_dyz_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_de_t
real(dp), dimension(0:KTMAX,0:JMAX,0:IMAX) :: a_lambda_shear_t
real(dp) :: a_RHO
real(dp) :: a_RHO_W
real(dp) :: a_RHO_SW
real(dp) :: a_L
real(dp) :: a_G
real(dp) :: a_NUE
real(dp) :: a_BETA
real(dp) :: a_DELTA_TM_SW
real(dp) :: a_OMEGA_MAX
real(dp) :: a_H_R
real(dp) :: a_RHO_C_R
real(dp) :: a_KAPPA_R
real(dp) :: a_RHO_A
real(dp) :: a_R_T_IMQ
real(dp) :: a_R
real(dp) :: a_A
real(dp) :: a_B
real(dp) :: a_LAMBDA0
real(dp) :: a_PHI0
real(dp) :: a_S_STAT_0
real(dp) :: a_BETA1_0
real(dp) :: a_BETA1_LT_0
real(dp) :: a_BETA1_HT_0
real(dp) :: a_BETA2_0
real(dp) :: a_BETA2_LT_0
real(dp) :: a_BETA2_HT_0
real(dp) :: a_PHI_SEP_0
real(dp) :: a_PMAX_0
real(dp) :: a_MU_0
real(dp), dimension(-256:255) :: a_RF_IMQ
real(dp), dimension(-256:255) :: a_KAPPA_IMQ
real(dp), dimension(-256:255) :: a_C_IMQ
real(dp) :: a_year_zero
integer(i2b) :: a_forcing_flag
integer(i4b) :: a_n_core
real(dp), dimension(a_n_core):: a_lambda_core
real(dp), dimension(a_n_core) :: a_phi_core
real(dp), dimension(a_n_core) :: a_x_core
real(dp), dimension(a_n_core) :: a_y_core
integer(i4b) :: a_grip_time_min
integer(i4b) :: a_grip_time_stp
integer(i4b) :: a_grip_time_max
integer(i4b) :: a_ndata_grip
real(dp), dimension(0:a_ndata_grip) :: a_griptemp
integer(i4b) :: a_gi_time_min
integer(i4b) :: a_gi_time_stp
integer(i4b) :: a_gi_time_max
integer(i4b) :: a_ndata_gi
real(dp), dimension(0:a_ndata_gi) :: a_glacial_index
integer(i4b) :: a_specmap_time_min
integer(i4b) :: a_specmap_time_stp
integer(i4b) :: a_specmap_time_max
integer(i4b) :: a_ndata_specmap
real(dp), dimension(0:a_ndata_specmap) :: a_specmap_zsl
real(dp) :: a_time_target_topo_init
real(dp) :: a_time_target_topo_final
integer(i2b), dimension(0:JMAX,0:IMAX) :: a_maske_target
real(dp), dimension(0:JMAX,0:IMAX) :: a_zs_target
real(dp), dimension(0:JMAX,0:IMAX) :: a_zb_target
real(dp), dimension(0:JMAX,0:IMAX) :: a_zl_target
real(dp), dimension(0:JMAX,0:IMAX) :: a_H_target
integer(i2b), dimension(0:JMAX,0:IMAX) :: a_maske_maxextent
integer(i4b) :: a_ncid_temp_precip
integer(i4b) :: a_ndata_temp_precip
real(dp) :: a_temp_precip_time_min
real(dp) :: a_temp_precip_time_stp
real(dp) :: a_temp_precip_time_max
real(dp), dimension(0:a_ndata_temp_precip) :: a_temp_precip_time
real(dp), dimension(-10000:10000) :: a_kei
real(dp) :: a_kei_r_max
real(dp) :: a_kei_r_incr
real(dp) :: a_fc
real(dp) :: a_objf_test
real(dp) :: a_mult_test
real(dp), dimension(-256:255) :: a_c_int_table
real(dp), dimension(-524288:524287) :: a_c_int_inv_table
integer(i4b) :: a_n_temp_min
integer(i4b) :: a_n_temp_max
integer(i4b) :: a_n_enth_min
integer(i4b) :: a_n_enth_max
real(dp) :: a_L_inv
real(dp) :: a_L_eto
real(dp) :: a_RHO_I_IMQ
real(dp) :: a_RHO_C_IMQ
real(dp) :: a_KAPPA_C_IMQ
real(dp) :: a_C_C_IMQ


maske = a_maske
maske_help = a_maske_help
maske_neu = a_maske_neu
n_cts = a_n_cts
n_cts_neu = a_n_cts_neu
kc_cts = a_kc_cts
kc_cts_neu = a_kc_cts_neu
flag_grounding_line_1 = a_flag_grounding_line_1
flag_grounding_line_2 = a_flag_grounding_line_2
flag_calving_front_1 = a_flag_calving_front_1
flag_calving_front_2 = a_flag_calving_front_2
flag_shelfy_stream_x = a_flag_shelfy_stream_x
flag_shelfy_stream_y = a_flag_shelfy_stream_y
flag_shelfy_stream = a_flag_shelfy_stream
xi = a_xi
eta = a_eta
zeta_c = a_zeta_c
zeta_t = a_zeta_t
zeta_r = a_zeta_r
aa = a_aa
flag_aa_nonzero = a_flag_aa_nonzero
ea = a_ea
eaz_c = a_eaz_c
eaz_c_quotient = a_eaz_c_quotient
lambda = a_lambda
phi = a_phi
area = a_area
sq_g11_g = a_sq_g11_g
sq_g22_g = a_sq_g22_g
insq_g11_g = a_insq_g11_g
insq_g22_g = a_insq_g22_g
sq_g11_sgx = a_sq_g11_sgx
sq_g11_sgy = a_sq_g11_sgy
sq_g22_sgx = a_sq_g22_sgx
sq_g22_sgy = a_sq_g22_sgy
insq_g11_sgx = a_insq_g11_sgx
insq_g22_sgy = a_insq_g22_sgy
zs%v = a_zs
zm%v = a_zm
zb = a_zb
zl = a_zl
zl0 = a_zl0
wss = a_wss
flex_rig_lith = a_flex_rig_lith
time_lag_asth = a_time_lag_asth
H_c%v = a_H_c
H_t%v = a_H_t
dzs_dxi%v = a_dzs_dxi
dzm_dxi = a_dzm_dxi
dzb_dxi = a_dzb_dxi
dH_c_dxi = a_dH_c_dxi
dH_t_dxi = a_dH_t_dxi
dzs_deta%v = a_dzs_deta
dzm_deta = a_dzm_deta
dzb_deta = a_dzb_deta
dH_c_deta = a_dH_c_deta
dH_t_deta = a_dH_t_deta
dzs_dxi_g%v = a_dzs_dxi_g
dzm_dxi_g%v = a_dzm_dxi_g
dzb_dxi_g = a_dzb_dxi_g
dH_c_dxi_g%v = a_dH_c_dxi_g
dH_t_dxi_g%v = a_dH_t_dxi_g
dzs_deta_g%v = a_dzs_deta_g
dzm_deta_g%v = a_dzm_deta_g
dzb_deta_g = a_dzb_deta_g
dH_c_deta_g%v = a_dH_c_deta_g
dH_t_deta_g%v = a_dH_t_deta_g
dzs_dtau%v = a_dzs_dtau
dzm_dtau%v = a_dzm_dtau
dzb_dtau = a_dzb_dtau
dzl_dtau = a_dzl_dtau
dH_c_dtau%v = a_dH_c_dtau
dH_t_dtau%v = a_dH_t_dtau
p_weert = a_p_weert
q_weert = a_q_weert
p_weert_inv = a_p_weert_inv
c_slide%v = a_c_slide
d_help_b%v = a_d_help_b
c_drag = a_c_drag
p_b_w = a_p_b_w
vx_b = a_vx_b
vy_b = a_vy_b
#if (ENHMOD == 5)
vx_m%v = a_vx_m
vy_m%v = a_vy_m
#else
vx_m = a_vx_m
vy_m = a_vy_m
#endif
ratio_sl_x = a_ratio_sl_x
ratio_sl_y = a_ratio_sl_y
vx_b_g = a_vx_b_g
vy_b_g = a_vy_b_g
vz_b%v = a_vz_b
vz_m%v = a_vz_m
vx_s_g = a_vx_s_g
vy_s_g = a_vy_s_g
#if (ENHMOD == 5)
vz_s%v = a_vz_s
#else
vz_s = a_vz_s
#endif
flui_ave_sia = a_flui_ave_sia
h_diff%v = a_h_diff
#if (ENHMOD == 5)
qx%v = a_qx
qy%v = a_qy
#else
qx = a_qx
qy = a_qy
#endif
q_gl_g = a_q_gl_g
q_geo = a_q_geo
temp_b = a_temp_b
temph_b = a_temph_b
Q_bm%v = a_Q_bm
#if (CALCMOD == 1 || CALCMOD == 2)
Q_tld%v = a_Q_tld
#else
Q_tld = a_Q_tld
#endif
Q_b_tot%v = a_Q_b_tot
H_w = a_H_w
accum = a_accum
evap = a_evap
#if (PDD_MODIFIER == 2)
runoff%v = a_runoff
as_perp%v = a_as_perp
#else
runoff = a_runoff
as_perp = a_as_perp
#endif
temp_s%v = a_temp_s
#if (CALCMOD == 0 || CALCMOD == 2)
am_perp = a_am_perp
am_perp_st = a_am_perp_st
#elif (CALCMOD == 1)
am_perp%v = a_am_perp
am_perp_st%v = a_am_perp_st
#endif
zs_neu%v = a_zs_neu
zm_neu%v = a_zm_neu
zb_neu = a_zb_neu
zl_neu = a_zl_neu
H_c_neu%v = a_H_c_neu
H_t_neu%v = a_H_t_neu
zs_ref = a_zs_ref
accum_present = a_accum_present
precip_ma_present = a_precip_ma_present
precip_ma_lgm_anom = a_precip_ma_lgm_anom
temp_ma_present%v = a_temp_ma_present
#if (PDD_MODIFIER == 2)
temp_mj_present%v = a_temp_mj_present
#else
temp_mj_present = a_temp_mj_present
#endif
temp_ma_lgm_anom = a_temp_ma_lgm_anom
temp_mj_lgm_anom = a_temp_mj_lgm_anom
dist_dxdy = a_dist_dxdy
precip_present = a_precip_present
precip_lgm_anom = a_precip_lgm_anom
gamma_precip_lgm_anom = a_gamma_precip_lgm_anom
temp_mm_present = a_temp_mm_present
temp_mm_lgm_anom = a_temp_mm_lgm_anom
d_help_c%v = a_d_help_c
vx_c%v = a_vx_c
vy_c%v = a_vy_c
vz_c%v = a_vz_c
temp_c%v = a_temp_c
temp_c_neu%v = a_temp_c_neu
temp_c_m%v = a_temp_c_m
age_c%v = a_age_c
age_c_neu%v = a_age_c_neu
txz_c = a_txz_c
tyz_c = a_tyz_c
sigma_c%v = a_sigma_c
#if (ENHMOD == 5)
enh_c%v = a_enh_c
#else
enh_c = a_enh_c
#endif
de_ssa = a_de_ssa
vis_int_g = a_vis_int_g
vx_g = a_vx_g
vy_g = a_vy_g
d_help_t%v = a_d_help_t
vx_t%v = a_vx_t
vy_t%v = a_vy_t
vz_t%v = a_vz_t
#if (CALCMOD == 1 || CALCMOD == 2)
omega_t%v = a_omega_t
omega_t_neu%v = a_omega_t_neu
#else
omega_t = a_omega_t
omega_t_neu = a_omega_t_neu
#endif
temp_t_m%v = a_temp_t_m
#if (CALCMOD == 2 || CALCMOD == 0)
age_t = a_age_t
age_t_neu = a_age_t_neu
#else
age_t%v = a_age_t
age_t_neu%v = a_age_t_neu
#endif
txz_t = a_txz_t
tyz_t = a_tyz_t
sigma_t%v = a_sigma_t
#if (ENHMOD == 5)
enh_t%v = a_enh_t
#else
enh_t = a_enh_t
#endif
temp_r%v = a_temp_r
temp_r_neu%v = a_temp_r_neu
#if (CALCMOD == 2)
enth_c%v = a_enth_c
enth_c_neu%v = a_enth_c_neu
#elif (CALCMOD == 0 || CALCMOD == 1)
enth_c = a_enth_c
enth_c_neu = a_enth_c_neu
#endif
#if (CALCMOD == 2)
omega_c%v = a_omega_c
omega_c_neu%v = a_omega_c_neu
#elif (CALCMOD == 0 || CALCMOD == 1)
omega_c = a_omega_c
omega_c_neu = a_omega_c_neu
#endif
enth_t = a_enth_t
enth_t_neu = a_enth_t_neu
#if (CALCMOD == 5 || ENHMOD == 5)
dxx_c%v = a_dxx_c
dxy_c%v = a_dxy_c
dxz_c%v = a_dxz_c
dyy_c%v = a_dyy_c
dyz_c%v = a_dyz_c
#else
dxx_c = a_dxx_c
dxy_c = a_dxy_c
dxz_c = a_dxz_c
dyy_c = a_dyy_c
dyz_c = a_dyz_c
#endif
#if (ENHMOD == 5)
de_c%v = a_de_c
lambda_shear_c%v = a_lambda_shear_c
lambda_shear_t%v = a_lambda_shear_t
#else
de_c = a_de_c
lambda_shear_c = a_lambda_shear_c
lambda_shear_t = a_lambda_shear_t
#endif
dxx_t = a_dxx_t
dyy_t = a_dyy_t
dxy_t = a_dxy_t
dxz_t = a_dxz_t
dyz_t = a_dyz_t
de_t = a_de_t
RHO = a_RHO
RHO_W = a_RHO_W
RHO_SW = a_RHO_SW
L = a_L
G = a_G
NUE = a_NUE
BETA = a_BETA
DELTA_TM_SW = a_DELTA_TM_SW
OMEGA_MAX = a_OMEGA_MAX
H_R = a_H_R
RHO_C_R = a_RHO_C_R
KAPPA_R = a_KAPPA_R
RHO_A = a_RHO_A
R_T_IMQ = a_R_T_IMQ
R = a_R
A = a_A
B = a_B
LAMBDA0 = a_LAMBDA0
PHI0 = a_PHI0
S_STAT_0 = a_S_STAT_0
BETA1_0 = a_BETA1_0
BETA1_LT_0 = a_BETA1_LT_0
BETA1_HT_0 = a_BETA1_HT_0
BETA2_0 = a_BETA2_0
BETA2_LT_0 = a_BETA2_LT_0
BETA2_HT_0 = a_BETA2_HT_0
PHI_SEP_0 = a_PHI_SEP_0
PMAX_0 = a_PMAX_0
MU_0 = a_MU_0
RF_IMQ = a_RF_IMQ
KAPPA_IMQ = a_KAPPA_IMQ
C_IMQ = a_C_IMQ
year_zero = a_year_zero
forcing_flag = a_forcing_flag
n_core = a_n_core
#if OUTSER==3
allocate(lambda_core(a_n_core))
allocate(phi_core(a_n_core))
allocate(x_core(a_n_core))
allocate(y_core(a_n_core))
lambda_core = a_lambda_core
phi_core = a_phi_core
x_core = a_x_core
y_core = a_y_core
#endif
grip_time_min = a_grip_time_min
grip_time_stp = a_grip_time_stp
grip_time_max = a_grip_time_max
ndata_grip = a_ndata_grip
#if TSURFACE==4
allocate(griptemp(0:a_ndata_grip))
griptemp = a_griptemp
#endif
gi_time_min = a_gi_time_min
gi_time_stp = a_gi_time_stp
gi_time_max = a_gi_time_max
ndata_gi = a_ndata_gi
#if TSURFACE==5
allocate(glacial_index(0:a_ndata_gi))
glacial_index = a_glacial_index
#endif
specmap_time_min = a_specmap_time_min
specmap_time_stp = a_specmap_time_stp
specmap_time_max = a_specmap_time_max
ndata_specmap = a_ndata_specmap
#if (SEA_LEVEL==3)
allocate(specmap_zsl(0:a_ndata_specmap))
specmap_zsl = a_specmap_zsl
#endif
time_target_topo_init = a_time_target_topo_init
time_target_topo_final = a_time_target_topo_final
maske_target = a_maske_target
zs_target = a_zs_target
zb_target = a_zb_target
zl_target = a_zl_target
H_target = a_H_target
maske_maxextent = a_maske_maxextent
ncid_temp_precip = a_ncid_temp_precip
ndata_temp_precip = a_ndata_temp_precip
temp_precip_time_min = a_temp_precip_time_min
temp_precip_time_stp = a_temp_precip_time_stp
temp_precip_time_max = a_temp_precip_time_max
#if ( (TSURFACE==6) && (ACCSURFACE==6) )
allocate(temp_precip_time(0:a_ndata_temp_precip))
temp_precip_time = a_temp_precip_time
#endif
kei = a_kei
kei_r_max = a_kei_r_max
kei_r_incr = a_kei_r_incr
fc%v = a_fc
objf_test%v = a_objf_test
mult_test = a_mult_test
c_int_table = a_c_int_table
c_int_inv_table = a_c_int_inv_table
n_temp_min = a_n_temp_min
n_temp_max = a_n_temp_max
n_enth_min = a_n_enth_min
n_enth_max = a_n_enth_max
L_inv = a_L_inv
L_eto = a_L_eto
RHO_I_IMQ = a_RHO_I_IMQ
RHO_C_IMQ = a_RHO_C_IMQ
KAPPA_C_IMQ = a_KAPPA_C_IMQ
C_C_IMQ = a_C_C_IMQ

end subroutine var_transfer
