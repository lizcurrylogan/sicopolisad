#!/bin/bash
# This script takes variables to re-build and compile the plain forward version of of sicoAD
# versus the original forward version of sicoAD

################################################################################

function page_break()
{
for line in {1..50}
do
echo "-------------------------------------------------------------------------"
echo "-------------------------------------------------------------------------"
done
echo "                              "$1" DONE"   
}

################################################################################
# Note: this function should be called from src/

function compare_outputs()
{

echo "-------------------------------------------------" >> regression_log.txt
echo "Comparing "$1" for test: "$2 >> regression_log.txt

if [ $1 == "COSTS" ]; then ### cost comparison
 
  FILE_ORIG="../sico_out/"$2"_ORIG/"$2"_ORIG_COST.dat"
  FILE_GRDCHK="../sico_out/"$2"_GRDCHK/CostVals_"$2"_GRDCHK.dat"
  DIFF=$(diff <(head -n 1 $FILE_ORIG) <(head -n 1 $FILE_GRDCHK))

  if [ "${DIFF:-0}" == 0 ]; then
    echo "YAY! SAME costs: ORIG v. GRDCHK. =)" >> regression_log.txt
  else
    echo "BOO! DIFF costs: ORIG v. GRDCHK. =(: " >> regression_log.txt
    echo "$DIFF" >> regression_log.txt
  fi

elif [ $1 == "GRADIENTS" ]; then ### gradient comparison
  
  FILE_GRDCHK="../sico_out/"$2"_GRDCHK/GradientVals_"$2"_GRDCHK.dat"
  FILE_AD="../sico_out/OAD/AD_grdchk_"$2".dat"
  DIFF=$(diff -w --suppress-common-lines $FILE_GRDCHK $FILE_AD)

  if [ "${DIFF:-0}" == 0 ]; then
    echo "YAY! SAME gradients: GRDCHK v. OAD. =)" >> regression_log.txt
  else
    echo "BOO! DIFF gradients: GRDCHK v. OAD. =(" >> regression_log.txt
    echo "$DIFF" >> regression_log.txt
  fi

fi

}

################################################################################

# These are the header files for our regression tests:
# --- This first one is simply 100 years instead of 10 (the second one):
declare -a HEADER_FILE=("ref_output_100y" "CALCMOD1_100y" "CALCMOD2_100y" "ENHMOD5_100y" "PDDMOD2_100y" )
#declare -a HEADER_FILE=("ref_output" "CALCMOD1" "CALCMOD2" "ENHMOD5" "PDDMOD2" )
#debugging this script:
#declare -a HEADER_FILE=("ref_output")

# These are the arguments given in _OAD mode (plain, tape, or adjoint):
declare -a MODES=("-p" "-t" "-a")

# Remove all old OAD results:
if [ -e ../sico_out/OAD ]; then
   if [ "$(ls -A ../sico_out/OAD)" ]; then
      rm ../sico_out/OAD/*
      echo "OAD output dir emptied."
   fi
else 
   mkdir ../sico_out/OAD
   echo "OAD output did not exists and we made it."
fi

# Remove old regression_log:
if [ -e regression_log.txt ]; then
   rm regression_log.txt
   echo "regression_log.txt was removed"
fi

# TESTS: _ORIG, _GRDCHK, _OAD -------------------------------------------

for test in "${HEADER_FILE[@]}"
do

# ------------------------ _ORIG
   HEADER=$test"_ORIG" 
   cd ../runs
   ./singleRunSico.sh -fm $HEADER
   page_break $HEADER

# ------------------------ _GRDCHK
   HEADER=$test"_GRDCHK" 
   ./singleRunSico.sh -fm $HEADER
   mv ../src/GradientVals_$HEADER.dat ../sico_out/$HEADER
   mv ../src/CostVals_$HEADER.dat ../sico_out/$HEADER
   cd ../src
   compare_outputs "COSTS" $test 
   page_break $HEADER

# ------------------------ _OAD
   HEADER=$test"_OAD" 

   # compiling OAD version:
   make -f MakefileOpenAD clean
   make -f MakefileOpenAD HEADER=$HEADER
   make -f MakefileOpenAD driver

   # loop over _OAD MODES: plain (-p), tape (-t), adjoint (-a)
   for mode in "${MODES[@]}"
   do

      # remove the directory if it already exists (will cause sico problems):
      if [ -d ../sico_out/$HEADER ]; then
         rm -r ../sico_out/$HEADER
         echo "output dir "$HEADER" existed and was removed"
      fi

      OUT_FNAME="out_"$HEADER$mode".dat"

      # this is the execution:
      ./driver $mode > $OUT_FNAME
      # moving these files:
      mv AD_COST    ../sico_out/OAD/"AD_COST"$mode".dat"
      mv $OUT_FNAME ../sico_out/OAD
      
      if [ $mode == "-a" ]; then 
         mv AD_Vals.dat               ../sico_out/OAD/"AD_Vals_"$test".dat"
         mv AD_Vals_for_grdchk.dat    ../sico_out/OAD/"AD_grdchk_"$test".dat"
         compare_outputs "GRADIENTS" $test 
      fi
 
      page_break $HEADER$mode

   done
done
