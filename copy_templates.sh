#!/bin/bash
# (Selection of shell)

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#   Making local, not version-controlled copies of the scripts for executing
#   SICOPOLIS and the run-specification (header) files.
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

CP=/bin/cp

RUN_DIR=./runs
HEADER_DIR=${RUN_DIR}/headers
TEMPLATE_DIR_NAME=templates

echo "Copying scripts *.sh"
echo "        from ${RUN_DIR}/${TEMPLATE_DIR_NAME}"
echo "        to ${RUN_DIR} ..."
echo " "
$CP -f ${RUN_DIR}/${TEMPLATE_DIR_NAME}/*.sh ${RUN_DIR}

echo "Copying headers *.h"
echo "        from ${HEADER_DIR}/${TEMPLATE_DIR_NAME}"
echo "        to ${HEADER_DIR} ..."
echo " "
$CP -f ${HEADER_DIR}/${TEMPLATE_DIR_NAME}/*.h ${HEADER_DIR}

echo "... done!"
echo " "

#--------
