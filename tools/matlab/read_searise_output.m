function [time, x, y, ivol, iareag, iareaf, ...
          usurf, topg, thk, ...
          acab, bmelt, bwa, bwp, dHdt, mask, gline_flux, ...
          uvelsurf, vvelsurf, wvelsurf, velsurf_hor, ...
          uvelbase, vvelbase, wvelbase, velbase_hor, ...
          tempsurf, tempbase] ...
        = read_searise_output(runname, outpath, filename)
% READ_SEARISE_OUTPUT Reading of SeaRISE output data.
% =========================================================================
% read_searise_output  Date: 2013-05-30
%
% Usage: 
%
%   [time, x, y, ivol, iareag, iareaf] ...
%      = read_searise_output(outpath, filename)
%
%      or
%
%   [time, x, y, ivol, iareag, iareaf, ...
%    usurf, topg, thk, ...
%    acab, bmelt, bwa, bwp, dHdt, mask, gline_flux, ...
%    uvelsurf, vvelsurf, wvelsurf, velsurf_hor, ...
%    uvelbase, vvelbase, wvelbase, velbase_hor, ...
%    tempsurf, tempbase] ...
%      = read_searise_output(outpath, filename)
%
% Description:
%   read_searise_output reads NetCDF output data of SICOPOLIS in the
%   SeaRISE standard format. Results are returned in the vectors
%   time, x, y, ivol, iareag, iareaf, and in the 3-D arrays
%   usurf, topg, thk, acab, bmelt, bwa, bwp, dHdt, mask, gline_flux,
%   uvelsurf, vvelsurf, wvelsurf, uvelbase, vvelbase, wvelbase,
%   tempsurf, tempbase.
%   If only the 6 vectors are specified as output variables,
%   only these will be read.
%
% Input:
%   runname  - name of SICOPOLIS simulation (string)
%   outpath  - path of SeaRISE output data file (without trailing "/")
%   filename - name of SeaRISE output data file
%
% Output:
%   time        - Time [a]
%   x           - x-coordinate of the grid point i [m]
%   y           - y-coordinate of the grid point j [m]
%   ivol        - Ice volume [m3]
%   iareag      - Area of grounded ice [m2]
%   iareaf      - Area of floating ice [m2]
%   usurf       - Topography of the free surface [m]
%   topg        - Topography of the lithosphere surface [m]
%   thk         - Ice thickness [m]
%   acab        - Mass balance at the ice surface [m a-1]
%   bmelt       - Basal melting rate [m a-1]
%   bwa         - Effective thickness of subglacial water [m]
%   bwp         - Basal water pressure [Pa]
%   dHdt        - Rate of change of the ice thickness [m a-1]
%   mask        - Ice-land-ocean mask [0 - ocean, 1 - grounded ice,
%                                      2 - floating ice, 3 - ice-free land]
%   gline_flux  - Horizontal volume flux across the grounding line [m2 a-1]
%   uvelsurf    - Horizontal velocity vx at the ice surface [m a-1]
%   vvelsurf    - Horizontal velocity vy at the ice surface [m a-1]
%   wvelsurf    - Vertical velocity vz at the ice surface [m a-1]
%   velsurf_hor - Horizontal velocity vh = sqrt(vx^2+vy^2)
%                 at the ice surface [m a-1]
%   uvelbase    - Horizontal velocity vx at the ice base [m a-1]
%   vvelbase    - Horizontal velocity vy at the ice base [m a-1]
%   wvelbase    - Vertical velocity vz at the ice base [m a-1]
%   velbase_hor - Horizontal velocity vh = sqrt(vx^2+vy^2)
%                 at the ice base [m a-1]
%   tempsurf    - Temperature at the ice surface [K]
%   tempbase    - Temperature at the ice base [K]
%
% Author:
%   Ralf Greve
% =========================================================================

if nargout == 6
   flag_2d_read = 0;
   % only scalar output, but not 2-d output will be read 
elseif nargout == 26
   flag_2d_read = 1;
   % both scalar and 2-d output will be read
else
   disp('read_searise_output:')
   disp('Wrong number of output arguments specified in call!')
   return
end

%-------- Opening NetCDF file --------

if strcmp(filename(end-2:end), '.nc')
   disp(' ')
   disp(['Name of SeaRISE file: ' filename])
else
   filename = [filename '.nc'];
   disp(' ')
   disp(['Name of SeaRISE file: ' filename])
end

try
   ncid = netcdf.open([outpath '/' filename], 'nowrite');
catch
   disp(' ')
   disp(['SeaRISE file not found in directory ' outpath '.'])
   outpath = [outpath '/' runname];
   disp(['Trying ' outpath ' instead...'])
   try
      ncid = netcdf.open([outpath '/' filename], 'nowrite');
      disp('SeaRISE file found. Continuing...')
   catch err
      disp(['SeaRISE file not found in directory ' outpath ' either.'])
      disp('Giving up.')
      rethrow(err)
   end
end

%-------- Reading of data from NetCDF file --------

%  ------ Time

varid   = netcdf.inqVarID(ncid,'time');
time    = netcdf.getVar(ncid,varid);

%  ------ Projection coordinates

varid   = netcdf.inqVarID(ncid,'x');
x       = netcdf.getVar(ncid,varid);

varid   = netcdf.inqVarID(ncid,'y');
y       = netcdf.getVar(ncid,varid);

%  ------ Scalar variables

varid   = netcdf.inqVarID(ncid,'ivol');
ivol    = netcdf.getVar(ncid,varid);

varid   = netcdf.inqVarID(ncid,'iareag');
iareag  = netcdf.getVar(ncid,varid);

varid   = netcdf.inqVarID(ncid,'iareaf');
iareaf  = netcdf.getVar(ncid,varid);

%  ------ 2D fields

if flag_2d_read == 1

   varid       = netcdf.inqVarID(ncid,'usurf');
   usurf       = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'topg');
   topg        = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'thk');
   thk         = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'acab');
   acab        = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'bmelt');
   bmelt       = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'bwa');
   bwa         = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'bwp');
   bwp         = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'dHdt');
   dHdt        = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'mask');
   mask        = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'gline_flux');
   gline_flux  = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'uvelsurf');
   uvelsurf    = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'vvelsurf');
   vvelsurf    = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'wvelsurf');
   wvelsurf    = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'velsurf_hor');
   velsurf_hor = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'uvelbase');
   uvelbase    = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'vvelbase');
   vvelbase    = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'wvelbase');
   wvelbase    = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'velbase_hor');
   velbase_hor = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'tempsurf');
   tempsurf    = netcdf.getVar(ncid,varid);

   varid       = netcdf.inqVarID(ncid,'tempbase');
   tempbase    = netcdf.getVar(ncid,varid);

end

%-------- Closing NetCDF file --------

netcdf.close(ncid);

disp(' ')
disp('Done.')
disp(' ')

end % function read_searise_output
%
