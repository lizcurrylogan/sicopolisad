function [time, dts_gi, z_sl, ...
          xi, eta, sigma_level_c, sigma_level_t, sigma_level_r, ...
          lon, lat, ...
          H_R, V_tot, A_grounded, A_floating, ...
          temp_s, as_perp, ...
          maske, n_cts, kc_cts, ...
          zs, zm, zb, zl, H_cold, H_temp, H, ...
          Q_bm, Q_tld, am_perp, ...
          qx, qy, ...
          dzs_dtau, dzm_dtau, dzb_dtau, dzl_dtau, ...
          dH_c_dtau, dH_t_dtau, dH_dtau, ...
          vx_b_g, vy_b_g, vz_b, vh_b, vx_s_g, vy_s_g, vz_s, vh_s, ...
          temp_b, temph_b, p_b_w, H_w, q_gl_g, ...
          vx_c, vy_c, vz_c, vx_t, vy_t, vz_t, ...
          temp_c, omega_t, temp_r, ...
          enth_c, enth_t, omega_c, age_c, age_t] ...
        = read_time_slice_nc(runname, ergnum, outpath, flag_3d_output)
% READ_TIME_SLICE_NC Reading of time slice data produced by SICOPOLIS.
% =========================================================================
% read_time_slice_nc  Date: 2015-12-16
%
% Usage: 
%
%   [time, dts_gi, z_sl, ...
%    xi, eta, sigma_level_c, sigma_level_t, sigma_level_r, ...
%    lon, lat, ...
%    H_R, V_tot, A_grounded, A_floating, ...
%    temp_s, as_perp, ...
%    maske, n_cts, kc_cts, ...
%    zs, zm, zb, zl, H_cold, H_temp, H, ...
%    Q_bm, Q_tld, am_perp, ...
%    qx, qy, ...
%    dzs_dtau, dzm_dtau, dzb_dtau, dzl_dtau, ...
%    dH_c_dtau, dH_t_dtau, dH_dtau, ...
%    vx_b_g, vy_b_g, vz_b, vh_b, vx_s_g, vy_s_g, vz_s, vh_s, ...
%    temp_b, temph_b, p_b_w, H_w, q_gl_g] ...
%      = read_time_slice_nc(runname, ergnum, outpath, flag_3d_output)
%
%      or
%
%   [time, dts_gi, z_sl, ...
%    xi, eta, sigma_level_c, sigma_level_t, sigma_level_r, ...
%    lon, lat, ...
%    H_R, V_tot, A_grounded, A_floating, ...
%    temp_s, as_perp, ...
%    maske, n_cts, kc_cts, ...
%    zs, zm, zb, zl, H_cold, H_temp, H, ...
%    Q_bm, Q_tld, am_perp, ...
%    qx, qy, ...
%    dzs_dtau, dzm_dtau, dzb_dtau, dzl_dtau, ...
%    dH_c_dtau, dH_t_dtau, dH_dtau, ...
%    vx_b_g, vy_b_g, vz_b, vh_b, vx_s_g, vy_s_g, vz_s, vh_s, ...
%    temp_b, temph_b, p_b_w, H_w, q_gl_g, ...
%    vx_c, vy_c, vz_c, vx_t, vy_t, vz_t, ...
%    temp_c, omega_t, temp_r, ...
%    enth_c, enth_t, omega_c, age_c, age_t] ...
%      = read_time_slice_nc(runname, ergnum, outpath, flag_3d_output)
%
% Description:
%   read_time_slice_nc reads time slice output data of SICOPOLIS for
%   terrestrial ice sheets in NetCDF format (from file runname-ergnum.nc).
%   If the 3-d fields are not specified as output variables,
%   they will not be read.
%
% Input:
%   runname        - name of SICOPOLIS simulation (string)
%   ergnum         - number of time slice file
%                    (string, with leading zeros, 4 digits)
%   outpath        - path of time slice file (string, without trailing "/")
%   flag_3d_output - 0: time slice data file contains only 2-d fields
%                    1: time slice data file contains the full set of
%                       2-d and 3-d fields
%
% Output:
%   time          - Time [a]
%   dts_gi        - Surface temperature anomaly [C] or glacial index [1]
%   z_sl          - Sea level [m]
%   xi            - x-coordinate of the grid point i [m]
%   eta           - y-coordinate of the grid point j [m]
%   sigma_level_c - sigma-coordinate of the grid point kc
%   sigma_level_t - sigma-coordinate of the grid point kt
%   sigma_level_r - sigma-coordinate of the grid point kr
%   lon           - Geographical longitude [deg E]
%   lat           - Geographical latitude [deg N]
%   H_R           - Thickness of the thermal lithosphere layer [m]
%   V_tot         - Ice volume [m3]
%   A_grounded    - Area of grounded ice [m2]
%   A_floating    - Area of floating ice [m2]
%   temp_s        - Temperature at the ice surface [C]
%   as_perp       - Mass balance at the ice surface [m a-1]
%   maske         - Ice-land-ocean mask
%                     [0 - grounded ice,
%                      1 - ice-free land, 2 - ocean, 3 - floating ice]
%   n_cts         - Mask for polythermal conditions
%                    [-1 - cold_base,
%                      0 - temperate_base_with_cold_ice_above,
%                      1 - temperate_base_with_temperate_ice_above]
%   zs            - Topography of the free surface [m]
%   zm            - Topography [m] of the bottom of the upper (kc) domain
%                                          = top of the lower (kt) domain
%                   (position of the CTS for the polythermal mode,
%                   equal to zb for the cold-ice mode and the enthalpy method)
%   zb            - Topography of the ice base [m]
%   zl            - Topography of the lithosphere surface [m]
%   H_cold        - Thickness of the cold ice layer [m]
%   H_temp        - Thickness of the temperate ice layer [m]
%   H             - Ice thickness [m]
%   Q_bm          - Basal melting rate [m a-1]
%   Q_tld         - Water drainage from the temperate layer [m a-1]
%   am_perp       - Volume flux across the CTS [m a-1]
%   qx            - Horizontal volume flux qx
%                   (staggered grid variable) [m2 a-1]
%   qy            - Horizontal volume flux qy
%                   (staggered grid variable) [m2 a-1]
%   dzs_dtau      - Rate of change of zs [m a-1]
%   dzm_dtau      - Rate of change of zm [m a-1]
%   dzb_dtau      - Rate of change of zb [m a-1]
%   dzl_dtau      - Rate of change of zl [m a-1]
%   dH_c_dtau     - Rate of change of the thickness of the upper (kc) domain
%                   [m a-1]
%   dH_t_dtau     - Rate of change of the thickness of the lower (kt) domain
%                   layer [m a-1]
%   dH_dtau       - Rate of change of the ice thickness H [m a-1]
%   vx_b_g        - Horizontal velocity vx at the ice base [m a-1]
%   vy_b_g        - Horizontal velocity vy at the ice base [m a-1]
%   vz_b          - Vertical velocity vz at the ice base [m a-1]
%   vh_b          - Horizontal velocity vh = sqrt(vx^2+vy^2)
%                   at the ice base [m a-1]
%   vx_s_g        - Horizontal velocity vx at the ice surface [m a-1]
%   vy_s_g        - Horizontal velocity vy at the ice surface [m a-1]
%   vz_s          - Vertical velocity vz at the ice surface [m a-1]
%   vh_s          - Horizontal velocity vh = sqrt(vx^2+vy^2)
%                   at the ice surface [m a-1]
%   temp_b        - Temperature at the ice base [C]
%   temph_b       - Temperature at the ice base relative to PMP [C]
%   p_b_w         - Basal water pressure [Pa]
%   H_w           - Effective thickness of subglacial water [m]
%   q_gl_g        - Horizontal volume flux across the grounding line
%                   [m2 a-1]
%   vx_c          - Horizontal velocity vx in the upper (kc) ice domain
%                   (staggered grid variable) [m a-1]
%   vy_c          - Horizontal velocity vy in the upper (kc) ice domain
%                   (staggered grid variable) [m a-1]
%   vz_c          - Vertical velocity vz in the upper (kc) ice domain
%                   (staggered grid variable) [m a-1]
%   vx_t          - Horizontal velocity vx in the lower (kt) ice domain
%                   (staggered grid variable) [m a-1]
%   vy_t          - Horizontal velocity vy in the lower (kt) ice domain
%                   (staggered grid variable) [m a-1]
%   vz_t          - Vertical velocity vz in the lower (kt) ice domain
%                   (staggered grid variable) [m a-1]
%   temp_c        - Temperature in the upper (kc) ice domain [C]
%   omega_t       - Water content in the lower (kt) ice domain [1]
%   temp_r        - Temperature in the lithosphere layer [C]
%   enth_c        - Enthalpy in the upper (kc) ice domain [J kg-1]
%   enth_t        - Enthalpy in the lower (kt) ice domain [J kg-1]
%   omega_c       - Water content in the upper (kc) ice domain [1]
%   age_c         - Age in the the upper (kc) ice domain [a]
%   age_t         - Age in the the lower (kt) ice domain [a]
%
% Author:
%   Ralf Greve
% =========================================================================

if nargout == 51
   flag_3d_read = 0;   % 3-d fields will not be read
elseif nargout == 65
   flag_3d_read = 1;   % all fields (including 3-d) will be read
else
   disp(' ')
   disp('read_time_slice_nc:')
   disp('Wrong number of output arguments specified in call!')
   return
end

if (flag_3d_read == 1) && (flag_3d_output == 0)
   disp(' ')
   disp('read_time_slice_nc:')
   disp('Time slice data file does not contain 3-d fields!')
   return
end
    
%-------- Name of time slice file --------

if flag_3d_output == 0
   filename = [runname '_2d_' ergnum '.nc'];
else
   filename = [runname ergnum '.nc'];
end
    
disp(' ')
disp(['Name of time slice file: ' filename])

%-------- Opening NetCDF file --------

try
   ncid = netcdf.open([outpath '/' filename], 'nowrite');
catch
   disp(' ')
   disp(['Time slice file not found in directory ' outpath '.'])
   outpath = [outpath '/' runname];
   disp(['Trying ' outpath ' instead...'])
   try
      ncid = netcdf.open([outpath '/' filename], 'nowrite');
      disp('Time slice file found. Continuing...')
   catch err
      disp(['Time slice file not found in directory ' outpath ' either.'])
      disp('Giving up.')
      rethrow(err)
   end
end

%-------- Reading of data from NetCDF file --------

%  ------ Time

varid         = netcdf.inqVarID(ncid,'time');
time          = netcdf.getVar(ncid,varid);

%  ------ Forcings

[varname, xtype, dimids, natts] = netcdf.inqVar(ncid,2);
                                % get info about 3rd variable in the file

if strcmp(varname, 'delta_ts') || strcmp(varname, 'glac_index')
   varid      = netcdf.inqVarID(ncid, varname);
   dts_gi     = netcdf.getVar(ncid, varid);
else
   disp(' ')
   disp('read_time_slice_nc:')
   disp('3rd variable in NetCDF file is neither delta_ts nor glac_index!')
   return   
end

varid         = netcdf.inqVarID(ncid, 'z_sl');
z_sl          = netcdf.getVar(ncid, varid);

%  ------ Projection coordinates

varid         = netcdf.inqVarID(ncid, 'xi');
xi            = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'eta');
eta           = netcdf.getVar(ncid, varid);

%  ------ Sigma levels

varid         = netcdf.inqVarID(ncid, 'sigma_level_c');
sigma_level_c = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'sigma_level_t');
sigma_level_t = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'sigma_level_r');
sigma_level_r = netcdf.getVar(ncid, varid);

%  ------ Longitudes and latitudes of the grid points

varid         = netcdf.inqVarID(ncid, 'lon');
lon           = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'lat');
lat           = netcdf.getVar(ncid, varid);

%  ------ Scalar output variables

varid         = netcdf.inqVarID(ncid, 'H_R');
H_R           = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'V_tot');
V_tot         = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'A_grounded');
A_grounded    = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'A_floating');
A_floating    = netcdf.getVar(ncid, varid);

%  ------ 2-d fields

varid         = netcdf.inqVarID(ncid, 'temp_s');
temp_s        = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'as_perp');
as_perp       = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'maske');
maske         = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'n_cts');
n_cts         = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'kc_cts');
kc_cts        = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'zs');
zs            = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'zm');
zm            = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'zb');
zb            = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'zl');
zl            = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'H_cold');
H_cold        = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'H_temp');
H_temp        = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'H');
H             = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'Q_bm');
Q_bm          = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'Q_tld');
Q_tld         = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'am_perp');
am_perp       = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'qx');
qx            = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'qy');
qy            = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'dzs_dtau');
dzs_dtau      = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'dzm_dtau');
dzm_dtau      = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'dzb_dtau');
dzb_dtau      = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'dzl_dtau');
dzl_dtau      = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'dH_c_dtau');
dH_c_dtau     = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'dH_t_dtau');
dH_t_dtau     = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'dH_dtau');
dH_dtau       = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'vx_b_g');
vx_b_g        = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'vy_b_g');
vy_b_g        = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'vz_b');
vz_b          = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'vh_b');
vh_b          = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'vx_s_g');
vx_s_g        = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'vy_s_g');
vy_s_g        = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'vz_s');
vz_s          = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'vh_s');
vh_s          = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'temp_b');
temp_b        = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'temph_b');
temph_b       = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'p_b_w');
p_b_w         = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'H_w');
H_w           = netcdf.getVar(ncid, varid);

varid         = netcdf.inqVarID(ncid, 'q_gl_g');
q_gl_g        = netcdf.getVar(ncid, varid);

%  ------ 3-d fields

if flag_3d_read == 1

   varid      = netcdf.inqVarID(ncid, 'vx_c');
   vx_c       = netcdf.getVar(ncid, varid);

   varid      = netcdf.inqVarID(ncid, 'vy_c');
   vy_c       = netcdf.getVar(ncid, varid);

   varid      = netcdf.inqVarID(ncid, 'vz_c');
   vz_c       = netcdf.getVar(ncid, varid);

   varid      = netcdf.inqVarID(ncid, 'vx_t');
   vx_t       = netcdf.getVar(ncid, varid);

   varid      = netcdf.inqVarID(ncid, 'vy_t');
   vy_t       = netcdf.getVar(ncid, varid);

   varid      = netcdf.inqVarID(ncid, 'vz_t');
   vz_t       = netcdf.getVar(ncid, varid);

   varid      = netcdf.inqVarID(ncid, 'temp_c');
   temp_c     = netcdf.getVar(ncid, varid);

   varid      = netcdf.inqVarID(ncid, 'omega_t');
   omega_t    = netcdf.getVar(ncid, varid);

   varid      = netcdf.inqVarID(ncid, 'temp_r');
   temp_r     = netcdf.getVar(ncid, varid);
   
   varid      = netcdf.inqVarID(ncid, 'enth_c');
   enth_c     = netcdf.getVar(ncid, varid);
   
   varid      = netcdf.inqVarID(ncid, 'enth_t');
   enth_t     = netcdf.getVar(ncid, varid);

   varid      = netcdf.inqVarID(ncid, 'omega_c');
   omega_c    = netcdf.getVar(ncid, varid);

   varid      = netcdf.inqVarID(ncid, 'age_c');
   age_c      = netcdf.getVar(ncid, varid);

   varid      = netcdf.inqVarID(ncid, 'age_t');
   age_t      = netcdf.getVar(ncid, varid);

end

%-------- Closing NetCDF file --------

netcdf.close(ncid);

disp(' ')
disp('Done.')
disp(' ')

end % function read_time_slice_nc
%
