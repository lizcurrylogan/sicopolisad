function [time, DTs_gi, z_sl, ...
          H_Vo, H_DA, H_DC, H_DF, H_Ko, H_By, ...
          v_Vo, v_DA, v_DC, v_DF, v_Ko, v_By, ...
          T_Vo, T_DA, T_DC, T_DF, T_Ko, T_By, ...
          Rb_Vo, Rb_DA, Rb_DC, Rb_DF, Rb_Ko, Rb_By, ...
          ndata] ...
        = read_core_ant(runname, outpath)
% READ_CORE_ANT Reading of time series data produced by SICOPOLIS.
% =========================================================================
% read_core_ant  Date: 2013-05-30
%
% Usage: 
%
%   [time, DTs_gi, z_sl, ...
%    H_Vo, H_DA, H_DC, H_DF, H_Ko, H_By, ...
%    v_Vo, v_DA, v_DC, v_DF, v_Ko, v_By, ...
%    T_Vo, T_DA, T_DC, T_DF, T_Ko, T_By, ...
%    Rb_Vo, Rb_DA, Rb_DC, Rb_DF, Rb_Ko, Rb_By, ...
%    ndata] ...
%      = read_core_ant(runname, outpath)
%
% Description:
%   read_core_ant reads time series output data of SICOPOLIS for
%   the deep ice cores of the Antarctic Ice Sheet in ASCII format
%   (from file runname.core).
%   Results are returned in the vectors
%   time, DTs_gi, z_sl, H_xx, v_xx, T_xx, Rb_xx.
%   The scalar ndata contains the number of data in the vectors.
%
% Input:
%   runname - name of SICOPOLIS simulation
%   outpath - path of time series file (without trailing "/")
%
% Output:
%   time    - time [a]
%   DTs_gi  - surface temperature anomaly [C] or glacial index [1]
%   z_sl    - sea level [m]
%   H_xx    - ice thickness at position xx [m]
%   v_xx    - surface speed at position xx [m a^-1]
%   T_xx    - basal temperature at position xx [C]
%   Rb_xx   - basal frictional heating at position xx [W m^-2]
%             where xx = Vo (Vostok), DA (Dome A), DC (Dome C),
%                        DF (Dome F), Ko (Kohnen) or By (Byrd)
%   ndata   - number of data
%
% Author:
%   Ralf Greve
% =========================================================================

%-------- Name of time-series file --------

filename = [runname '.core'];

disp(' ')
disp(['Name of time-series file: ' filename])

%-------- Opening time-series file --------

fid = fopen([outpath '/' filename], 'r');

if fid == -1

   disp(' ')
   disp(['Time-series file not found in directory ' outpath '.'])
   outpath = [outpath '/' runname];
   disp(['Trying ' outpath ' instead...'])

   fid2 = fopen([outpath '/' filename], 'r');

   if fid2 ~= -1
      disp('Time-series file found. Continuing...')
      fid = fid2;
   else
      disp(['Time-series file not found in directory ' outpath ' either.'])
      disp('Giving up.')
   end
   
end

%-------- Reading of data from time-series file --------

for n=1:6
  ch_dummy = fgetl(fid);
end

n = 0;

while feof(fid) == 0

   n = n+1;

   try
   
      time(n)   = fscanf(fid, '%g', 1);
      DTs_gi(n) = fscanf(fid, '%g', 1);
      z_sl(n)   = fscanf(fid, '%g', 1);

      H_Vo(n)   = fscanf(fid, '%g', 1);
      H_DA(n)   = fscanf(fid, '%g', 1);
      H_DC(n)   = fscanf(fid, '%g', 1);
      H_DF(n)   = fscanf(fid, '%g', 1);
      H_Ko(n)   = fscanf(fid, '%g', 1);
      H_By(n)   = fscanf(fid, '%g', 1);
      
      v_Vo(n)   = fscanf(fid, '%g', 1);
      v_DA(n)   = fscanf(fid, '%g', 1);
      v_DC(n)   = fscanf(fid, '%g', 1);
      v_DF(n)   = fscanf(fid, '%g', 1);
      v_Ko(n)   = fscanf(fid, '%g', 1);
      v_By(n)   = fscanf(fid, '%g', 1);

      T_Vo(n)   = fscanf(fid, '%g', 1);
      T_DA(n)   = fscanf(fid, '%g', 1);
      T_DC(n)   = fscanf(fid, '%g', 1);
      T_DF(n)   = fscanf(fid, '%g', 1);
      T_Ko(n)   = fscanf(fid, '%g', 1);
      T_By(n)   = fscanf(fid, '%g', 1);
      
      Rb_Vo(n)  = fscanf(fid, '%g', 1);
      Rb_DA(n)  = fscanf(fid, '%g', 1);
      Rb_DC(n)  = fscanf(fid, '%g', 1);
      Rb_DF(n)  = fscanf(fid, '%g', 1);
      Rb_Ko(n)  = fscanf(fid, '%g', 1);
      Rb_By(n)  = fscanf(fid, '%g', 1);
      
      ch_dummy   = fgetl(fid);

   catch

      ndata = n-1;
      break

   end

end

%-------- Closing time-series file --------

status = fclose(fid);

disp(' ')
disp('Done.')
disp(' ')

end % function read_core_ant
%
