function [time, DTs_gi, z_sl, ...
          H_GR, H_G2, H_D3, H_CC, H_NG, H_NE, ...
          v_GR, v_G2, v_D3, v_CC, v_NG, v_NE, ...
          T_GR, T_G2, T_D3, T_CC, T_NG, T_NE, ...
          Rb_GR, Rb_G2, Rb_D3, Rb_CC, Rb_NG, Rb_NE, ...
          ndata] ...
        = read_core_grl(runname, outpath)
% READ_CORE_GRL Reading of time series data produced by SICOPOLIS.
% =========================================================================
% read_core_grl  Date: 2013-05-30
%
% Usage: 
%
%   [time, DTs_gi, z_sl, ...
%    H_GR, H_G2, H_D3, H_CC, H_NG, H_NE, ...
%    v_GR, v_G2, v_D3, v_CC, v_NG, v_NE, ...
%    T_GR, T_G2, T_D3, T_CC, T_NG, T_NE, ...
%    Rb_GR, Rb_G2, Rb_D3, Rb_CC, Rb_NG, Rb_NE, ...
%    ndata] ...
%      = read_core_grl(runname, outpath)
%
% Description:
%   read_core_grl reads time series output data of SICOPOLIS for
%   the deep ice cores of the Greenland Ice Sheet in ASCII format
%   (from file runname.core).
%   Results are returned in the vectors
%   time, DTs_gi, z_sl, H_xx, v_xx, T_xx, Rb_xx.
%   The scalar ndata contains the number of data in the vectors.
%
% Input:
%   runname - name of SICOPOLIS simulation
%   outpath - path of time series file (without trailing "/")
%
% Output:
%   time    - time [a]
%   DTs_gi  - surface temperature anomaly [C] or glacial index [1]
%   z_sl    - sea level [m]
%   H_xx    - ice thickness at position xx [m]
%   v_xx    - surface speed at position xx [m a^-1]
%   T_xx    - basal temperature at position xx [C]
%   Rb_xx   - basal frictional heating at position xx [W m^-2]
%             where xx = GR (GRIP), G2 (GISP2), D3 (Dye 3),
%                        CC (Camp Century), NG (NGRIP) or NE (NEEM)
%   ndata   - number of data
%
% Author:
%   Ralf Greve
% =========================================================================

%-------- Name of time-series file --------

filename = [runname '.core'];

disp(' ')
disp(['Name of time-series file: ' filename])

%-------- Opening time-series file --------

fid = fopen([outpath '/' filename], 'r');

if fid == -1

   disp(' ')
   disp(['Time-series file not found in directory ' outpath '.'])
   outpath = [outpath '/' runname];
   disp(['Trying ' outpath ' instead...'])

   fid2 = fopen([outpath '/' filename], 'r');

   if fid2 ~= -1
      disp('Time-series file found. Continuing...')
      fid = fid2;
   else
      disp(['Time-series file not found in directory ' outpath ' either.'])
      disp('Giving up.')
   end
   
end

%-------- Reading of data from time-series file --------

for n=1:6
  ch_dummy = fgetl(fid);
end

n = 0;

while feof(fid) == 0

   n = n+1;

   try
   
      time(n)   = fscanf(fid, '%g', 1);
      DTs_gi(n) = fscanf(fid, '%g', 1);
      z_sl(n)   = fscanf(fid, '%g', 1);

      H_GR(n)   = fscanf(fid, '%g', 1);
      H_G2(n)   = fscanf(fid, '%g', 1);
      H_D3(n)   = fscanf(fid, '%g', 1);
      H_CC(n)   = fscanf(fid, '%g', 1);
      H_NG(n)   = fscanf(fid, '%g', 1);
      H_NE(n)   = fscanf(fid, '%g', 1);
      
      v_GR(n)   = fscanf(fid, '%g', 1);
      v_G2(n)   = fscanf(fid, '%g', 1);
      v_D3(n)   = fscanf(fid, '%g', 1);
      v_CC(n)   = fscanf(fid, '%g', 1);
      v_NG(n)   = fscanf(fid, '%g', 1);
      v_NE(n)   = fscanf(fid, '%g', 1);

      T_GR(n)   = fscanf(fid, '%g', 1);
      T_G2(n)   = fscanf(fid, '%g', 1);
      T_D3(n)   = fscanf(fid, '%g', 1);
      T_CC(n)   = fscanf(fid, '%g', 1);
      T_NG(n)   = fscanf(fid, '%g', 1);
      T_NE(n)   = fscanf(fid, '%g', 1);
      
      Rb_GR(n)  = fscanf(fid, '%g', 1);
      Rb_G2(n)  = fscanf(fid, '%g', 1);
      Rb_D3(n)  = fscanf(fid, '%g', 1);
      Rb_CC(n)  = fscanf(fid, '%g', 1);
      Rb_NG(n)  = fscanf(fid, '%g', 1);
      Rb_NE(n)  = fscanf(fid, '%g', 1);
      
      ch_dummy   = fgetl(fid);

   catch

      ndata = n-1;
      break

   end

end

%-------- Closing time-series file --------

status = fclose(fid);

disp(' ')
disp('Done.')
disp(' ')

end % function read_core_grl
%
