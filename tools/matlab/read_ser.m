function [time, DTs_gi, z_sl, ...
          V_tot, V_grounded, V_floating, A_tot, A_grounded, A_floating, ...
          H_max, zs_max, V_temp, V_fw, V_sle, ...
          A_temp, V_bm, V_tld, H_t_max, vs_max, ndata] ...
        = read_ser(runname, outpath)
% READ_SER Reading of time series data produced by SICOPOLIS.
% =========================================================================
% read_ser  Date: 2013-05-30
%
% Usage: 
%
%   [time, DTs_gi, z_sl, ...
%    V_tot, V_grounded, V_floating, A_tot, A_grounded, A_floating, ...
%    H_max, zs_max, V_temp, V_fw, V_sle, ...
%    A_temp, V_bm, V_tld, H_t_max, vs_max, ndata] ...
%      = read_ser(runname, outpath)
%
% Description:
%   read_ser reads time series output data of SICOPOLIS for
%   terrestrial ice sheets in ASCII format (from file runname.ser)
%   for the case OUTSER_V_A==2, that is, volume and area data in the
%   file comprise total, grounded and floating ice volumes and areas.
%   Results are returned in the vectors
%   time, DTs_gi, z_sl, V_tot, V_grounded, V_floating,
%   A_tot, A_grounded, A_floating, H_max, zs_max, V_temp, V_fw, V_sle,
%   A_temp, V_bm, V_tld, H_t_max, vs_max.
%   The scalar ndata contains the number of data in the vectors.
%
% Input:
%   runname - name of SICOPOLIS simulation
%   outpath - path of time series file (without trailing "/")
%
% Output:
%   time       - time [a]
%   DTs_gi     - surface temperature anomaly [C] or glacial index [1]
%   z_sl       - sea level [m]
%   V_tot      - total ice volume [m^3]
%   V_grounded - volume of grounded ice [m^3]
%   V_floating - volume of floating ice [m^3]
%   A_tot      - total ice area [m^2]
%   A_grounded - area of grounded ice [m^2]
%   A_floating - area of floating ice [m^2]
%   H_max      - maximum ice thickness [m]
%   zs_max     - maximum surface elevation [m]
%   V_temp     - volume of temperate ice [m^3]
%   V_fw       - freshwater flux due to melting and calving [m^3 a^-1]
%   V_sle      - ice volume in sea level equivalent [m SLE]
%   A_temp     - area of temperate-based grounded ice [m^2]
%   V_bm       - basal melting rate [m^3 a^-1]
%   V_tld      - water drainage from temperate ice layers [m^3 a^-1]
%   H_t_max    - maximum thickness of temperate ice [m]
%   vs_max     - maximum surface speed [m a^-1]
%   ndata      - number of data
%
% Author:
%   Ralf Greve
% =========================================================================

%-------- Name of time-series file --------

filename = [runname '.ser'];

disp(' ')
disp(['Name of time-series file: ' filename])

%-------- Opening time-series file --------

fid = fopen([outpath '/' filename], 'r');

if fid == -1

   disp(' ')
   disp(['Time-series file not found in directory ' outpath '.'])
   outpath = [outpath '/' runname];
   disp(['Trying ' outpath ' instead...'])

   fid2 = fopen([outpath '/' filename], 'r');

   if fid2 ~= -1
      disp('Time-series file found. Continuing...')
      fid = fid2;
   else
      disp(['Time-series file not found in directory ' outpath ' either.'])
      disp('Giving up.')
   end
   
end

%-------- Reading of data from time-series file --------

for n=1:5
  ch_dummy = fgetl(fid);
end

n = 0;

while feof(fid) == 0

   n = n+1;

   try
   
      time(n)       = fscanf(fid, '%g', 1);
      DTs_gi(n)     = fscanf(fid, '%g', 1);
      z_sl(n)       = fscanf(fid, '%g', 1);

      V_tot(n)      = fscanf(fid, '%g', 1);
      V_grounded(n) = fscanf(fid, '%g', 1);
      V_floating(n) = fscanf(fid, '%g', 1);
      A_tot(n)      = fscanf(fid, '%g' ,1);
      A_grounded(n) = fscanf(fid, '%g' ,1);
      A_floating(n) = fscanf(fid, '%g' ,1);

      H_max(n)      = fscanf(fid, '%g', 1);
      zs_max(n)     = fscanf(fid, '%g', 1);
      V_temp(n)     = fscanf(fid, '%g', 1);
      V_fw(n)       = fscanf(fid, '%g', 1);
      V_sle(n)      = fscanf(fid, '%g', 1);
   
      A_temp(n)     = fscanf(fid, '%g', 1);
      V_bm(n)       = fscanf(fid, '%g', 1);
      V_tld(n)      = fscanf(fid, '%g', 1);
      H_t_max(n)    = fscanf(fid, '%g', 1);
      vs_max(n)     = fscanf(fid, '%g', 1);

      ch_dummy      = fgetl(fid);

   catch

      ndata = n-1;
      break

   end

end

%-------- Closing time-series file --------

status = fclose(fid);

disp(' ')
disp('Done.')
disp(' ')

end % function read_ser
%
