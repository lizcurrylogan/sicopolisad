#!/bin/bash
LANG=C

################################################################################
#
#  t o o l s . s h
#
#  bash script for
#  compilation, linking and execution of the tools.
#
#  Authors: Malte Thoma, Thomas Goelles, Ralf Greve
#
#    Execute script 
#       ./tools.sh -p <program> -m <run_name> [further options...]
#    where <program> is the name of the program to be executed,
#    and <run_name> is the name of the simulation.
#
################################################################################

function error()
{
   echo -e "\n******************************************************************************">&2
   echo -e   "** ERROR:                                                                   **">&2
   echo -e   "$1" | awk '{printf("** %-73s**\n",$0)}'>&2
   echo -e "******************************************************************************\n">&2
   exit 1
}

################################################################################

function info()
{
   echo -e "$1" >&2
}

################################################################################

function usage()
{
   echo -e "\n  Usage: `basename $0` -p <program> -m <run_name> [further options...]\n"\
   "     use -p? to get a list of available programs\n"\
   "     use -m? to get a list of available simulations\n"\
   "     [-p <program>]  => name of the program\n"\
   "     [-m <run_name>] => name of the simulation\n"\
   "     [-d <dir>] => individual output directory used for the simulation,\n"\
   "                   default is sico_out/<run_name>\n"\
   "     [-n] => skip make clean\n"\
   "     [-z] => manual configuration by file sico_configs.sh\n"\
   "             (otherwise handled by GNU Autotools)\n"\

   if [ "$1" ]; then error "$1"; fi
}

################################################################################

function check_args()
{
   while getopts d:hm:np:z? OPT ; do
     case $OPT in
       d) local OUTDIR=$OPTARG ;;
       m) RUN=$OPTARG ;;
       n) SKIP_MAKECLEAN="TRUE" ;;
       p) PROGNAME=$OPTARG ;;
       z) MANUAL_CONFIGURATION=="TRUE";;
     h|?) usage; exit 1;;
     
     esac            
   done

   # Check if PROGNAME is set correctly
   if [ ! "$PROGNAME" ]; then error "No program chosen. Try option -h."; fi
   if [ "$PROGNAME" == "?" ]; then 
      info "-----------------------"
      info "Available programs are:"
      info "-----------------------"
      # ls *.F90 | sed 's/.F90//g' 
      info "make_searise_output"
      info "resolution_doubler"
      info "sicograph"
      exit 1
   fi
   if [ ! -e $PROGNAME/$PROGNAME.F90 ]; then 
      error "Program $PROGNAME.F90 not found. Try options -p? or -h."
   fi 

   # Output directory, absolute paths
   if [ ! "$OUTDIR" ]; then      
      OUTDIR=`pwd`"/../sico_out/"
   else
      lastch=`echo $OUTDIR | sed -e 's/\(^.*\)\(.$\)/\2/'`
      if [ ${lastch} == "/" ]; then OUTDIR=`echo $OUTDIR | sed '$s/.$//'`; fi
      if [ ! -e $OUTDIR ]; then error "$OUTDIR does not exist."; fi       
   fi

   # Check if RUN is set correctly
   if [ ! "$RUN" ]; then error "No simulation set. Try option -h."; fi
   if [ $RUN == "?" ]; then 
      info "Available results in $OUTDIR are:"
      ls $OUTDIR  | sed 's/F_//g' | sed 's/R_.*//g' 
      exit 1
   fi

   DATAPATH="$OUTDIR/$RUN" 

   if [ ! -e $DATAPATH ]; then 
      error "$DATAPATH does not exist."
   fi
}

################################################################################

function compile()
{
   if [ $MANUAL_CONFIGURATION ]; then
      info "\nManual configuration (by file sico_configs.sh)."
      source ../runs/sico_configs.sh
   else
      info "\nUsing makefile created by GNU Autotools."
   fi

   cd ./${PROGNAME}

   EXE_FILE=${PROGNAME}.x
   HEADER_FILE="sico_specs.h"

   $RM -f ${EXE_FILE}

   if [ $MANUAL_CONFIGURATION ]; then
      $RM -f *.mod
   else
      make -s clean
   fi

   $RM -f $HEADER_FILE
   $CP $DATAPATH/$HEADER_FILE .

   if [ $MANUAL_CONFIGURATION ]; then
      FCFLAGS=${FCFLAGS}' -o '${EXE_FILE}
      ${FC} ./${PROGNAME}.F90 ${FCFLAGS}
   else
      make -s 2> /dev/null
      make -s ${PROGNAME}
      $MV ${PROGNAME} ${EXE_FILE}
   fi
}

################################################################################

function run()
{
   info "Starting ./${EXE_FILE} ..."
   ./${EXE_FILE}

   if [ ! $SKIP_MAKECLEAN ]; then

      $RM -f ${EXE_FILE}
      $RM -f ${HEADER_FILE}

      if [ $MANUAL_CONFIGURATION ]; then
         $RM -f *.mod
      else
         make -s clean
      fi

   fi

   info "\n... finished."

   cd $OLDPWD
}

################################################################################

RM=/bin/rm
CP=/bin/cp
MV=/bin/mv

check_args $*
compile
run

############################# End of tools.sh ##################################
